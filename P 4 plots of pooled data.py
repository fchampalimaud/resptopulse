# 4. Calculate the statistics of the pooled data_csus.
# Calculate the statistics of pooled data_csus in single trials.


# %%
# %reset -f
from pathlib import Path
import pandas as pd
import seaborn as sns
import gc
# from pandas.api.types import CategoricalDtype


from my_general_variables import *
from my_experiment_specific_variables import *
# import my_functions_P_2 as f
import my_functions_F_1 as f


import importlib
importlib.reload(f)

pd.set_option('mode.chained_assignment', None)


# save_here = Path(r'G:\My Drive\PhD\Lab meetings\my Lab Meetings\Paton #2')


# Sub_data = False

# if Sub_data:
# 	key = (,)
# 	level = 

control = control

# Fuse_controls = True

Continuous_traces = True
Difference_at_onset = False

Variable_cr_window = True

Subt_by_control = False
Null_baseline = True
Bin_data = False

P_value = False

# bin_interval = 1 # s



frmt = 'svg' 

stat = 'mean'

comment = 'all trials, all fish'

mean_bef_onset = 'mean '+str(baseline_window)+' s before'
difference = 'difference'



# Path to save processed data; create folder to save processed data if it does not exist yet.
path_to_save_proc_data = path_home / 'Processed data'
# path_to_save_proc_data.mkdir(parents=True, exist_ok=True)

path_pooled_data = path_to_save_proc_data / 'pkl files' / 'Pooled data by condition'
# path_part_to_save = path_pooled_data / 'Stats'
# path_part_to_save.mkdir(parents=True, exist_ok=True)









all_data_paths = [*Path(path_pooled_data).glob('*.pkl')]












#* Define figure paths. 1
#TODO might want to turn this into a function as it is repeated in '2'
path_part_fig_continuous_traces = []

for csus in [cs, us]:
	for param in [metrics_dict[m][folder_name] for m in metrics_dict]:

		if Subt_by_control:
			param = Path(str(param) + (' subtrated by control' if Subt_by_control else ''))

		if Null_baseline:
			param = Path(str(param) + (' null baseline' if Null_baseline else ''))

		path_part_fig_continuous_traces.append(path_to_save_proc_data / param / ('aligned by ' + str(csus)))
		path_part_fig_continuous_traces[-1].mkdir(parents=True, exist_ok=True)

# all_fish_data_paths = [*Path(path_home / 'Processed data' / 'pkl files' / 'Pooled data by condition' / 'Stats').glob('*.pkl')]


if P_value:

#TODO maybe not necessary
	Subt_by_control = False
	# Bin_data = False


f.defineMetricsDict(Subt_by_control, Null_baseline)


if Bin_data:

	#! bin_interval_f = bin_interval

	time_bins = list(np.arange(-bin_interval, -t_crop_data_bef_s-bin_interval, -bin_interval))[::-1] + list(np.arange(0, t_crop_data_aft_s+bin_interval, bin_interval))
	last_part_fig_name = str(bin_interval) + '-s bin'

else:

	time_bins = None
	last_part_fig_name = str(bin_interval) + '-s window'

if Difference_at_onset:

	def make_diff_at_onset_plots(all_data_csus_pool_type_difference_at_onset_):

		#* Plot the baseline movement. #* Plot the movement after the onset of CS. #* Plot the difference at the onset.

		exp_types_ = all_data_csus_pool_type_difference_at_onset_['Exp.'].unique()

		for m_i, m in enumerate(metrics):
		# m=bout

			comment_ = comment + '  ' + csus + '  ' + m

			#TODO remove this part
			#todo option is to use .copy(False)
			# data_condition = data_condition_difference_at_onset
			# data_control = data_control_difference_at_onset
			# data_condition = all_data_csus_pool_type_difference_at_onset_[all_data_csus_pool_type_difference_at_onset_['Exp.'] == exp_type]
			# data_control = all_data_csus_pool_type_difference_at_onset_[all_data_csus_pool_type_difference_at_onset_['Exp.'] == control]

			#* For the parallel coordinates plot.
			all_data_csus_pool_type_difference_at_onset_['Index'] = ['_'.join(i)  for i in all_data_csus_pool_type_difference_at_onset_.index]

			#* Select only data of the "m" metric.
			just_difference_at_onset = all_data_csus_pool_type_difference_at_onset_.drop(columns=[m_to_remove for m_to_remove in metrics if m_to_remove != m]).reset_index(drop=True)

			#* Select only "difference".
			just_difference_at_onset = just_difference_at_onset.drop(columns=(pd.MultiIndex.from_product([[m], [mean_bef_onset, mean_aft_onset]]))).droplevel(1, axis=1)

			just_difference_at_onset = just_difference_at_onset.groupby([number_session, 'Index', 'Exp.'], as_index=True, observed=True)[m].mean().reset_index(number_session).pivot(columns=number_session).reset_index('Exp.')

			just_difference_at_onset.columns = [just_difference_at_onset.columns[0][0]] + [i[1] for i in just_difference_at_onset.columns[1:].ravel()]

			just_difference_at_onset.reset_index(inplace=True)


			#* For the line/bar plot with the number of fish in each session that go up or down.

			just_difference_at_onset_ = just_difference_at_onset.copy()

			just_difference_at_onset_.loc[:,sessions_dict[pool_type][csus][names_sessions]] = just_difference_at_onset_.loc[:,sessions_dict[pool_type][csus][names_sessions]].applymap(lambda x: f.up_or_down(x))


			#* For the scatter plot.
	#TODO check the degrees of freedom!!!!!!!!!!!!!!!!!
			#* Mean groupby(['Exp.', number_session])
			mean_and_sem_exp_session = all_data_csus_pool_type_difference_at_onset_.groupby(['Exp.', number_session], as_index=False).agg(['mean', 'sem'])


	#* Striplot.

			fig, axs = plt.subplots(3, 1, sharex=True, sharey=False, facecolor='white', constrained_layout=True)

			for ax_i, ax in enumerate(axs):

				sns.stripplot(x=number_session, y=(m, all_data_csus_pool_type_difference_at_onset_[m].columns[ax_i]), hue='Exp.', jitter=0.35, dodge=True, palette=palette, data=all_data_csus_pool_type_difference_at_onset_, ax=ax, alpha=0.4, clip_on=False)
				# sns.stripplot(x=number_session, y=(m, list_[ax_i], 'mean'), hue='Exp.', jitter=0.35, dodge=True, palette=palette, data=all_data_csus_pool_type_difference_at_onset_, ax=ax, alpha=0.4, clip_on=False)

			fig, axs = f.arrangeFig_2(fig, axs, csus, m, pool_type, cr_window, y_lim_3=(None,None))
			# axs[2].autoscale(axis='y')
			# axs[0].set_ylim(0,1.2)
			# axs[1].set_ylim(0,1.2)

			# for ax in axs:
			# 	ax.autoscale()

			f.saveFig_2(fig, fig_names[m_i], 'stripplot', exp_types[data_condition_i], baseline_window, cr_window, comment_, figure_size=(15,15), frmt='svg')


	#* Parallel coordinates plot.

			for ex in exp_types_:
					
				fig, axs = plt.subplots(1, 1, facecolor='white', constrained_layout=True)

				axs = pd.plotting.parallel_coordinates(just_difference_at_onset.loc[just_difference_at_onset['Exp.']==ex].drop(columns=['Exp.']), 'Index',  axvlines=True, colormap='gist_rainbow')

				axs.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0, frameon=False)
				# axs.get_legend().remove()

				axs.set_xlabel(None)

				axs.set_ylabel('$\Delta{P}^{\ stim.\ onset}_{\ tail\ movement}$')

				axs.spines[:].set_visible(False)
				axs.tick_params(axis='both', which='both', bottom=True, top=False, right=False)
				# axs.locator_params(axis='y', tight=False, nbins=4)

				# axs.set_ylim((-0.35,0.5))
				# axs.set_ylim((-0.4,0.2))
				# axs.set_ylim((-0.2,0.2))

				axs.axhline(0, color='k', alpha=0.8, linewidth=1)

				fig.suptitle(ex + '  ' + pool_type + '\n', x=0, horizontalalignment='left')

				for tick in axs.get_xticklabels():
					tick.set_rotation(45)

				f.saveFig_2(fig, fig_names[m_i], 'parallelcoord', ex, baseline_window, cr_window, comment_, figure_size=(15,15))


	#* Line/bar plot with the number of fish in each session that go up or down. 

			for ex in exp_types_:

				data_ = just_difference_at_onset_.loc[just_difference_at_onset_['Exp.']==ex].drop(columns=['Exp.', 'Index'])

				data_ = pd.concat([data_.iloc[:,i].value_counts() for i in range(len(data_.columns))], axis=1).fillna(0).T
				
				# data_ = data_[['Decrease', 'Increase', 'No change']]

				fig, axs = plt.subplots(1, 1, facecolor='white', constrained_layout=True)

				data_.plot(ax=axs, clip_on=False)

				axs.spines['right'].set_visible(False)
				axs.spines['top'].set_visible(False)

				axs.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0, frameon=False)

				axs.set_ylabel('Number of fish')

				axs.tick_params(axis='both', which='both', bottom=True, top=False, right=False)

				# axs.set_ylim((-0.35,0.5))

				axs.axhline(0, color='k', alpha=0.8, linewidth=1)

				fig.suptitle(ex + '  ' + pool_type + '\n', x=0, horizontalalignment='left')

				for tick in axs.get_xticklabels():
					tick.set_rotation(45)

				f.saveFig_2(fig, fig_names[m_i], 'lineplot2', ex, baseline_window, cr_window, comment_, figure_size=(10,10))


	#* Scatter plot: mean with SEM.
			
			fig, axs = plt.subplots(3, 1, sharex=True, sharey=False, facecolor='white', constrained_layout=True)

			for ax_i, ax in enumerate(axs):

				for exp_type_i, exp_type in enumerate(mean_and_sem_exp_session.index.get_level_values('Exp.').unique()):

					mean_and_sem_exp_session_ = mean_and_sem_exp_session.xs(exp_type).xs((m), axis=1)
					
					mean_and_sem_exp_session_.reset_index(number_session, inplace=True)
					ax.errorbar(mean_and_sem_exp_session_[number_session], mean_and_sem_exp_session_.xs((list_[ax_i], 'mean'), axis=1), yerr=mean_and_sem_exp_session_.xs((list_[ax_i], 'sem'), axis=1), fmt='v' if exp_type == control else '^', color=palette[exp_type_i], markeredgecolor=palette[exp_type_i], alpha=0.7, linewidth=1, markersize=10, elinewidth=2, capthick=2, capsize=6, label=exp_type)

			fig, axs = f.arrangeFig_2(fig, axs, csus, m, pool_type, cr_window, y_lim_3=(None,None))

			f.saveFig_2(fig, fig_names[m_i], 'scatterplot', exp_types[data_condition_i], baseline_window, cr_window, comment_, figure_size=(15,15))


	#* Boxplot

			fig, axs = plt.subplots(3, 1, sharex=True, sharey=False, facecolor='white', constrained_layout=True)

			for ax_i, ax in enumerate(axs):
				
				sns.boxplot(x=number_session, y=(m, all_data_csus_pool_type_difference_at_onset_[m].columns[ax_i]), hue='Exp.', fliersize=0, linewidth=1, dodge=True, palette=[p + [0.2] for p in palette], data=all_data_csus_pool_type_difference_at_onset_, ax=axs[ax_i], showmeans=True, meanline=True, meanprops=dict(linestyle='--', linewidth=1, color='k'), medianprops=dict(color='k', linewidth=1,), boxprops=dict(edgecolor='k'), whiskerprops=dict(color='k'), capprops=dict(color='k'))

			fig, axs = f.arrangeFig_2(fig, axs, csus, m, pool_type, cr_window, y_lim_3=(None,None))
			# axs[0].set_ylim(-1,0.8)
			# axs[1].set_ylim(-1,0.8)
			# for ax in axs:
			# 	ax.autoscale()

			f.saveFig_2(fig, fig_names[m_i], 'boxplot', exp_types[data_condition_i], baseline_window, cr_window, comment_, figure_size=(15,15))


				# #* Statistical test
				# # from scipy.stats import mannwhitneyu as mann
				# # from scipy.stats import wilcoxon as wilc
				# from scipy.stats import mannwhitneyu as mann
				# from scipy.stats import ranksums as ranks


				# print('baseline different than 0? ', (all_data_csus_pool_type_difference_at_onset_[(bout, mean_bef_onset)] == 0).any())


				# data_ = all_data_csus_pool_type_difference_at_onset_.set_index('Exp.').drop(columns=[number_trial_csus, (m, mean_bef_onset), (m, mean_aft_onset)])
				# ind = data_.index.unique()


				# #* Compare within condition.
				# print('mannwhitneyu')

				# c_1 = data_[number_session].cat.categories[0]

				# for i in ind:
				# d_ = data_.xs(i)

				# for c in d_[number_session].cat.categories[1:]:

				# _, p_values = mann(d_[d_[number_session] == c_1].iloc[:,-1], d_[d_[number_session] == c].iloc[:,-1])

				# print(i, '\n', c_1, '\n', c, '\n', p_values,'\n\n')


				# #* Compare different conditions.
				# print('ranksums')

				# for c in data_[number_session].cat.categories:

				# d_ = data_[data_[number_session] == c].drop(columns=number_session)

				# _, p_values = ranks(d_.xs(ind[0]), d_.xs(ind[1]))

				# print(ind[0], '\n', ind[1], '\n', c, '\n', p_values[0],'\n\n')


				# d_control = data_control.iloc[:, [1,2,4]].set_index(number_session)
				# d_condition = data_condition.iloc[:, [1,2,4]].set_index(number_session)

				# p_values_mann = list(np.zeros(len(d_control.index.get_level_values(number_session).categories)))

				# for s_i, s in enumerate(d_control.index.get_level_values(number_session).categories):

				# 	_, p_values_mann[s_i] = mann(d_control.loc[s,:], d_condition.loc[s,:])
				# 	# , method='exact')













for csus in [cs, us]:

	all_data_csus_paths = [path for path in all_data_paths if str(path.stem).split('_')[-1] == csus]

	exp_types = [str(path.stem).split('_')[0] for path in all_data_csus_paths]

	control_index = exp_types.index(control)


	#* Read data.
	# all_data_csus = [f.prepare_data(pd.read_pickle(str(condition_csus_path)), csus, pool_type, stat, Bin_data, time_bins) for condition_csus_path in all_data_csus_paths]
	# all_data_csus = [f.prepare_data(pd.read_pickle(str(condition_csus_path)), csus, pool_type) for condition_csus_path in all_data_csus_paths]
	all_data_csus = [pd.read_pickle(str(condition_csus_path)) for condition_csus_path in all_data_csus_paths]


	# if Fuse_controls:

	# 	controls = pd.concat([data_condition for data_condition in all_data_csus if 'control' in data_condition.index.get_level_values('Exp.')[0]])

	# 	controls_index = controls.index.get_level_values('Exp.').unique()

	# 	#TODO HERE I NEED TO UPDATE THE 'EXP.' IN THE INDEX!!!
	# 				for i in range(len(controls_index)):
	# 					controls.rename(index={controls_index[i]:control}, inplace=True)

					# controls.reset_index('Exp.', inplace=True)
					# controls.loc[:, 'Exp.'] = control
					# controls.set_index('Exp.', append=True, inplace=True)

		# all_data_csus = pd.concat([data_condition for data_condition in all_data_csus if 'control' not in data_condition.index.get_level_values('Exp.')[0]] + [controls])

	# break

	#* Get the number of fish per condition.
	exp_types_number_fish = [len(condition.index.unique()) for condition in all_data_csus]
	control_number_fish = exp_types_number_fish[control_index]


#TODO select data
	#* Discard part of the data.
	# if Sub_data:
	# 	all_data_csus = [data_condition.xs(key=key, level=level) for data_condition in all_data_csus]


	#* Iterate over the different types of pooling.
	for pool_type in [k for k in sessions_dict.keys()][-1:]:


		# fig_names = []

		# for c in range(int(len(path_part_fig_continuous_traces) / 2)):
		# 	fig_names.append(str(path_part_fig_continuous_traces[c if csus == cs else c + len(metrics)] / (pool_type + '_' + last_part_fig_name)))
		#* Define figure paths. 2
		fig_names = [fig_name / (pool_type + '_' + last_part_fig_name) for fig_name in [path_part_fig_continuous_traces[c if csus == cs else c + len(metrics)] for c in range(int(len(path_part_fig_continuous_traces) / 2))]]


		#* Prepare data.
		all_data_csus_pool_type = [f.prepare_data(data_condition, csus, pool_type) for data_condition in all_data_csus]

	# 	break
	# break


#TODO need to work on the boolean flags... cannot do all at the same time
		if Subt_by_control or P_value:

			data_control = all_data_csus_pool_type.pop(control_index)

			#? This may be wrong.
			del exp_types[control_index], exp_types_number_fish[control_index]
			del control_index

		if P_value:
			all_data_csus_p_value = f.calculate_p_value_over_time(data_control, all_data_csus_pool_type)



		if Continuous_traces:

			#TODO I think I can remove the reindex part.
			all_data_csus_binned_or_filtered = [f.bin_or_filter_data(data_condition, stat, Bin_data, time_bins) for data_condition in all_data_csus_pool_type]


			if Subt_by_control:

				data_control = f.bin_or_filter_data(data_control, stat, Bin_data, time_bins)
				
				# # all_data_csus_binned_or_filtered[c_i].set_index([time_trial_csus, number_session], inplace=True)

				# all_data_csus_binned_or_filtered[c_i] = all_data_csus_binned_or_filtered[c_i][metrics] - data_control[metrics]
				all_data_csus_binned_or_filtered = [data_condition[metrics] - data_control[metrics] for data_condition in all_data_csus_binned_or_filtered]


			if Null_baseline:

				# index = all_data_csus_binned_or_filtered[c_i].index

				# all_data_csus_binned_or_filtered[c_i] = all_data_csus_binned_or_filtered[c_i][metrics] - all_data_csus_binned_or_filtered[c_i].loc[(index >= -baseline_window * expected_framerate) & (index <= 0), metrics].mean().to_numpy()
				all_data_csus_binned_or_filtered = [data_condition[metrics] - data_condition.loc[(data_condition.index >= -baseline_window * expected_framerate) & (data_condition.index <= 0), metrics].mean().to_numpy() for data_condition in all_data_csus_binned_or_filtered]

			all_data_csus_binned_or_filtered = [data_condition.reset_index() for data_condition in all_data_csus_binned_or_filtered]


			#* Plot pooled trials.
			for m_i, m in enumerate(metrics):

				fig, axs = f.createFig_1(csus, pool_type, y_label=metrics_dict[m][y_label], y_lim=metrics_dict[m][y_lim])
				
				#TODO part of p-value to be done
				# if P_value:

				for data_condition_i, data_condition in enumerate(all_data_csus_binned_or_filtered):

					exp_type = exp_types[data_condition_i]

					if Subt_by_control and exp_type == control:
						continue


					#* Add data to figure.
					for ax_i, ax in enumerate(axs):

						if csus == us and ax_i >= len(axs[:-2]):
							ax_i -= 1
							break

						axs[ax_i].plot(data_condition[time_trial_csus], data_condition.loc[:,m].iloc[:,ax_i], color=condition_dict[exp_type][color], alpha=0.8, linewidth=3, drawstyle="steps-mid", label=exp_type + ' ({})'.format(exp_types_number_fish[data_condition_i]))


					#* Highlight in the subplots specific stimuli to the condition.
					f.highlightSpecStim(axs, csus, exp_type, condition_dict[exp_type][color], sessions_dict[pool_type][csus][trials_sessions])

					if sessions_dict[pool_type][horizontal_fig]:
						axs[ax_i].legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0, frameon=False)
					else:
						axs[0].legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0, frameon=False)
						
					#* Save figure.
					f.saveFig_1(fig, fig_name=fig_names[m_i], figure_size=sessions_dict[pool_type][fig_size], frmt=frmt, dpi=50)

			gc.collect()




	# 	break
	# break

#TODO I will need to adapt for csus==US
		if Difference_at_onset:

			all_data_csus_binned_or_filtered = None


			#* Addapt the conditioned response window to the type of experiment.
			if Variable_cr_window:


				#TODO or use list.pop()
				for data_condition_i, data_condition in enumerate(all_data_csus_pool_type):

					if data_condition_i == control_index or 'control' in data_condition.index.get_level_values('Exp.')[0]:
						continue
					# if exp_types[data_condition_i] != 'trace':
					# 	continue

					exp_type = exp_types[data_condition_i]

					cr_window = condition_dict[exp_types[data_condition_i]][us_latency]

					mean_aft_onset = 'mean '+str(cr_window)+' s after'
					list_ = [mean_bef_onset, mean_aft_onset, difference]

					all_data_csus_pool_type_difference_at_onset = pd.concat([f.difference_at_onset(d, cr_window) for d in [all_data_csus_pool_type[control_index], data_condition]])


					#* Define the color palette.
					palette = [condition_dict[control][color], condition_dict[exp_types[data_condition_i]][color]]

					#TODO Remove
					all_data_csus_pool_type_difference_at_onset_ = all_data_csus_pool_type_difference_at_onset.copy()


					print('baseline_window: ', baseline_window)
					print('cr_window: ', cr_window)


					make_diff_at_onset_plots(all_data_csus_pool_type_difference_at_onset_)

					break
#TODO here: the loop to make the plots
				
				
			#* In case cr_window is the same to all conditions.
			#TODO still to be implemented/tested
			else:
				all_data_csus_pool_type_difference_at_onset = [f.difference_at_onset(data_condition, cr_window) for data_condition in all_data_csus_pool_type]
				
				
				#* Define the color palette.
				palette = [condition_dict[exp_type][color] for exp_type in exp_types]

				make_diff_at_onset_plots()


#TODO here: the loop to make the plots


#TODO do for the other case:
	#TODO csus = us!!!!!!!!!!


		break
	break

print('\n\ndone')
# exec(open("6.1.py").read())