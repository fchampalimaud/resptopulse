# 1. Preprocess data, using absolute time; plot summary of each experiment, showing cropped data; clean data after identifying swim movements.

# %%
from typing import Final
from pathlib import Path
import pandas as pd
from pandas.api.types import CategoricalDtype
import numpy as np
from tqdm import tqdm
import gc
from my_general_variables import *
from my_experiment_specific_variables import *
import my_functions_one as f

from importlib import reload

reload(f)





pd.options.mode.chained_assignment = None
np.warnings.filterwarnings('ignore', category=np.VisibleDeprecationWarning)


Over_write: Final = False



# # Path to save backup of behavior raw data; create folder if it does not exist yet
# path_to_save_backup = path_backup / 'Behavior'
# path_to_save_backup .mkdir(parents=True, exist_ok=True)


# Name pf file with info about all protocols actually run.
# Path(r'F:\EXTRA\trace CC 1.5 s and 2.5 s 30 trials\after abs time but before always abs time')

protocol_info_path = str(path_home / 'Protocol summary.txt')


# Write header of protocol_info
f.save_info(protocol_info_path, 'Fish ID', ['notes', 'habituation (min)', 'min cs-cs inter. (min)', 'min cs dur. (ms)', 'max cs dur.(ms)', 'min us-us inter. (min)', 'min us dur. (ms)', 'max us dur.(ms)', 'number of CS', 'number of US'])
# '\tnotes\thabituation (min)\tmin cs-cs inter. (min)\tmin cs dur. (ms)\tmax cs dur.(ms)\tmin us-us inter. (min)\tmin us dur. (ms)\tmax us dur.(ms)\tnumber of CS\tnumber of US'

# Path to save processed data; create folder to save processed data if it does not exist yet
path_to_save_proc_data = path_home / 'Processed data'
path_to_save_proc_data.mkdir(parents=True, exist_ok=True)

path_orig_pkl = path_to_save_proc_data / 'pkl files'
path_orig_pkl.mkdir(parents=True, exist_ok=True)

path_lost_frames = path_home / ('Lost frames')
path_lost_frames.mkdir(parents=True, exist_ok=True)

path_summary_exp = path_home / ('Summary of protocol actually run')
path_summary_exp.mkdir(parents=True, exist_ok=True)

path_summary_beh = path_home / ('Summary of behavior')
path_summary_beh.mkdir(parents=True, exist_ok=True)


path_cropped_exp_with_bout_detection = path_to_save_proc_data / r'1. summary of exp.'

path_cropped_exp_with_bout_detection.mkdir(parents=True, exist_ok=True)


all_fish_raw_data_paths = [*Path(path_home).glob('*mp tail tracking.txt')]



for fish_path in tqdm(all_fish_raw_data_paths):
	# try:

	# To free up memory.
	gc.collect()

	stem_fish_path_orig = fish_path.stem.replace('mp tail tracking', '').lower()


	# if 'test' in stem_fish_path_orig:
	# 	continue

	pkl_name = str(path_orig_pkl / stem_fish_path_orig) + '.pkl'


	# raw_data_backup_name = str(path_to_save_backup / stem_fish_path_orig.lower()) + '_raw data' + '.pkl'

	print('\n\n' + stem_fish_path_orig)

	#! # Do nothing if pkl file already exists
	if not Over_write and Path(pkl_name).exists():
		print('Pkl with data already exists.')
		continue



	fig_camera_name_abs_time = str(path_lost_frames / stem_fish_path_orig)  + 'camera_abs_time.png'
	fig_camera_name_ela_time = str(path_lost_frames / stem_fish_path_orig)  + 'camera_ela_time.png'
	fig_protocol_name = str(path_summary_exp / stem_fish_path_orig) + '_protocol.png'
	fig_behavior_name = str(path_summary_beh / stem_fish_path_orig) + '_behavior.png'
	fig_cropped_exp_with_bout_detection_name = str(path_cropped_exp_with_bout_detection / stem_fish_path_orig) + '.html'

	#* Do nothing if pkl file already exists
	if Path(fig_camera_name_abs_time).exists():
		print('already exists.')
		continue


	# Paths of different data
	data_path = str(fish_path)
	protocol_path = data_path.replace('mp tail tracking', 'stim control')
	camera_path = data_path.replace('mp tail tracking', 'cam')
	# sync_reader_path = data_path.replace('mp tail tracking', 'scape sync reader')
	# eye_data_path = data.replace('mp tail tracking', 'flood fill eye tracking')

	day, strain, age, exp_type, rig, fish_number = f.fish_id(stem_fish_path_orig)

	# break
	# stem_fish_path

	#TODO explore sync reader file.
	# if ((sync_reader := f.read_sync_reader(sync_reader_path)) is None):
	# 	sync_reader.iloc[:,1:] -= sync_reader.iloc[:,1:].iloc[0]
	# 	f.framerate_and_reference_frame(sync_reader, first_frame_absolute_time, number_frames_discard_beg, protocol_info_path, stem_fish_path_orig, str(path_lost_frames / stem_fish_path_orig)  + 'sync_ela_time.png')


	
	# Read when the cam file, which contatins the processing time of each frame.
	# camera, predicted_framerate, reference_frame_id, reference_frame_time = f.framerate_and_reference_frame(camera_path, number_frames_discard_beg, max_interval_between_frames)
	#TODO Adapt for when there is no cam file. ADD MESSAGE TO INFO PROTOCOL
	if ((camera := f.read_camera(camera_path)) is None) or (abs_time not in camera.columns):
		first_frame_absolute_time = None
		# continue

	# elif (abs_time not in camera.columns):
	# 	first_frame_absolute_time = None
	# 	camera = camera.iloc[:,0:2]

	else:
		first_frame_absolute_time = camera.loc[:, abs_time].iloc[0]


	# if camera is None:
	# 	# Attention! 702 is what it is more or less, after calculating from the cam file (when available).
	# 	predicted_framerate = 702
	# 	reference_frame_time = 0
	# 	reference_frame_id = 0
		
	# else:

	camera.iloc[:,1:] -= camera.iloc[:,1:].iloc[0]

	# camera.iloc[:,0] = camera.iloc[:,0] - reference_frame_id

	# break
	



	#TODO Adapt for when there is no cam file
	#? possible to correct bad data?
	#* Look into the elapsed time column.
	print('Looking into the ElapsedTime column:')
	predicted_framerate, reference_frame_id, reference_frame_time, lost_f = f.framerate_and_reference_frame(camera.drop(columns=abs_time, errors='ignore'), first_frame_absolute_time, number_frames_discard_beg, protocol_info_path, stem_fish_path_orig, fig_camera_name_ela_time)


	if lost_f:
		del camera
		gc.collect()
		continue

	# del camera
	# gc.collect()


	# 'reference_frame_time if reference_frame_time is not None else reference_frame_id' because if there is no absolute time in the cam file, reference_frame_id is None and protocol contains information about the stimuli timings in number of frames and not absolute time.
	if (protocol := f.read_protocol(protocol_path, reference_frame_time if reference_frame_time is not None else reference_frame_id, protocol_info_path, stem_fish_path_orig)) is None:
		continue



	# stim_number = 'Stimulus number'

	# protocol[stim_number] = np.arange(len(protocol))
	# protocol.set_index(stim_number, append=True, inplace=True)

	if first_frame_absolute_time is None:
	# and camera is not None:

		if abs_time not in camera.columns:

			#TODO still to be finished
			# camera.iloc[:,0] = camera.iloc[:,0] - reference_frame_id
			# camera = camera.loc[camera.iloc[:,0] >= 0]

			# camera[abs_time] = np.arange(len(camera), dtype='int64') * 1000 / predicted_framerate + camera[ela_time].iloc[0]

			# for beg_end in [beg, end]:
				
			# 	protocol_ = protocol.loc[:,beg_end].reset_index().rename(columns={beg_end : camera.columns[0]})
			# 	camera_protocol = pd.merge_ordered(camera, protocol_).set_index(abs_time).interpolate(kind='slinear').reset_index()

			# 	protocol[beg_end] = camera_protocol[camera_protocol[experiment_type].notna()].set_index(experiment_type).loc[:,ela_time]



			# In case there is no information about the absolute time in the cam file, need to convert information in the protocol from number of frames to ms, using the predicted_framerate.
			protocol = protocol / predicted_framerate * 1000 # ms

	# elif first_frame_absolute_time is not None:
	else:

		#* Map stimuli timings of protocol (in unixtime) to ElapsedTime in camera. (Sometimes, the unixtime of the PC where the experiments are run gets updated during the experiment, creating a shift in the two ways of measuring time.)
		if camera.iloc[1:,2].notna().any():

			f.map_abs_time_to_elapsed_time(camera, protocol)

		else:

			if (camera[abs_time].diff() - 1000 / predicted_framerate).max() >= (buffer_size * 1000 / predicted_framerate):

				print('Cannot use these data because Unix time was updated during the experiment and only the absolute time of the first frame was saved.')
				
				continue



	# if exp_type != 'OC':
	number_cycles, number_reinforcers, _, _, _, habituation_duration, cs_dur, cs_isi, us_dur, us_isi = f.protocol_info(protocol)
	# else:
	# 	number_cycles, number_reinforcers, number_trials, number_sessions, number_bouts, habituation_duration, cs_dur, cs_isi, us_dur, us_isi = f.protocol_info(protocol)

	if f.lost_stim(number_cycles, number_reinforcers, min_number_cs_trials, min_number_us_trials, protocol_info_path, stem_fish_path_orig, 1):
		continue

	f.save_info(protocol_info_path, stem_fish_path_orig, ['', habituation_duration, np.min(cs_isi), np.min(cs_dur), np.max(cs_dur), np.min(us_isi), np.min(us_dur), np.max(us_dur), number_cycles, number_reinforcers])
	# '\t' + str(habituation_duration) + '\t' + str(np.min(cs_isi)) + '\t' + str(np.min(cs_dur)) + '\t' + str(np.max(cs_dur)) + '\t' + str(np.min(us_isi)) + '\t' + str(np.min(us_dur)) + '\t' + str(np.max(us_dur)) + '\t' + str(number_cycles) + '\t' + str(number_reinforcers))


	# Plot overview of the experimental protocol actually run
	f.plot_protocol(cs_dur, cs_isi, us_dur, us_isi, stem_fish_path_orig, fig_protocol_name)

	# if camera is not None:
	try:
		tracking_frames_to_discard = f.number_frames_discard(data_path, reference_frame_id)
	except:
		print('Tail tracking might be corrupted!')
		f.save_info(protocol_info_path, stem_fish_path_orig, 'Tail tracking might be corrupted!')
		continue


	# number_frames = int(predicted_framerate * (habituation_duration + max_interval_trials*(expected_number_cs-1) + time_aft_last_trial) * 60)

	# try:
	# 	del data
	# except:
	# 	pass
	gc.collect()

	#TODO try engine='pyarrow'
	if (data := f.read_tail_tracking_data(data_path, reference_frame_id)) is None:
		# number_frames
		f.save_info(protocol_info_path, stem_fish_path_orig, 'Tail tracking might be corrupted!')
		continue


	# Look for possible tail tracking errors
	if f.tracking_errors(data, single_point_tracking_error_thr):
		continue


	#* Interpolate data to expeted_framerate (700 FPS).
	# if camera is not None:
	data = f.interpolate_data(data, expected_framerate, predicted_framerate)

	del predicted_framerate

	# Save raw data in a backup.
		# if not Path(raw_data_backup_name).exists():
		# 	a=timer()
		# 	data.to_pickle(raw_data_backup_name)
		# 	b=timer()
		# 	print('Time to back up raw data in a pkl file: {} ms.'.format(b-a))

	
	# Read eye tracking data and merge eye_data and data dataframes
		# 	eye_data = pd.read_csv(eye_data_path, sep=' ', header=0, decimal=',')
		# 						# nrows=number_rows, skiprows=rows_to_skip, 
		# 	# eye_data.iloc[:, 0] = eye_data.iloc[:, 0].astype('int')
		# 	# eye_data.iloc[:, [1,2]] = eye_data.iloc[:,[1,2]].astype('float32')

		# 	eye_data.loc[:, 'FrameID'] -= reference_frame_time
			
		# 	# Update column names ('Rigth' is as it is spelled in the column names.)
		# 	eye_data.rename(columns=dict(zip(['FrameID', 'RigthEyeAngle', 'LeftEyeAngle'], [time_frame, right_eye_angle, left_eye_angle])), inplace=True)
		# 	# data.rename(columns=dict(zip(cols, [time_frame, angle_bef, angle, angle_aft, right_eye_angle, left_eye_angle])), inplace=True)

		# 	# Merge eye_data with data
		# 	data = data.set_index(time_frame).join(eye_data.set_index(time_frame)).reset_index()
			
		# 	# Convert eye tracking data from radian to degree
		# 	data.loc[:, [right_eye_angle, left_eye_angle]] *= 180/np.pi


	# Filter tail tracking data
	data = f.filter_data(data, space_bcf_window, time_bcf_window)


#! Doubt the angles are correct. Plus and minus almost 300 degrees?!?!?!? I got slightly more than 260 deg.
	print('Max tail angle at the chosen point: {} deg'.format(round(data.iloc[:,-1].max())))
	f.save_info(protocol_info_path, stem_fish_path_orig, 'Max tail angle at the chosen point: {} deg'.format(round(data.iloc[:,-1].max())))
	

	# Calculate 'activity_bout_detection'
	data = f.activity_for_bout_detection(data, chosen_tail_point, time_min_window, time_max_window)


	#* Convert protocol from ms to number of frames.
	#* This is not the real time at which the stimuli happened, but the time of the stimuli if the framerate had been the expected_framerate.
	protocol = (protocol * expected_framerate/1000).astype('int')

 
	data = f.stim_in_data(data, protocol, number_cycles, number_reinforcers, exp_type)

	f.plot_behavior_overview(data, stem_fish_path_orig, fig_behavior_name)

	if f.lost_stim(len(data[data[cs_beg]!=0]), len(data[data[us_beg]!=0]), min_number_cs_trials, min_number_us_trials, protocol_info_path, stem_fish_path_orig, 2):

		print(data[data[cs_beg]!=0])
		print(data[data[us_beg]!=0])

		print('break')
		#break

		continue



	#* time_bef_frame and time_aft_frame are for expected_framerate (700 FPS).
	data = f.extract_data_around_stimuli(data, protocol, time_bef_frame, time_aft_frame, time_bcf_window, time_max_window, time_min_window)
	

	data.iloc[:,0] = data.iloc[:,0] - data.iat[0,0]

	data = f.find_beg_and_end_of_bouts(data, bout_detection_thr_1, min_bout_duration, min_interbout_time, bout_detection_thr_2)

	f.plot_cropped_experiment(data, expected_framerate, bout_detection_thr_1, bout_detection_thr_2, downsampling_step, stem_fish_path_orig, fig_cropped_exp_with_bout_detection_name)

	data = f.clean_data(data)


	# Calculate tail activity.
	data[activity] = data.iloc[:,1:2+chosen_tail_point].diff().abs().sum(axis=1) * (expected_framerate / 1000)
	# data = f.calculate_tail_activity(data, cols, chosen_tail_point, predicted_framerate)


	# Set the columns' dtypes.
	data.loc[:, cols[1:]] = data.loc[:, cols[1:]].astype(pd.SparseDtype('float32', np.nan))
	data.loc[:, activity] = data.loc[:, activity].astype(pd.SparseDtype('float32', 0))


	data = f.identify_trials(data, expected_framerate, time_bef_frame, time_aft_frame)


	#* Order the columns.
	data = data[cols_ordered]


	#* Set the columns' dtypes.
	data.loc[:, cols[1:]] = data.loc[:, cols[1:]].astype(pd.SparseDtype('float32', np.nan))
	data.loc[:, time_trial_csus] = data.loc[:, time_trial_csus].astype('int')
	# data.loc[:, cols_stim[-2:]] = data.loc[:, cols_stim[-2:]].astype(CategoricalDtype(ordered=True))
	# data.loc[:, cs_beg] = data.loc[:, cs_beg].astype(CategoricalDtype(categories=data[cs_beg].unique().sort(), ordered=True))	
	# data.loc[:, us_beg] = data.loc[:, us_beg].astype(CategoricalDtype(categories=data[us_beg].unique().sort(), ordered=True))
	# data.loc[:, cs_end] = data.loc[:, cs_end].astype(CategoricalDtype(categories=data[cs_end].unique().sort(), ordered=True))
	# data.loc[:, us_end] = data.loc[:, us_end].astype(CategoricalDtype(categories=data[us_end].unique().sort(), ordered=True))
	data.loc[:, number_trial_csus] = data.loc[:, number_trial_csus].astype(CategoricalDtype(categories=np.sort(data[number_trial_csus].unique()), ordered=True))
	data.loc[:, activity] = data.loc[:, activity].astype(pd.SparseDtype('float32', 0))

	# data.loc[:, number_trial_csus] = data.loc[:, number_trial_csus].astype('int')
	# data.loc[:, number_trial_csus].dtype


	#* Set the index.
	data['Exp.'] = exp_type
	data['ProtocolRig'] = rig
	data['Age (dpf)'] = age
	data['Day'] = day
	data['Fish no.'] = fish_number
	data['Strain'] = strain

	#* Change to a categorical index.
	data.loc[:, ['Exp.', 'ProtocolRig', 'Age (dpf)', 'Day', 'Fish no.', 'Strain']] = data.loc[:, ['Exp.', 'ProtocolRig', 'Age (dpf)', 'Day', 'Fish no.', 'Strain']].astype('category')


	if exp_type == 'OC':
		
		data.set_index(keys=['Session', trial, 'Cycle'], inplace=True, drop=True, append=True)

	data.set_index(keys=['Strain', 'Age (dpf)', 'Exp.', 'ProtocolRig', 'Day', 'Fish no.'],inplace=True)


	#* Save as a pickle file.
	data.to_pickle(pkl_name)


	del data

	gc.collect()
	# except:
	# 	pass
	
	# break

print('FINISHED')
exec(open('F 1 analysis of single fish.py').read())
