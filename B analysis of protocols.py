# %%
import numpy as np
import pandas as pd
from pathlib import Path
import pandas as pd
import matplotlib.pyplot as plt

# import protocol_maker as pm

pd.set_option('mode.chained_assignment', None)


#! This has to match what is in the protocol.
time_bef_first_stim = 60000 # ms
habituation_numb_trials = 3
pre_condit_numb_trials = 10 # without US
post_condit_numb_trials = 10 # without US

frmt = 'png'

path_home = Path(r'C:\Users\joaqc\Desktop\protocols')

protocol_names = ['Control']
# ['Control-1', 'Control-2']
# 'Test', 
time = 'Time'
isi = 'ISI'
cs_us = 'CS/US'

all_data_paths = [*Path(path_home).glob('*.txt')]

low_high = 2 # min

t_axis_2 = (-low_high, low_high)
bins_2 = np.arange(t_axis_2[0], t_axis_2[1], 1/12)

# t_axis = (-time_bef_ms/1000, time_aft_ms/1000)
# bins = np.arange(t_axis[0], t_axis[1], 0.5)

# for path in reversed(all_data_paths):

def t_axis_bins_1(data):

	t_axis = (0, data[time].max())
	bins = np.arange(t_axis[0], t_axis[-1], 1/12)

	return t_axis, bins

def plot_1(cs_beg, us_beg, t_axis, bins, name):

	fig_1, axs_1 = plt.subplots(constrained_layout=True, figsize=(20,5))

	axs_1.hist(cs_beg, bins=bins, range=t_axis, density=True, histtype='bar', align='left', color='green', alpha=0.5, label='CS onsets')
	axs_1.hist(us_beg, bins=bins, range=t_axis, density=True, histtype='bar', align='left', color='red', alpha=0.5, label='US onsets')

	axs_1.spines['left'].set_visible(False)
	axs_1.spines['right'].set_visible(False)
	axs_1.spines['top'].set_visible(False)
	axs_1.tick_params(axis='both', which='both', bottom=True, top=False, right=False, direction='out')
	axs_1.axes.get_yaxis().set_visible(False)

	axs_1.set_xlabel('Time (min)')
	fig_1.legend()
	fig_1.suptitle(name)
	# fig_1.show()

	fig_1.savefig(str(path_home / (name + '_' + 'all trials together')) + '.svg', dpi=100, facecolor='white')

def plot_2(us_relative_to_cs_onset, cs_relative_to_us_onset, t_axis, bins, name):

	fig_2, axs_2 = plt.subplots(constrained_layout=True, figsize=(20,5))

	axs_2.hist(cs_relative_to_us_onset, bins=bins, range=t_axis, density=True, histtype='bar', align='left', color='green', alpha=0.5, label='CS onsets relative to US onset')
	axs_2.hist(us_relative_to_cs_onset, bins=bins, range=t_axis, density=True, histtype='bar', align='left', color='red', alpha=0.5, label='US onsets relative to CS onset')

	axs_2.spines['left'].set_visible(False)
	axs_2.spines['right'].set_visible(False)
	axs_2.spines['top'].set_visible(False)
	axs_2.tick_params(axis='both', which='both', bottom=True, top=False, right=False, direction='out')
	axs_2.axes.get_yaxis().set_visible(False)

	axs_2.set_xlabel('Time (min)')
	fig_2.legend()
	fig_2.suptitle(name)
	
	fig_2.savefig(str(path_home / (name + '_' + 'all trials, relative to each other')) + '.png', dpi=100, facecolor='white')

def stim_time_relations(cs_beg, us_beg):

	us_relative_to_cs_onset = []
	cs_relative_to_us_onset = []

	for stim in cs_beg:

		tmp = us_beg - stim
		us_relative_to_cs_onset += list(tmp[abs(tmp) <= low_high])

	for stim in us_beg:

		tmp = cs_beg - stim
		cs_relative_to_us_onset += list(tmp[abs(tmp) <= low_high])

	return us_relative_to_cs_onset, cs_relative_to_us_onset

all_data = []
all_data_ = []
cs_beg = [*range(len(protocol_names))]
us_beg = [*range(len(protocol_names))]
# cs_beg = np.arange(len(protocol_names))
# us_beg = np.arange(len(protocol_names))

us_relative_to_cs_onset = [*range(len(protocol_names))]
cs_relative_to_us_onset = [*range(len(protocol_names))]
# us_relative_to_cs_onset = np.arange(len(protocol_names))
# cs_relative_to_us_onset = np.arange(len(protocol_names))

for protocol_type_i, protocol_type in enumerate(protocol_names):

	# break

	protocol_type_paths = [path for path in all_data_paths if protocol_type == path.stem.split('_')[0]]

	data = [*range(len(protocol_type_paths))]

	#* data_ will only contain the conditioning blocks.
	data_ = [*range(len(protocol_type_paths))]

	for path_i, path in enumerate(protocol_type_paths):

		# break

		data[path_i] = pd.read_csv(str(path), sep='\t', skiprows=[1], usecols=[1,5,7], dtype='int')

		# print(data[path_i][isi].min())
		
		#* Calculate the onset time of each stimulus.
		data[path_i].loc[:, time] = [time_bef_first_stim] + (time_bef_first_stim + data[path_i].loc[:, isi].cumsum()).to_list()[:-1]		
		data[path_i].drop(columns=['ReinfOnset', isi], inplace=True)
		data[path_i].loc[:, time] /= (1000*60)
		
		data_[path_i] = data[path_i].iloc[habituation_numb_trials + pre_condit_numb_trials : -post_condit_numb_trials + 1, :]

		# break

	data = pd.concat(data)
	data_ = pd.concat(data_)

	#* CS and US times
	cs_beg[protocol_type_i] = data.loc[data[cs_us]==0, time].to_numpy()
	us_beg[protocol_type_i] = data.loc[data[cs_us]==2, time].to_numpy()
	
	t_axis_1, bins_1 = t_axis_bins_1(data)

	plot_1(cs_beg[protocol_type_i], us_beg[protocol_type_i], t_axis_1, bins_1, protocol_type)

	# all_data += [data]
	# all_data_ += [data_]

	us_relative_to_cs_onset[protocol_type_i], cs_relative_to_us_onset[protocol_type_i] = stim_time_relations(data_.loc[data_[cs_us]==0, time].to_numpy(), data_.loc[data_[cs_us]==2, time].to_numpy())

	plot_2(us_relative_to_cs_onset[protocol_type_i], cs_relative_to_us_onset[protocol_type_i], t_axis_2, bins_2, protocol_type)

# all_data = pd.concat(all_data)
# all_data_ = pd.concat(all_data_)

# cs_beg = np.concatenate(cs_beg)
# us_beg = np.concatenate(us_beg)
# t_axis_1, bins_1 = t_axis_bins_1(all_data)

# plot_1(cs_beg, us_beg, t_axis_1, bins_1, 'Both controls together')	

# us_relative_to_cs_onset = np.concatenate(us_relative_to_cs_onset)
# cs_relative_to_us_onset = np.concatenate(cs_relative_to_us_onset)

# plot_2(us_relative_to_cs_onset, cs_relative_to_us_onset, t_axis_2, bins_2, 'Both controls together')
