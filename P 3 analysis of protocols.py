# 4. Calculate the statistics of the pooled data_csus.
# Calculate the statistics of pooled data_csus in single trials.


# %%
# %reset -f
from pathlib import Path
import pandas as pd
import seaborn as sns
from gc import collect
from pandas.api.types import CategoricalDtype

from my_general_variables import *
from my_experiment_specific_variables import *

# import my_functions_P_2 as f
import my_functions_F_1 as f


import importlib
importlib.reload(f)

pd.set_option('mode.chained_assignment', None)

frmt = 'png'

# Path to save processed data; create folder to save processed data if it does not exist yet.
path_to_save_proc_data = path_home / 'Processed data'
# path_to_save_proc_data.mkdir(parents=True, exist_ok=True)

path_pooled_data = path_to_save_proc_data / 'pkl files' / 'Pooled data by condition'

path_part_to_save = Path(r'C:\Experiments_to copy\Processed data\pkl files\Pooled data by condition\Analysis of protocols')
# path_part_to_save = path_pooled_data / 'Analysis of protocols'
# path_part_to_save.mkdir(parents=True, exist_ok=True)

all_data_paths = [*Path(path_part_to_save).glob('*.pkl')]


t_axis = (-time_bef_ms/1000, time_aft_ms/1000)

bins = np.arange(t_axis[0], t_axis[1], 0.5)


for path in reversed(all_data_paths):

	print(path.stem, '\n\n')

	data = pd.read_pickle(str(path)).iloc[:,:5]
	# .reset_index(drop=True)

	stim = f.findStim(data)
	stim = [e / expected_framerate for e in stim] # s

	fig, axs = plt.subplots()

	axs.hist(stim[0], bins=bins, range=t_axis, density=True, histtype='step', align='left', color='green', label='CS beg')
	# plt.hist(stim[1]/1000, bins=bins, range=t_axis, density=True, histtype='step')
	axs.hist(stim[2], bins=bins, range=t_axis, density=True, histtype='step', align='left', color='red', label='US beg')
	# plt.hist(stim[3]/1000, bins=bins, range=t_axis, density=True, histtype='step')

	axs.spines[:].set_visible(False)
	axs.axes.xaxis.set_ticks(np.arange(t_axis[0], t_axis[1], 5))
	axs.tick_params(axis='both', which='both', bottom=True, top=False, right=False, direction='out')
	axs.set_xlim(t_axis)
	axs.set_xlabel('t (s)')
	axs.legend(loc='best', borderaxespad=0, frameon=False)
	# bbox_to_anchor=(1, 1.05),

	fig.suptitle(path.stem)
	# fig.show()

	# break

	fig.savefig(str(path_part_to_save/path.stem) + ' analysis of protocols.' + frmt, format=frmt, facecolor='white')
	# break

