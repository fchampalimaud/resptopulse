# %%
# %reset-f

import numpy as np
import pandas as pd
from pathlib import Path
import matplotlib.pyplot as plt
from pathlib import Path
import random

import warnings
warnings.filterwarnings("ignore", category=np.VisibleDeprecationWarning)

from pandas.core.common import SettingWithCopyWarning
warnings.simplefilter(action="ignore", category=SettingWithCopyWarning)

path_home = Path(r'C:\Users\joaqc\Desktop\protocols')


protocol_numb = 10
us_latency_delay = 3000  # relative to the CS onset.
us_latency_trace = 2500  # relative to the CS offset.

#* Time before the first stimulus.
time_bef_first_stim = 60000 # 1 min

habituation_time = 600000 # 10 min

#* ISI needs to be always substantially larger than the duration of the stimulus.
interv_bet_stim_onsets_test_low = int(2.5 * 60000)
interv_bet_stim_onsets_test_high = int(3.5 * 60000)

interv_bet_first_stim_onsets_control = int(60000)

#* This ensures a min ISI of 3 min - 2 * 15 s = 2.5 min and a max ISIof 3 min + 2 * 15 s = 3.5 min.

#! CHECK
interv_bef_last_stim = 15000
min_interv_bet_cs = 19000 # Has to be larger than cs_duration
min_interv_bet_any_stim = 500
habituation_numb_trials = 3
pre_condit_numb_trials = 10 # without US
condit_numb_trials = 50
post_condit_numb_trials = 10 # without US

#* Duration of the CS.
#! Needs to match what is in the Arduino code.
cs_duration = 333*12
us_duration = 100
first_us_duration = int(us_duration/2)
last_us_duration = int(2*us_duration)
us_power = [100, 77, 80, 77]
reps = 1

setups = ['orange', 'brown', 'blue', 'black']
expType = 'CC'
trial_numb = 'TrialNo'
cs_us = 'CS/US'
l_r = 'L/R'
us_duration_name = 'ReinfDur'
us_power_name = 'ReinfPower'
us_onset_name = 'ReinfOnset'
reps_name = '#RepsByArd'
interstim_interv_relative_to_stim_onsets = 'ISI'
time = 'Time'
protocol_names = ['Delay', 'Trace', 'Control']

# experiment_type = str(us_latency_delay/1000 - 4) + ' s'
numb_columns = 8
interv_bet_stim_onsets_test_mean = int((interv_bet_stim_onsets_test_low + interv_bet_stim_onsets_test_high) / 2)

low_high = 2 # min
t_axis = (-low_high, low_high)
bins = np.arange(t_axis[0], t_axis[1], 1/12)


interstim_interv_relative_to_stim_onsets_habituation = int((habituation_time - time_bef_first_stim)/habituation_numb_trials)


#* Define the common part to all protocols (habituation period).

##* Initial part of the experiment.
protocol_beg = pd.DataFrame(np.zeros((1 + habituation_numb_trials, numb_columns)), columns=[trial_numb, cs_us, l_r, us_duration_name, us_power_name, us_onset_name, reps_name, interstim_interv_relative_to_stim_onsets], dtype='uint')

protocol_beg.loc[0, trial_numb] = time_bef_first_stim
protocol_beg.iloc[0, 1:] = ''

protocol_beg.loc[protocol_beg.index[1:], [l_r, reps_name, interstim_interv_relative_to_stim_onsets]] = [1, 1, interstim_interv_relative_to_stim_onsets_habituation]

###* US
protocol_beg.loc[1, [cs_us, us_duration_name]] = [2, first_us_duration] # Half of the US duration in the conditioning block to make sure US is very salient there.

###* Correct ISI in the last stimulus in the habituation. (NO NEED FOR NOW.)
# protocol_beg.loc[1:, interstimulus_interv].iloc[-1] = habituation_time - time_bef_first_stim - protocol_beg.loc[1:habituation_numb_trials-1, interstimulus_interv].sum()
# protocol_beg.loc[1:, interstimulus_interv].sum() + time_bef_first_stim

###* Name this block.
protocol_beg['Block'] = 'Habituation'
protocol_beg.set_index('Block', append=True, inplace=True)


##* Last part of the experiment.
protocol_end = pd.DataFrame(np.zeros((1, numb_columns)), columns=[trial_numb, cs_us, l_r, us_duration_name, us_power_name, us_onset_name, reps_name, interstim_interv_relative_to_stim_onsets], dtype='uint')

protocol_end.loc[:, [cs_us, us_duration_name, l_r, reps_name, interstim_interv_relative_to_stim_onsets]] = [2, last_us_duration, 1, 1, 60000]

protocol_end['Block'] = 'Last stim.'
protocol_end.set_index('Block', append=True, inplace=True)


def initialize_piece_of_protocol(numb_trials, block_name):

	protocol = pd.DataFrame(np.zeros((numb_trials, numb_columns)), columns=[trial_numb, cs_us, l_r, us_duration_name, us_power_name, us_onset_name, reps_name, interstim_interv_relative_to_stim_onsets], dtype='uint')

	protocol.loc[:, [l_r, reps_name, interstim_interv_relative_to_stim_onsets]] = [1, 1, np.random.randint(interv_bet_stim_onsets_test_low, interv_bet_stim_onsets_test_high, size=numb_trials)]

	protocol['Block'] = block_name

	protocol.set_index('Block', append=True, inplace=True)

	return protocol

def randomize_time_stim(protocol, min_interv_bet_cs):

	while True:

		time_ = np.sort(random.sample(list(range(protocol.loc[protocol.index[0], time] + np.random.randint(-interv_bet_first_stim_onsets_control, interv_bet_first_stim_onsets_control), protocol.loc[protocol.index[-1], time] + np.random.randint(-interv_bet_first_stim_onsets_control, interv_bet_first_stim_onsets_control))), k=len(protocol)))

		if all(np.diff(time_) > min_interv_bet_cs):

			return time_

def new_control(protocol_beg, protocol_pre, protocol_control_cs, protocol_control_us, protocol_post, protocol_end, min_interv_bet_any_stim):

	#* Define the conditioning block in control 1.
	protocol_control = pd.concat([protocol_control_cs, protocol_control_us]).sort_values(by=time).droplevel(0).reset_index(drop=False).set_index('Block', append=True)

	#* This "if" also checks if it is below 0.
	if any(protocol_control[time].diff() < min_interv_bet_any_stim):

		return None

	#* Concatenate with pre- and post-conditioning blocks, sorting values by the time column.
	protocol_control = pd.concat([protocol_pre, protocol_control, protocol_post, protocol_end])

	#* Correct the trial numbers.
	protocol_control.loc[:, trial_numb] = np.arange(1, len(protocol_control) + 1)

	#* interstimulus_interv
	ind = protocol_control.iloc[pre_condit_numb_trials-1 : -post_condit_numb_trials].index
	protocol_control.loc[ind[:-1], interstim_interv_relative_to_stim_onsets] = protocol_control.loc[ind, time].diff()[1:].astype('int').to_numpy()

	#* Concatenate with protocol_beg.
	protocol_control = pd.concat([protocol_beg, protocol_control])

	return protocol_control


for num in range(int(protocol_numb/len(setups))):

	for setup_i, setup in enumerate(setups):

		#* Define what is still missing in the first part of the protocol because it depends on the setup (us_power).
		protocol_beg.loc[protocol_beg[cs_us]==2, us_power_name] = us_power[setup_i]

		protocol_beg.loc[protocol_beg.index[-1], interstim_interv_relative_to_stim_onsets] = np.random.randint(max(interstim_interv_relative_to_stim_onsets_habituation, interv_bet_stim_onsets_test_low), interv_bet_stim_onsets_test_high)

		#* Test condition protocols.

		##* Pre-conditioning.
		protocol_pre = initialize_piece_of_protocol(pre_condit_numb_trials, 'Pre-conditioning')

		##* Post-conditioning.
		protocol_post = initialize_piece_of_protocol(post_condit_numb_trials, 'Post-conditioning')
		protocol_post.loc[protocol_post.index[-1], interstim_interv_relative_to_stim_onsets] = interv_bef_last_stim

		##* Conditioning block.
		protocol_cond = initialize_piece_of_protocol(condit_numb_trials, 'Conditioning')
		protocol_cond.loc[:, [cs_us, us_duration_name, us_power_name, us_onset_name]] = [1, us_duration, us_power[setup_i], us_latency_delay]

		##* Define the part of the procotol that is unique to each experiment.
		protocol_test_delay = pd.concat([protocol_pre, protocol_cond, protocol_post])

		###* Number the stimuli.
		protocol_test_delay.loc[:, trial_numb] = np.arange(1, len(protocol_test_delay) + 1)

		###* Final delay protocol.
		protocol_test_delay = pd.concat([protocol_beg, protocol_test_delay, protocol_end])

		###* Final trace protocol.
		protocol_test_trace = protocol_test_delay.copy()
		protocol_test_trace.loc[protocol_test_trace.index.get_level_values('Block') == 'Conditioning', us_onset_name] = cs_duration + us_latency_trace

		#* Control protocols, in which CS happen as in the test protocol.

		##* Calculate the time of each stimulus onset.		
		protocol_test_delay.loc[:, time] = [0, time_bef_first_stim] + (protocol_test_delay[interstim_interv_relative_to_stim_onsets][1:-1].cumsum() + time_bef_first_stim).to_list()

		protocol_test_trace.loc[:, time] = [0, time_bef_first_stim] + (protocol_test_trace[interstim_interv_relative_to_stim_onsets][1:-1].cumsum() + time_bef_first_stim).to_list()

		##* Update pre- and post-conditioning dataframes.
		protocol_beg = protocol_test_delay.xs('Habituation', level='Block', drop_level=False)
		protocol_pre = protocol_test_delay.xs('Pre-conditioning', level='Block', drop_level=False)
		protocol_post = protocol_test_delay.xs('Post-conditioning', level='Block', drop_level=False)
		protocol_end = protocol_test_delay.xs('Last stim.', level='Block', drop_level=False)

		##* Piece of protocol_test_delay that needs to be changed to make the controls.
		protocol_control_cs = protocol_test_delay.xs('Conditioning', level='Block', drop_level=False)
		protocol_control_us = protocol_control_cs.copy()

		###* CS
		protocol_control_cs.loc[:, [cs_us, us_duration_name, us_power_name, us_onset_name]] = [0,0,0,0]

		###* US
		protocol_control_us.loc[:, [cs_us, us_onset_name]] = [2,0]
		protocol_control_us.loc[:, time] += us_latency_delay

	# 	break
	# break

		#* Define the timing of the CS in the control protocol.
		while True:

			protocol_control_cs.loc[:, time] = randomize_time_stim(protocol_control_cs, min_interv_bet_cs)

			if (protocol_control := new_control(protocol_beg, protocol_pre, protocol_control_cs, protocol_control_us, protocol_post, protocol_end, min_interv_bet_any_stim)) is not None:

				break


		#* Plots of the protocols.
		fig_1, axs_1 = plt.subplots(3,1, sharex=True, constrained_layout=True, figsize=(20,5))
		fig_2, axs_2 = plt.subplots(3,2, sharex=True, constrained_layout=True, figsize=(15,7))

		for p_i, p in enumerate([protocol_test_delay, protocol_test_trace, protocol_control]):

			#* Save the protocols.
			match p_i:
				case 0:
					experiment_type = str(us_latency_delay/1000 - 4) + ' s'
				case 1:
					experiment_type = str(us_latency_trace/1000) + ' s'
				case 2:
					experiment_type = '-'

			p.drop(columns=time).to_csv(str(path_home / (protocol_names[p_i] + '_' + experiment_type + '_' + setup + '_' + str(num+1))) + '.txt', sep='\t', index=False)

			p_ = p.copy()

			# #! This is redundant but it is to check that the time was correctly calculated.
			# p_[time] = [0, time_bef_first_stim] + (p_.loc[p_.index[1:], interstim_interv_relative_to_stim_onsets][:-1].cumsum() + time_bef_first_stim).to_list()
			# print(p[time] - p_[time])

			p_.loc[p_.index[1:], time] /= (1000*60)

			#* CS times
			cs_beg = p_.loc[p_[cs_us].isin([0,1]), time]

			#* US times
			us_beg = pd.concat((p_.loc[p_[cs_us]==1, time] + us_latency_delay/(1000*60), p_.loc[p_[cs_us]==2, time]))

			#* Figure 1.		
			axs_1[p_i].eventplot(cs_beg, color='green', label='CS' if p_i==0 else '')
			axs_1[p_i].eventplot(us_beg, color='red', label='US' if p_i==0 else '')
			axs_1[p_i].axes.get_yaxis().set_visible(False)
			axs_1[p_i].set_title(protocol_names[p_i])

			#* Figure 2.
			cs_beg = cs_beg.droplevel(0).loc['Conditioning'].reset_index(drop=True)
			us_beg = us_beg.droplevel(0).loc['Conditioning'].reset_index(drop=True)


			# cs_beg_ = cs_beg.copy()
			# for i in range(int(len(cs_beg)/10)):

				# cs_beg = cs_beg_[i*10:(i+1)*10].copy()

			us_relative_to_cs_onset = []
			cs_relative_to_us_onset = []

			for stim_i, stim in enumerate(cs_beg):

				tmp = us_beg - stim
				us_relative_to_cs_onset += list(tmp[abs(tmp) <= low_high])

			for stim_i, stim in enumerate(us_beg):

				tmp = cs_beg - stim
				cs_relative_to_us_onset += list(tmp[abs(tmp) <= low_high])

			axs_2[p_i,0].hist(cs_relative_to_us_onset, bins=bins, range=t_axis, density=True, histtype='bar', align='left', color='green', alpha=0.5, label='CS onsets relative to US onset' if p_i==0 else '')
			axs_2[p_i,1].hist(us_relative_to_cs_onset, bins=bins, range=t_axis, density=True, histtype='bar', align='left', color='red', alpha=0.5, label='US onsets relative to CS onset' if p_i==0 else '')

			for c in [0,1]:
				axs_2[p_i,c].spines['left'].set_visible(False)
				axs_2[p_i,c].spines['right'].set_visible(False)
				axs_2[p_i,c].spines['top'].set_visible(False)
				axs_2[p_i,c].tick_params(axis='both', which='both', bottom=True, top=False, right=False, direction='out')
				axs_2[p_i,c].axes.get_yaxis().set_visible(False)
				axs_2[p_i,c].set_title(protocol_names[p_i])

				# break

		
		#* Figure 1.
		axs_1[2].set_xlabel('Time (min)')
		fig_1.legend()
		fig_1.suptitle('Total duration: ' + str(round(p_.loc[:, time].iloc[-1]/60, 2)) + ' h')

		fig_1.savefig(str(path_home / (setup + '_' + str(num+1))) + '.svg', dpi=100, facecolor='white')

		#* Figure 2.
		axs_2[2,0].set_xlabel('Time (min)')
		axs_2[2,1].set_xlabel('Time (min)')
		fig_2.legend()
		fig_2.suptitle('Conditioning block')

		fig_2.savefig(str(path_home / (setup + '_' + str(num+1) + '_relative to each other')) + '.png', dpi=100, facecolor='white')


		# plt.close('all')


	# 	break
	# break
# %%
