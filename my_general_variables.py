from os import name
from typing import Final
import numpy as np
import math
import matplotlib.pyplot as plt


#* Acronyms
'''
	cs. conditined stimulus or conditioned stumili
	us. unconditined stimulus or unconditioned stumili

	cr. conditioned response

	bef. before
	aft. after

	beg. beginning

	min. [at the beginning] minimum
	max. maximum

	thr. threshold

	bcf. boxcar filter


	ms. miliseconds
	s. seconds
	min. [at the end] minutes

	id. identification

	abs. absolute

	stim. stimulus or stimuli

	ifi. interframe interval

'''



#* Parameters for the analysis

# Point of the tail chosen as the last one to be considered in the analysis, using a tracking with 15 points on the tail.
chosen_tail_point: Final = 16 - 3

# To define the whole extent of trials, i.e., regions in time before and after a stimulus.
time_bef_ms: Final = 45000 # ms
time_aft_ms: Final = 45000 # ms

# To crop data, before pooling.
t_crop_data_bef_s = 15 #s
t_crop_data_aft_s = 15 #s

# To check whether any frame was lost.
number_frames_discard_beg: Final = 30*700 # around 0.5 min at 700 FPS (number of frames)
buffer_size: Final = 700 # frames
max_interval_between_frames: Final = 0.005 # ms
lag_thr: Final = 50 # ms

# To check for tracking errors.
single_point_tracking_error_thr: Final = 2 * 180/np.pi # deg

# Parameteres for filtering raw data.
#! Need to add here the units.
#? If you change this, you will have to change the rest of the code.
# All below in number of frames at expected_framerate (700 FPS)
space_bcf_window: Final = 3 # number segments of the tail
time_bcf_window: Final = 10
time_max_window: Final = 20
time_min_window: Final = 400
#center_window = True
filtering_window: Final = 350 # frames

# Parameters for bout detection.
bout_detection_thr_1: Final = 1 # deg/ms
min_interbout_time: Final = 10 # frames
min_bout_duration: Final = 40 # frames
bout_detection_thr_2: Final = 1 # deg/ms

# Interpolate data to this framerate.
expected_framerate: Final = 700 # FPS

# After interpolating, time_bef and time_aft can be used in number of frames.
time_bef_frame: Final = int(np.ceil(time_bef_ms * expected_framerate/1000)) # frames
time_aft_frame: Final = int(np.ceil(time_aft_ms * expected_framerate/1000)) # frames


# Width of the bins used to group bout_beg and bout_end data, in s.
bin_interval = 0.5 # s


#* Parameters for plotting data

# Donwsampling step to plot the whole experimental data, using plot_cropped_experiment function.
downsampling_step: Final = 5

baseline_window = 15 # s


#* Variables containing strings

experiment_type = 'experiment type'
beg: Final = 'beg (ms)'
end: Final = 'end (ms)'

abs_time = 'AbsoluteTime'
ela_time = 'ElapsedTime'


cs: Final = 'CS'
us: Final = 'US'

cs_beg: Final = 'CS beg'
cs_end: Final = 'CS end'
us_beg: Final = 'US beg'
us_end: Final = 'US end'

activity_bout_detection: Final = 'Activity for bout detection (deg/ms)'
activity: Final = 'Activity (deg/ms)'
bout: Final = 'Bout'
bout_beg: Final = 'Bout beg'
bout_end: Final = 'Bout end'

bout_bef: Final = bout + ' before'
bout_aft: Final = bout + ' after'
bout_diff: Final = bout + ' difference'

# bout_beg_end: Final = ' Bout beg and end'
activity_mean: Final = activity + ' mean'
activity_sem: Final = activity + ' sem'
# normalized_activity = 'Normalized activity'
bout_mean: Final = bout + ' mean'
# bout_sem = bout + ' sem'
# bout_beg_sum: Final = bout_beg + ' sum'
# bout_end_sum: Final = ('Bout end') + ' sum'
bout_beg_mean: Final = bout_beg + ' mean'
# bout_beg_sem = bout_beg + ' sem'
bout_end_mean: Final = bout_end + ' mean'
# bout_end_sem = bout_end + ' sem'
count = 'Count'
trial = 'Trial'
number_trial_csus = 'Trial number'
type_trial_csus = 'Trial type'
#! Change this name!
time_trial_csus = 'Trial time (frame) [700 FPS]'
number_session = 'Session number'

color = 'color'
name = 'name'


control = 'control'
delay = 'delay'
delayNoOpt = 'delaynoopt'
trace = 'trace'

# control_1 = 'delay_control-1'
# control_2 = 'delay_control-2'
# test = 'delay_test'
# trace500 = 'trace500CC'
# trace1500 = 'trace1500CC'
# trace2500 = 'trace2500CC'




sessions_1t = 'single trials'
sessions_10t = 'sessions 10 trials'
# sessions_30t = 'sessions 30 trials'
# sessions_45t = 'sessions 45 trials'


trials_sessions = 'trials in each session'
names_sessions = 'names of sessions'
id_in_path = 'name in original path'

number_cols_or_rows = 'number of cols or rows'

horizontal_fig = 'horizontal'
fig_size = 'figure size'

us_latency = 'US latency'


folder_name = 'folder name'


# , bout_beg, bout_end]
# metrics = [bout_mean, bout_beg_mean, bout_end_mean]

# x_label = 'x label'
x_label = '\nt (s)'




function_add_data = 'function to add data to plot'
y_label = 'y label'
y_lim = 'y limit'
y_ticks = 'y ticks'




tail_angle = 'Angle of point {} (deg)'.format(chosen_tail_point)


cols_to_use_orig = ['FrameID']
for i in range(chosen_tail_point+int(math.floor(space_bcf_window/2))):
	cols_to_use_orig.append('angle' + str(i))

#! Remove FrameID from cols!
cols = [0] * len(cols_to_use_orig)
cols[1:] = ['Angle of point {} (deg)'.format(i) for i in range(len(cols)-1)]

cols_stim = [cs_beg, cs_end, us_beg, us_end, type_trial_csus, number_trial_csus]

cols_bout = [bout_beg, bout_end, bout]

# cols_stats = [activity_mean, activity_sem, bout_mean, bout_sem, bout_beg_mean, bout_beg_sem, bout_end_mean, bout_end_sem]
cols_stats = [activity_mean, bout_mean, bout_beg_mean, bout_end_mean]


cols_ordered = [[time_trial_csus], cols_stim, cols[1:], [activity], cols_bout]
cols_ordered = [i for j in cols_ordered for i in j]



metrics_dict = {}
metrics_single_trials_dict = {}


metrics_single_fish = [tail_angle, bout, bout_beg, bout_end]
# [bout]


if tail_angle in metrics_single_fish:
	metrics_single_trials_dict[tail_angle] = {}
	metrics_single_trials_dict[tail_angle][folder_name] = '2.0. single fish angle'
	metrics_single_trials_dict[tail_angle][y_label] = 'Tail end angle (deg)'
	metrics_single_trials_dict[tail_angle][y_lim] = (-180,180)
	metrics_single_trials_dict[tail_angle][y_ticks] = np.arange(0,-120,120)

if bout in metrics_single_fish:
	metrics_single_trials_dict[bout] = {}
	metrics_single_trials_dict[bout][folder_name] = '2.2. single fish bouts'
	metrics_single_trials_dict[bout][y_label] = 'Tail movement'
	metrics_single_trials_dict[bout][y_lim] = (None,None)
	metrics_single_trials_dict[bout][y_ticks] = []

if bout_beg in metrics_single_fish:
	metrics_single_trials_dict[bout_beg] = {}
	metrics_single_trials_dict[bout_beg][folder_name] = '2.3. single fish beg bouts'
	metrics_single_trials_dict[bout_beg][y_label] = 'Start of tail movement'
	metrics_single_trials_dict[bout_beg][y_lim] = (None,None)
	metrics_single_trials_dict[bout_beg][y_ticks] = []

if bout_end in metrics_single_fish:
	metrics_single_trials_dict[bout_end] = {}
	metrics_single_trials_dict[bout_end][folder_name] = '2.4. single fish end bouts'
	metrics_single_trials_dict[bout_end][y_label] = 'End of tail movement'
	metrics_single_trials_dict[bout_end][y_lim] = (None,None)
	metrics_single_trials_dict[bout_end][y_ticks] = []


	
metrics = [bout, bout_beg, bout_end]


if activity in metrics:
	metrics_dict[activity] = {}
	metrics_dict[activity][folder_name] = '3.1. pooled fish activity'

if bout in metrics:
	metrics_dict[bout] = {}
	metrics_dict[bout][folder_name] = '3.2. pooled fish bouts'

if bout_beg in metrics:
	metrics_dict[bout_beg] = {}
	metrics_dict[bout_beg][folder_name] = '3.3. pooled fish beg bouts'

if bout_end in metrics:
	metrics_dict[bout_end] = {}
	metrics_dict[bout_end][folder_name] = '3.4. pooled fish end bouts'








#* Colors for data in the plots.
# opacity: Final = 0.6

#TODO add more stuff to this dict.
condition_dict = {control : {color : [30,  136, 229], name : 'Control', id_in_path : 'control'},
# blue
# [55,  126, 184, opacity]
# line_color = 'rgb(30,136,229)'
delayNoOpt : {color : [152, 78,  163], name : 'Delay CC w/o Opt', id_in_path : 'delaynoopt', us_latency : 3},
# purple
delay : {color : [222, 222, 0], name : 'Delay CC', id_in_path : 'delay_', us_latency : 3},
# yellow
trace : {color :  [166, 86,  40], name : 'Trace CC 2500 ms', id_in_path : 'trace', us_latency : 6.5},
# brown


# trace1500 : {color : [255, 127, 0], name : 'Trace CC 1500 ms', id_in_path : 'trace1500conditioning_paired', us_latency : 5.5},
# # brown [166, 86,  40, opacity]
# trace2500 : {color : [166, 86,  40], name : 'Trace CC 2500 ms', id_in_path : 'trace2500conditioning_paired', us_latency : 6.5}
# brown
# 216, 27,  96 magenta
# line_color = 'rgb(255,193,7)'
# purple [152, 78,  163, opacity]
}

for c in condition_dict.keys():
	condition_dict[c][color][:3] = [i / 255 for i in condition_dict[c][color][:3]]


def get_conditions():
	# conditions = 
	return [condition_dict[k][id_in_path] for k in [*condition_dict.keys()]]

def get_exp_types():
	return [*condition_dict.keys()]








# colors = {
#     # 'yellow': [222, 222, 0],     #dede00
#     # 'orange': [255, 127, 0],    #ff7f00
#     'brown':  [166, 86,  40],   #a65628
#     'purple': [152, 78,  163],  #984ea3
#     'pink':   [247, 129, 191],  #f781bf

# 	# 'green':  [77,  175, 74],   #4daf4a

#     # 'blue':   [55,  126, 184],  #377eb8
#     # 'red':    [228, 26,  28],   #e41a1c
#     'gray':   [153, 153, 153],  #999999
# }  
# c_str = {k:f'rgba({v[0]},{v[1]},{v[2]},{opacity})' for (k, v) in colors.items()}

# condition_dict = {control : {'color' : c_str['blue'], 'name' : 'Control'},
# # line_color = 'rgb(30,136,229)'
# delayCC : {'color' : c_str['green'], 'name' : 'Delay CC'},
# # line_color = 'rgb(216,27,96)'
# trace500CC : {'color' : c_str['yellow'], 'name' : 'Trace CC 500 ms'},
# trace1500CC : {'color' : c_str['orange'], 'name' : 'Trace CC 1500 ms'},
# trace2500CC : {'color' : c_str['red'], 'name' : 'Trace CC 2500 ms'}
# # line_color = 'rgb(255,193,7)'
# }



plt.style.use('classic')

plt.rcParams['figure.constrained_layout.use'] = True

#TODO Define the consistent font at the top of the script
SMALL_SIZE = 20
MEDIUM_SIZE = 24
BIGGER_SIZE = 30

plt.rc('font', size=MEDIUM_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=MEDIUM_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=MEDIUM_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
plt.rcParams['figure.constrained_layout.use'] = True
plt.rcParams.update({'mathtext.default': 'regular' })
plt.rcParams['svg.fonttype'] = 'none'


# x_lim_time = (-15,15)
# x_ticks_time = (np.arange(-15,16,5))