# %%
import numpy as np
import pandas as pd
from pathlib import Path
import pandas as pd
import matplotlib.pyplot as plt

pd.set_option('mode.chained_assignment', None)

path_home = Path(r'C:\Users\joaqc\Desktop')

time = 'Time'
stim_dur = 'Stimulus duration'
isi = 'ISI'
cs_us = 'CS/US'

beg = 'Beg'
end = 'End'

cs = 'Cycle'
us = 'Reinforcer'


protocol_names = ['Test', 'Control-1', 'Control-2']


all_data_paths = [*Path(path_home).glob('*stim control.txt')]

protocol_types = [path.stem.split(' ')[0]   for path in all_data_paths]

test = [pd.read_csv(str(path), sep='\t', index_col='Type') for path in all_data_paths if 'test' in path.stem][0]
control_1 = [pd.read_csv(str(path), sep='\t', index_col='Type') for path in all_data_paths if 'control-1' in path.stem][0]
control_2 = [pd.read_csv(str(path), sep='\t', index_col='Type') for path in all_data_paths if 'control-2' in path.stem][0]

for d_i, d in enumerate([test, control_1, control_2]):
	# break
	# d.loc[:,beg]
	d.sort_values(by=beg, inplace=True)

	d -= d.iloc[0,0]

	d[stim_dur] = d.loc[:,end] - d.loc[:,beg]

	d[isi] = d.loc[:,beg].diff().to_list()[1:] + [np.nan]
	# d[isi] = d[isi].astype('int')

	print(protocol_names[d_i])

	print('Min ISI:  ', d[isi].min())

	for stim in [cs, us]:
		print('Min and Max duration of ' + stim + ':  ' + str(d.loc[stim, stim_dur].min()) + '  ' + str(d.loc[stim, stim_dur].max()))
	print('\n')

print('test_cs vs control_1_cs')
print(test.loc[cs, beg].to_numpy() - control_1.loc[cs, beg].to_numpy(), '\n')

print('test_us vs control_2_us')
print(test.loc[us, beg].to_numpy() - control_2.loc[us, beg].to_numpy(), '\n')

print('control_2_us vs control_1_cs (conditioning block)')
print(control_2.loc[us, beg].iloc[3:-1].to_numpy() - control_1.loc[cs, beg].iloc[3+3:-3].to_numpy(), '\n')

print('control_2_cs vs control_1_us (conditioning block)')
print(control_2.loc[cs, beg].iloc[3+3:-3].to_numpy() - control_1.loc[us, beg].iloc[3:-1].to_numpy(), '\n')


print('test_us vs control_1_us')
print(test.loc[us, beg].to_numpy() - control_1.loc[us, beg].to_numpy(), '\n')

print('test_cs vs control_2_cs')
print(test.loc[cs, beg].to_numpy() - control_2.loc[cs, beg].to_numpy(), '\n')