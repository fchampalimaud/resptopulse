import numpy as np
import matplotlib.pyplot as plt
import gc
import time
import pandas as pd
import pickle as pkl
from pandas.api.types import CategoricalDtype



from my_general_variables import *
from my_experiment_specific_variables import *

plt.style.use('classic')

#TODO Define the consistent font at the top of the script
SMALL_SIZE = 14
MEDIUM_SIZE = 18
BIGGER_SIZE = 26

plt.rc('font', size=MEDIUM_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=MEDIUM_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=MEDIUM_SIZE)    # legend fontsize
plt.rc('figure', titlesize=MEDIUM_SIZE)  # fontsize of the figure title


def findStim(data):

	cs_beg_array = data.loc[data[cs_beg] != 0, time_trial_csus].to_numpy()
	cs_end_array = data.loc[data[cs_end] != 0, time_trial_csus].to_numpy()

	us_beg_array = data.loc[data[us_beg] != 0, time_trial_csus].to_numpy()
	us_end_array = data.loc[data[us_end] != 0, time_trial_csus].to_numpy()

	#! NEEDS TO BE FIXED BY USING AND FURTHER CORRECTING WHAT IS IN THESE COMMENTS.
	# if len(cs_beg_array) != 0 and cs_end_array[0] < cs_beg_array[0]:
	# 	cs_beg_array = np.append(data.loc[data.index[0], time_trial_csus].to_numpy(), cs_beg_array)
	# if len(cs_end_array) != 0 and cs_end_array[-1] < cs_beg_array[-1]:
	# 	cs_end_array = np.append(data.loc[data.index[-1], time_trial_csus].to_numpy(), cs_end_array)

	# if len(us_beg_array) != 0 and us_end_array[0] < us_beg_array[0]:
	# 	us_beg_array = np.append(data.loc[data.index[0], time_trial_csus].to_numpy(), us_beg_array)
	# if len(us_end_array) != 0 and us_end_array[-1] < us_beg_array[-1]:
	# 	us_end_array = np.append(data.loc[data.index[-1], time_trial_csus].to_numpy(), us_end_array)

	if len(cs_end_array) < len(cs_beg_array):
		cs_end_array = np.append(cs_end_array, data.loc[:, time_trial_csus].iloc[-1])

	if len(us_end_array) < len(us_beg_array):
		us_end_array = np.append(us_end_array, data.loc[:, time_trial_csus].iloc[-1])

	return cs_beg_array, cs_end_array, us_beg_array, us_end_array

# def stimuliInSession(session, s_i, session_all_stim):

# 	cs_beg_array = session.loc[session[cs_beg] != 0, time_trial_csus].to_numpy()
# 	cs_end_array = session.loc[session[cs_end] != 0, time_trial_csus].to_numpy()

# 	us_beg_array = session.loc[session[us_beg] != 0, time_trial_csus].to_numpy()
# 	us_end_array = session.loc[session[us_end] != 0, time_trial_csus].to_numpy()

# 	if len(cs_end_array) < len(cs_beg_array):
# 		cs_end_array = np.append(cs_end_array, session.loc[:, time_trial_csus].iloc[-1])

# 	if len(us_end_array) < len(us_beg_array):
# 		us_end_array = np.append(us_end_array, session.loc[:, time_trial_csus].iloc[-1])

# 	session_all_stim[0][s_i-1] = cs_beg_array
# 	session_all_stim[1][s_i-1] = cs_end_array
# 	session_all_stim[2][s_i-1] = us_beg_array
# 	session_all_stim[3][s_i-1] = us_end_array




#* Save figure.
def saveFig_1(fig, fig_name, figure_size, frmt='svg', dpi=20):

	t = time.strftime("%Y-%m-%d %H.%M.%S", time.gmtime())

	fig_name_ = str(fig_name)
	# + '_' 
	# + str(t)

	#* Once I need to save as svg, rerun this script.
	fig.set_size_inches(figure_size)
	fig.savefig(fig_name_ + '.' + frmt, format=frmt, facecolor='white', dpi=dpi)
	
	#* Save fig also in a pickle file
	# pkl.dump(fig,  open(fig_name_ + '.' + 'pickle',  'wb')  )

		# figx = pkl.load(  open('FigureObject'+'.pickle',  'rb')  )
		# data = figx.axes[0].lines[0].get_data()
		# plt.close()




def pooled_session_stats(data_csus, trials_in_s):


	# session is a slice of the original data_csus, with each trial independent.
	if type(trials_in_s) is list:
		session = data_csus[data_csus[number_trial_csus].isin(trials_in_s)]
	else:
		session = data_csus[data_csus[number_trial_csus] == trials_in_s]

	# session = session.groupby(by=time_trial_csus, as_index=True, dropna=False)[[activity, bout, bout_beg, bout_end]].agg({activity: ['count', 'mean', 'sem'], bout: 'mean', bout_beg: 'mean', bout_end: 'mean'})
#TODO adapt for when metrics have less params.
	session = session.groupby(by=time_trial_csus, as_index=True, dropna=False)[metrics].mean()
#TODO may need to use this to check if it has all the trials
	# agg({bout: ['count', 'sum'], bout_beg: 'sum', bout_end: 'sum'})


	return session

# def set_dtypes_and_sort_index(data):

	
	# data.sort_index(inplace=True)
	
	# # Set the columns' dtypes.
	# data[time_trial_csus] = data[time_trial_csus].astype('int32')
	# data[cols_stim[:4]] = data[cols_stim[:4]].astype('category')
	# data[number_trial_csus] = data[number_trial_csus].astype('category')
	# data[activity] = data[activity].astype(pd.SparseDtype('float32', 0))
	# data.loc[:, cols_bout] = 	data[cols_bout].astype(pd.SparseDtype('bool'))

	# # return data



def pooled_data_stats(data_csus, csus, pool_type):
# , path_part_to_save):

	# index = data_csus.index[0]

	sessions_csus = sessions_dict[pool_type][csus][trials_sessions]

	condition_sessions = [0] * len(sessions_csus)

	for s_i, trials_in_s in enumerate(sessions_csus):

		session = pooled_session_stats(data_csus, trials_in_s)

		# Identify the session number.
		session[number_trial_csus] = str(s_i+1)


		# # Confirm the number of elements in each timepoint.
		# if not session[session[(activity, count)] != len(trials_in_s)].empty:
		# 	print('Missing timepoints in some of the trials!')

		# if len(session.iloc[:,0].unique()) != 1:
		# 	print('Missing timepoints in some of the trials! ({}, trials in this session: {})'.format(data_csus.index[0], trials_in_s))
		# 	plt.plot(session[count])
		# 	plt.show()

		# all_sessions_pooled = pd.concat([all_sessions_pooled, session])
		condition_sessions[s_i] = session.copy()

	del session

	condition_sessions = pd.concat(condition_sessions)


	# Used the 'Trial number' column to identify the session number. So, need to rename it.
	condition_sessions.rename(columns={number_trial_csus: number_session}, inplace=True)


	condition_sessions.reset_index(time_trial_csus, drop=False, inplace=True)


	# Remove multi-index set on the column axis.
	# condition_sessions.columns = [time_trial_csus, count] + [' '.join(x) for x in condition_sessions.iloc[:, 2:-1].columns.ravel()] + [number_session]


	# # Reorder columns.
	# condition_sessions = condition_sessions[[time_trial_csus, number_session, count] + condition_sessions.iloc[:, 2:-1].columns.to_list()]


	# # Set the columns' dtypes.
	# condition_sessions = condition_sessions.astype({time_trial_csus : 'int', number_session : 'category', count : 'category', activity_mean : 'float32', activity_sem : 'float32', bout_mean : 'float32', bout_beg_mean : 'float32', bout_end_mean : 'float32'})


	# # plt.plot(condition_sessions[count])
	# # plt.show()


	# # Set the index.
	# condition_sessions[['Strain', 'Age (dpf)', 'Exp.', 'ProtocolRig', 'Day', 'Fish no.']] = index
	
	# condition_sessions[['Strain', 'Age (dpf)', 'Exp.', 'ProtocolRig', 'Day', 'Fish no.']] = condition_sessions[['Strain', 'Age (dpf)', 'Exp.', 'ProtocolRig', 'Day', 'Fish no.']].astype('category')
	
	# condition_sessions.set_index(keys=['Strain', 'Age (dpf)', 'Exp.', 'ProtocolRig', 'Day', 'Fish no.'],inplace=True)


	# # Save data in a pkl file.
	# condition_sessions.to_pickle(path_part_to_save + '_' + csus + '_' + pool_type + '_stats.pkl')

	return condition_sessions


def bin_or_filter_data(data, stat, Bin_data, time_bins):


	#* Bin data.
	if Bin_data:

#TODO this part should call another function called binData()
		bins = pd.cut(data.loc[:, time_trial_csus], time_bins, include_lowest=True)

		data = data.groupby(by=[number_session, bins], as_index=True, dropna=False)[metrics].agg(stat)

		data = data.reset_index().pivot(index=time_trial_csus, columns=number_session).reset_index(time_trial_csus)

		# data = data.reindex(np.array(time_bins[:-1]) + np.array(bin_interval/2))

		data[time_trial_csus] = np.array(time_bins[:-1]) + np.array(bin_interval/2)

		data.set_index(time_trial_csus, inplace=True)


	else:
		#* Filter data with rolling mean.
		data = data.groupby(by=[number_session, time_trial_csus], as_index=True, dropna=False)[metrics].agg(stat)

		data = data.reset_index().pivot(index=time_trial_csus, columns=number_session)

		data = data.rolling(window=filtering_window, center=True).mean().dropna()

	return data





# Create the figure.
def createFig_1(csus, pool_type, y_label, y_lim, x_lim_time=(-15,15), x_ticks_time=(np.arange(-15,16,5)), x_label='t (s)'):

	x_label = '\n' + x_label
	y_label += '\n'

	names_sessions_csus = sessions_dict[pool_type][csus][names_sessions]
	sessions_csus = sessions_dict[pool_type][csus][trials_sessions]
	number_c_or_r = sessions_dict[pool_type][number_cols_or_rows]
	horizontal = sessions_dict[pool_type][horizontal_fig]
	figure_size = sessions_dict[pool_type][fig_size]

	if horizontal:
		n_cols = number_c_or_r
		n_rows = 1
	else:
		n_cols = 1
		n_rows = number_c_or_r

	fig, axs = plt.subplots(n_rows, n_cols, sharex=False, sharey=False, facecolor='white', constrained_layout=True, figsize=figure_size, clip_on=False)

	for s_i in range(len(sessions_csus)):

		
		axs[s_i].set_title(names_sessions_csus[s_i], loc='left')
		axs[s_i].spines[:].set_visible(False)

		if horizontal:
			#  or number_c_or_r >= 9:
			axs[s_i].set_xlabel(x_label)
			if s_i > 0:
				axs[s_i].set_yticks([])
		else:
			axs[s_i].set_ylabel(y_label)

		axs[s_i].set(xlim=x_lim_time, xticks=x_ticks_time, ylim=y_lim)
		# , xlabel=x_label, ylabel=y_label)

		axs[s_i].axhline(0, color='k', alpha=0.8, linewidth=1)

		axs[s_i].tick_params(axis='both', which='both', bottom=True, top=False, right=False)


		#* Highlight in the subplots common stimuli to all conditions.
		if csus == cs:
			axs[s_i].axvspan(0, cs_duration, ymin=0, ymax=1, color='green', alpha=0.8, linewidth=2, linestyle='--', fill=False, clip_on=False)
		
			# if s_i not in [1, len(sessions_csus)] and control != exp_type:
			# 	axs[s_i].axvspan(us_latency, us_latency + us_duration, ymin=0, ymax=1, color='red', alpha=0.2, linewidth=0, linestyle='--', fill=True)

		else:
			axs[s_i].axvspan(0, us_duration, ymin=0, ymax=1, color='red', alpha=0.8, linewidth=0, fill=True)


	if horizontal:
		# axs[s_i].legend(bbox_to_anchor=(1.05, 1), loc='upper right', borderaxespad=0, frameon=False)
		axs[s_i].legend(bbox_to_anchor=(1, 1.05), loc='best', borderaxespad=0, frameon=False)
		# axs[0].tick_params('y',reset=True)
		axs[0].set_ylabel(y_label)
		# axs[0].set_yticks()

		if csus == us:
			for i in range(1, number_c_or_r-s_i):
				axs[-i].spines[:].set_visible(False)
				axs[-i].set_xticks([])
				axs[-i].set_yticks([])

			# for s_i in range(len(sessions_csus)):
			# 	axs[s_i].tick_params('x',reset=True)

	else:
		# if csus==us:
		axs[0].legend(bbox_to_anchor=(1, 1.05), loc='best', borderaxespad=0, frameon=False)
		axs[s_i].tick_params('x',reset=True)
		axs[s_i].set_xlabel(x_label)


	fig.suptitle(pool_type + '\n', x=0, horizontalalignment='left')

	return fig, axs


def highlightSpecStim(axs, csus, exp_type, color, sessions_csus):
				
	for s_i in range(len(sessions_csus)):

		if exp_type not in control:

			us_latency_ = condition_dict[exp_type][us_latency]
			
			if csus == cs:
				if s_i not in [0, len(sessions_csus)-1]:

					# axs[s_i].axvspan(us_latency_, us_latency_ + us_duration, ymin=0, ymax=1, color=color, alpha=0.2, linewidth=1, fill=False)
					axs[s_i].axvspan(us_latency_, us_latency_ + us_duration, ymin=0, ymax=1, color=color, alpha=0.8, linewidth=0, linestyle='--', fill=True)

			else:
				# if s_i in range(2, len(sessions_csus)):
				if s_i <= len(sessions_csus):

					axs[s_i].axvspan(-us_latency_, -us_latency_ + cs_duration, ymin=0, ymax=1, color=color, alpha=0.8, linewidth=2, linestyle='--', fill=False)
					# axs[s_i].axvspan(-us_latency_, -us_latency_ + cs_duration, ymin=0, ymax=1, color=color, alpha=0.2, linewidth=0, fill=True)





def defineMetricsDict(Subt_by_control, Null_baseline):

	if Subt_by_control or Null_baseline:

		if activity in metrics:
			metrics_dict[activity][y_label] = 'Relative average tail movement vigour (deg/ms)'
			metrics_dict[activity][y_lim] = (-5,20)
			metrics_dict[activity][y_ticks] = (np.arange(0,21,20))
		
		if bout in metrics:
			metrics_dict[bout][y_label] = '$\Delta{P}_{tail\ movement}$'
			metrics_dict[bout][y_lim] = (-0.25,0.85)
			metrics_dict[bout][y_ticks] =  []

		if bout_beg in metrics:
			metrics_dict[bout_beg][y_label] = '$\Delta{P}_{start\ tail\ movement}$'
			metrics_dict[bout_beg][y_lim] = (-0.0003,0.0015)
			metrics_dict[bout_beg][y_ticks] = []

		if bout_end in metrics:
			metrics_dict[bout_end][y_label] = '$\Delta{P}_{end\ tail\ movement}$'
			metrics_dict[bout_end][y_lim] = (-0.0003,0.0015)
			metrics_dict[bout_end][y_ticks] = []

	else:

		if activity in metrics:
			metrics_dict[activity][y_label] = 'Tail movement vigour (deg/ms)'
			metrics_dict[activity][y_lim] = (-1,25)
			metrics_dict[activity][y_ticks] = (np.arange(0,21,20))

		if bout in metrics:
			metrics_dict[bout][y_label] = '$P_{tail\ movement}$'
			metrics_dict[bout][y_lim] = (0,0.95)
			metrics_dict[bout][y_ticks] =  []

		if bout_beg in metrics:
			metrics_dict[bout_beg][y_label] = '$P_{start\ tail\ movement}$'
			metrics_dict[bout_beg][y_lim] = (-0.0003,0.002)
			metrics_dict[bout_beg][y_ticks] = []

		if bout_end in metrics:
			metrics_dict[bout_end][y_label] = '$P_{end\ tail\ movement}$'
			metrics_dict[bout_end][y_lim] = (-0.0003,0.002)
			metrics_dict[bout_end][y_ticks] = []

	# return metrics_dict




def prepare_data(data, csus, pool_type):


	#* Remove columns not used.
	data = data[list(data.columns[[0,5]]) + metrics]


	#* Select data_csus inside data_csus.
	# index_unique = data_csus.index.unique()
	# index_unique = index_unique[:3]
	# data_csus = data_csus.loc[index_unique, :]


	#* Crop data_csus.
	# data_csus = data_csus[(data_csus[time_trial_csus] >= -t_crop_data_bef_s * expected_framerate) & (data_csus[time_trial_csus] <= t_crop_data_aft_s * expected_framerate)]


	#* Save the references of all fish.
	# with open(path_pooled_data / 'fish_id.txt', 'a') as file:
	# 	file.write(pool_type + '\n')
	# 	file.writelines([str(fish) + '\n' for fish in data_csus.index.unique()])
	# 	file.write('\n\n')
	# Path.write_text(data_csus, newline='\n')


#TODO remove this once 'prepocessing' is repeated.
	# data[number_trial_csus] = data[number_trial_csus].astype(CategoricalDtype(ordered=True))

	#* Identify trials with the number of the session they belong to.
	if pool_type != sessions_1t:
		
		sessions_csus = sessions_dict[pool_type][csus][trials_sessions]

		data[number_session] = 0

		for s_i, trials_in_s in enumerate(sessions_csus):

			# if type(trials_in_s) is list:
			data.loc[data[number_trial_csus].isin([str(t) for t in trials_in_s]), number_session] = sessions_dict[pool_type][csus][names_sessions][s_i]
			# s_i + 1

			# In case of single trials and sessions_csus entries being scalars and not lists with a single entry.
			# else:
			# 	data.loc[data[number_trial_csus] == str(trials_in_s), number_session] = s_i + 1

		data[number_session] = data[number_session].astype(CategoricalDtype(categories=sessions_dict[pool_type][csus][names_sessions], ordered=True))
		
		#TODO I think I can get rid of this part once the prepocessing is repeated.
		data.loc[:, number_trial_csus] = data.loc[:, number_trial_csus].astype('int')
		data.loc[:, number_trial_csus] = data.loc[:, number_trial_csus].astype(CategoricalDtype(categories=np.sort(data[number_trial_csus].unique()), ordered=True))

	else:
		data[number_session] = data[number_trial_csus]

	#! Important to remove trials that are to be discarded because they are not included in any trials_in_s. (for experiments after May 2022)
	data.dropna(inplace=True)


	#* Sort index to increase performance when indexing in subsequent operations.
	data.sort_index(inplace=True)


	#* Extremely important to change to dense data to speed up several times the groupby operation!
	data[metrics] = data[metrics].sparse.to_dense().astype('float')


	#* Convert time from frames to s.
	data[time_trial_csus] = data[time_trial_csus] / expected_framerate # s


	return data




def difference_at_onset(data, cr_window):
	# , exp_type=None

# x_lim_time = (-15,15)
# x_ticks_time = (np.arange(-15,16,5))
	if number_session in data.columns and number_trial_csus in data.columns:
		data_ = data.set_index([number_trial_csus, number_session], append=True).sort_index().copy()
		
		trials_bef_onset = data_.loc[(data_[time_trial_csus] >= -baseline_window) & (data_[time_trial_csus] <= 0), :].groupby(level=data_.index.names, observed=True)[metrics].agg('mean')

	else:
		if number_session in data.columns:
			data_ = data.set_index(number_session, append=False).sort_index().copy()
		else:
		# elif number_trial_csus in data.columns:
			data_ = data.set_index(number_trial_csus, append=False).sort_index().copy()

		trials_bef_onset = data_.loc[(data_[time_trial_csus] >= -baseline_window) & (data_[time_trial_csus] <= 0), :].groupby(level=data_.index.names, observed=True)[metrics].agg('mean')


	trials_bef_onset.columns = pd.MultiIndex.from_arrays([metrics, ['mean '+str(baseline_window)+' s before' for _ in metrics]])


	trials_aft_onset = data_.loc[(data_[time_trial_csus] > 0) & (data_[time_trial_csus] <= cr_window), :].groupby(level=data_.index.names, observed=True)[metrics].agg('mean')

	trials_aft_onset.columns = pd.MultiIndex.from_arrays([metrics, ['mean '+str(cr_window)+' s after'  for _ in metrics]])
	# trials_aft_onset.rename(columns=lambda x: x +' mean '+str(cr_window)+' s after')

	# trials_aft_onset.sub(trials_bef_onset, axis=1, level=1)

	# data_difference_at_onset = trials_aft_onset - trials_bef_onset
	# .iloc[:,-len(metrics)]

	# data_difference_at_onset.rename(columns=lambda x: x +' difference')

	data_difference_at_onset = pd.concat([trials_bef_onset, trials_aft_onset], axis=1)

	metrics_len = len(metrics)

	data_ = pd.DataFrame(data_difference_at_onset.iloc[:,-metrics_len:].to_numpy() - data_difference_at_onset.iloc[:,-metrics_len*2:-metrics_len].to_numpy())

	# pd.DataFrame(data_difference_at_onset.iloc[:,-metrics_len:].to_numpy() - data_difference_at_onset.iloc[:,-metrics_len*2:-metrics_len].to_numpy())

	data_.columns = pd.MultiIndex.from_arrays([metrics, ['difference'  for _ in metrics]])

	data_.set_index(data_difference_at_onset.index, inplace=True)

	data_difference_at_onset = pd.concat([data_difference_at_onset, data_], axis=1)

	# data_difference_at_onset.columns = pd.MultiIndex.from_arrays([metrics*3, [m+' mean '+str(baseline_window)+' s before' for m in metrics] + [m+' mean '+str(cr_window)+' s after'  for m in metrics] + [m+' difference' for m in metrics]])

	# data_difference_at_onset

	if number_session in data_difference_at_onset.index.names and number_trial_csus in data_difference_at_onset.index.names:
		data_difference_at_onset = data_difference_at_onset.reset_index(['Exp.', number_trial_csus, number_session])
	
	else:
		if number_trial_csus in data_difference_at_onset.index.names:
			data_difference_at_onset = data_difference_at_onset.reset_index(number_trial_csus)
		else:
			data_difference_at_onset = data_difference_at_onset.reset_index(number_session)

	return data_difference_at_onset.sort_index()







def arrangeFig_2(fig, axs, csus, metric, pool_type, cr_window, y_lim_3):

	if pool_type == sessions_1t:
		x_label = '\n' + 'Trial number'
	else:
		x_label = None
		# sessions_dict[pool_type][csus][names_sessions]

	# x_ticks = np.arange(1, len(x_label)+1)
	x_lim = (-0.5, len(sessions_dict[pool_type][csus][names_sessions]))
	
	axs[0].get_shared_y_axes().join(axs[0], axs[1])
	
# #TODO
# axs[0].set_ylabel(y_label)
# axs[1].set_ylabel(y_label)
# axs[2].set_ylabel(y_label)

	for s_i in range(3):
		
		# axs[s_i].spines[:].set_visible(False)
		axs[s_i].spines['right'].set_visible(False)
		axs[s_i].spines['top'].set_visible(False)
		axs[s_i].locator_params(axis='y', tight=False, nbins=4)
		axs[s_i].tick_params(axis='both', which='both', bottom=True, top=False, right=False)

		if s_i<2:
			axs[s_i].axes.get_xaxis().set_visible(False)

			axs[s_i].set_ylim((0, None))

			axs[s_i].axes.get_xaxis().set_visible(False)
		
		else:
			if pool_type == sessions_1t:
				axs[s_i].set_xlabel(x_label)

		axs[s_i].set_xlim(x_lim)
		# axs[s_i].set_xticks(x_ticks)
		
		axs[s_i].axhline(0, color='k', alpha=0.8, linewidth=1)
	
	axs[2].set_xlabel(None)

	axs[2].set_ylim(y_lim_3)

	axs[0].legend(bbox_to_anchor=(1, 1.05), loc='best', borderaxespad=0, frameon=True)
	axs[1].legend().set_visible(False)
	axs[2].legend().set_visible(False)
	
	if csus == cs:
		axs[0].set_ylabel('$P^{before\ stim.\ onset}_{tail\ movement}$')
		axs[1].set_ylabel('$P^{after\ stim.\ onset}_{tail\ movement}$')
		axs[2].set_ylabel('$\Delta{P}^{stim.\ onset}_{tail\ movement}$')

# #TODO
# 	if csus == us:
# 		axs[0].set_ylabel('$P^{baseline}_{tail\ movement}$')
# 		axs[1].set_ylabel('$P^{US}_{tail\ movement}$')
# 		axs[2].set_ylabel('$\Delta{P}^{US\ onset}_{tail\ movement}$')

	# axs[0].locator_params(axis='y', tight=False, nbins=4)
	# axs[1].locator_params(axis='y', tight=False, nbins=4)
	# axs[2].locator_params(axis='y', tight=False, nbins=4)

	for tick in axs[2].get_xticklabels():
		tick.set_rotation(45)

	axs[0].set_title('Movement before {} onset (−{}-0 s)'. format(csus, baseline_window), loc='left')
	axs[1].set_title('Movement after {} onset (0-{} s)'. format(csus, cr_window), loc='left')
	axs[2].set_title('Change at onset (0 s)', loc='left')

	fig.suptitle(metrics_dict[metric][y_label] + '\n' + pool_type + '\n', x=0, horizontalalignment='left')

	fig.set_size_inches((15,20))

	return fig, axs





def saveFig_2(fig, fig_name, type_plot, exp_type, baseline_window, cr_window, comment, figure_size, frmt='svg', dpi=40):

	t = time.strftime("%Y-%m-%d %H.%M.%S", time.gmtime())

	fig_name_ = str(fig_name) + 'control-{}_{}_{}-s baseline_{}-s after onset_{}_{}'.format(exp_type, type_plot, baseline_window, cr_window, comment, t)

	#* Once I need to save as svg, rerun this script.
	fig.set_size_inches(figure_size)
	fig.savefig(fig_name_ + '.' + frmt, format=frmt, facecolor='white', dpi=dpi)
	
	#* Save fig also in a pickle file
	pkl.dump(fig,  open(fig_name_ + '.' + 'pickle',  'wb')  )

		# figx = pkl.load(  open('FigureObject'+'.pickle',  'rb')  )
		# data = figx.axes[0].lines[0].get_data()
		# plt.close()




def up_or_down(x):
	if x<0:
		x='Decrease'
	elif x>0:
		x='Increase'
	else:
		x='No change'
		
	return x




















# # Create the figure.
# def createFig(csus, pool_type, y_label, y_lim, x_lim_time=(-15,15), x_ticks_time=(np.arange(-15,16,5)), x_label='t (s)'):

# 	x_label = '\n' + x_label
# 	y_label += '\n'

# 	names_sessions_csus = sessions_dict[pool_type][csus][names_sessions]
# 	sessions_csus = sessions_dict[pool_type][csus][trials_sessions]
# 	number_c_or_r = sessions_dict[pool_type][number_cols_or_rows]
# 	horizontal = sessions_dict[pool_type][horizontal_fig]
# 	figure_size = sessions_dict[pool_type][fig_size]

# 	if horizontal:
# 		n_cols = number_c_or_r
# 		n_rows = 1
# 	else:
# 		n_cols = 1
# 		n_rows = number_c_or_r

# 	fig, axs = plt.subplots(n_rows, n_cols, sharex=False, sharey=False, facecolor='white', constrained_layout=True, figsize=figure_size, clip_on=False)

# 	for s_i in range(len(sessions_csus)):

		
# 		axs[s_i].set_title(names_sessions_csus[s_i], loc='left')
# 		axs[s_i].spines[:].set_visible(False)

# 		if horizontal:
# 			#  or number_c_or_r >= 9:
# 			axs[s_i].set_xlabel(x_label)
# 			if s_i > 0:
# 				axs[s_i].set_yticks([])
# 		else:
# 			axs[s_i].set_ylabel(y_label)

# 		axs[s_i].set(xlim=x_lim_time, xticks=x_ticks_time, ylim=y_lim)
# 		# , xlabel=x_label, ylabel=y_label)

# 		axs[s_i].axhline(0, color='k', alpha=0.8, linewidth=1)



# 		#* Highlight in the subplots common stimuli to all conditions.
# 		if csus == cs:
# 			axs[s_i].axvspan(0, cs_duration, ymin=0, ymax=1, color='green', alpha=0.8, linewidth=2, linestyle='--', fill=False, clip_on=False)
		
# 			# if s_i not in [1, len(sessions_csus)] and control != exp_type:
# 			# 	axs[s_i].axvspan(us_latency, us_latency + us_duration, ymin=0, ymax=1, color='red', alpha=0.2, linewidth=0, linestyle='--', fill=True)

# 		else:
# 			axs[s_i].axvspan(0, us_duration, ymin=0, ymax=1, color='red', alpha=0.8, linewidth=0, fill=True)


# 	if horizontal:
# 		# axs[s_i].legend(bbox_to_anchor=(1.05, 1), loc='upper right', borderaxespad=0, frameon=False)
# 		# axs[s_i].legend(bbox_to_anchor=(1, 1.05), loc='best', borderaxespad=0, frameon=True)
# 		# axs[0].tick_params('y',reset=True)
# 		axs[0].set_ylabel(y_label)
# 		# axs[0].set_yticks()

# 		if csus == us:
# 			for i in range(1, number_c_or_r-s_i):
# 				axs[-i].spines[:].set_visible(False)
# 				axs[-i].set_xticks([])
# 				axs[-i].set_yticks([])

# 			# for s_i in range(len(sessions_csus)):
# 			# 	axs[s_i].tick_params('x',reset=True)

# 	else:
# 		# if csus==us:
# 		# axs[0].legend(bbox_to_anchor=(1, 1.05), loc='best', borderaxespad=0, frameon=True)
# 		axs[s_i].tick_params('x',reset=True)
# 		axs[s_i].set_xlabel(x_label)



# 	fig.suptitle(pool_type + '\n', x=0, horizontalalignment='left')

# 	return fig, axs



# def highlightSpecStim(axs, csus, exp_type, color, sessions_csus):
				
# 	for s_i in range(len(sessions_csus)):

# 		if exp_type != control:

# 			us_latency_ = condition_dict[exp_type][us_latency]
			
# 			if csus == cs:
# 				if s_i not in [0, len(sessions_csus)-1]:

# 					# axs[s_i].axvspan(us_latency_, us_latency_ + us_duration, ymin=0, ymax=1, color=color, alpha=0.2, linewidth=1, fill=False)
# 					axs[s_i].axvspan(us_latency_, us_latency_ + us_duration, ymin=0, ymax=1, color=color, alpha=0.8, linewidth=0, linestyle='--', fill=True)

# 			else:
# 				# if s_i in range(2, len(sessions_csus)):
# 				if s_i <= len(sessions_csus):

# 					axs[s_i].axvspan(-us_latency_, -us_latency_ + cs_duration, ymin=0, ymax=1, color=color, alpha=0.8, linewidth=2, linestyle='--', fill=False)
# 					# axs[s_i].axvspan(-us_latency_, -us_latency_ + cs_duration, ymin=0, ymax=1, color=color, alpha=0.2, linewidth=0, fill=True)





# def createFig_1(csus, pool_type, y_label, y_lim, x_lim_time=(-15,15), x_ticks_time=(np.arange(-15,16,5)), x_label='t (s)'):




# 	x_label = '\n' + x_label
# 	y_label += '\n'

# 	names_sessions_csus = sessions_dict[pool_type][csus][names_sessions]
# 	sessions_csus = sessions_dict[pool_type][csus][trials_sessions]

# 	number_c_or_r = sessions_dict[pool_type][number_cols_or_rows]
	
# 	# horizontal = sessions_dict[pool_type][horizontal_fig]
# 	# figure_size = sessions_dict[pool_type][fig_size]


# 	fig, axs = plt.subplots(number_c_or_r, 1, sharex=False, sharey=False, facecolor='white', constrained_layout=True, figsize=figure_size, clip_on=False)

# 	for s_i in range(len(sessions_csus)):

		
# 		axs[s_i].set_title(names_sessions_csus[s_i], loc='left')
# 		axs[s_i].spines[:].set_visible(False)

# 		if horizontal:
# 			#  or number_c_or_r >= 9:
# 			axs[s_i].set_xlabel(x_label)
# 			if s_i > 0:
# 				axs[s_i].set_yticks([])
# 		else:
# 			axs[s_i].set_ylabel(y_label)

# 		axs[s_i].set(xlim=x_lim_time, xticks=x_ticks_time, ylim=y_lim)
# 		# , xlabel=x_label, ylabel=y_label)

# 		axs[s_i].axhline(0, color='k', alpha=0.8, linewidth=1)



# 		#* Highlight in the subplots common stimuli to all conditions.
# 		if csus == cs:
# 			axs[s_i].axvspan(0, cs_duration, ymin=0, ymax=1, color='green', alpha=0.8, linewidth=2, linestyle='--', fill=False, clip_on=False)
		
# 			# if s_i not in [1, len(sessions_csus)] and control != exp_type:
# 			# 	axs[s_i].axvspan(us_latency, us_latency + us_duration, ymin=0, ymax=1, color='red', alpha=0.2, linewidth=0, linestyle='--', fill=True)

# 		else:
# 			axs[s_i].axvspan(0, us_duration, ymin=0, ymax=1, color='red', alpha=0.8, linewidth=0, fill=True)


# 	if horizontal:
# 		# axs[s_i].legend(bbox_to_anchor=(1.05, 1), loc='upper right', borderaxespad=0, frameon=False)
# 		axs[s_i].legend(bbox_to_anchor=(1, 1.05), loc='best', borderaxespad=0, frameon=True)
# 		# axs[0].tick_params('y',reset=True)
# 		axs[0].set_ylabel(y_label)
# 		# axs[0].set_yticks()

# 		if csus == us:
# 			for i in range(1, number_c_or_r-s_i):
# 				axs[-i].spines[:].set_visible(False)
# 				axs[-i].set_xticks([])
# 				axs[-i].set_yticks([])

# 			# for s_i in range(len(sessions_csus)):
# 			# 	axs[s_i].tick_params('x',reset=True)

# 	else:
# 		# if csus==us:
# 		axs[0].legend(bbox_to_anchor=(1, 1.05), loc='best', borderaxespad=0, frameon=True)
# 		axs[s_i].tick_params('x',reset=True)
# 		axs[s_i].set_xlabel(x_label)



# 	fig.suptitle(pool_type + '\n', x=0, horizontalalignment='left')

# 	return fig, axs








# # TODO improve based on my_functions_five script




# def plotContinuousTracesTrialsSingleFish(data_csus, sessions_csus, number_rows, fish_id, fig_names):


# 	fig_blocksOfTrialsSingleFish_angle_name, fig_blocksOfTrialsSingleFish_activity_name, fig_blocksOfTrialsSingleFish_boutProbability_name, fig_blocksOfTrialsSingleFish_begBoutProbability_name, fig_blocksOfTrialsSingleFish_endBoutProbability_name = fig_names

# 	sessions_csus = [i[0] for i in sessions_csus]

# 	scaling_factor = len(sessions_csus)/90

# 	# Create the figures.
# 	fig_blocksOfTrialsSingleFish_angle, axs_fig_blocksOfTrialsSingleFish_angle = plt.subplots(number_rows, 1, sharex=True, sharey=True, facecolor='white', figsize=(50,200*scaling_factor), constrained_layout=True)
# 	# fig_blocksOfTrialsSingleFish_activity, axs_fig_blocksOfTrialsSingleFish_activity = plt.subplots(number_rows, 1, sharex=True, sharey=True, facecolor='white', figsize=(20,70*scaling_factor), constrained_layout=True)
# 	fig_blocksOfTrialsSingleFish_boutProbability, axs_fig_blocksOfTrialsSingleFish_boutProbability = plt.subplots(number_rows, 1, sharex=True, sharey=True, facecolor='white', figsize=(20,70*scaling_factor), constrained_layout=True)
# 	fig_blocksOfTrialsSingleFish_begBoutProbability, axs_fig_blocksOfTrialsSingleFish_begBoutProbability = plt.subplots(number_rows, 1, sharex=True, sharey=True, facecolor='white', figsize=(20,70*scaling_factor), constrained_layout=True)
# 	fig_blocksOfTrialsSingleFish_endBoutProbability, axs_fig_blocksOfTrialsSingleFish_endBoutProbability = plt.subplots(number_rows, 1, sharex=True, sharey=True, facecolor='white', figsize=(20,70*scaling_factor), constrained_layout=True)

# 	for s_i, trials_in_s in enumerate(sessions_csus, start=1):

# 		session = data_csus[data_csus[number_trial_csus] == str(trials_in_s)]

# 		# Add data to figures.
		
# 		axs_fig_blocksOfTrialsSingleFish_angle[s_i-1].plot(session[time_trial_csus], session[tail_angle], 'k', linewidth=1)

# 		# axs_fig_blocksOfTrialsSingleFish_activity[s_i-1].plot(session[time_trial_csus], session[activity], 'k', linewidth=1)
# 		axs_fig_blocksOfTrialsSingleFish_boutProbability[s_i-1].eventplot(session.loc[session[bout] != 0, time_trial_csus].to_list(), color='black', lineoffsets=1, linelengths=1)
# 		axs_fig_blocksOfTrialsSingleFish_begBoutProbability[s_i-1].eventplot(session.loc[session[bout_beg] != 0, time_trial_csus].to_list(), color='black', lineoffsets=1, linelengths=1)
# 		axs_fig_blocksOfTrialsSingleFish_endBoutProbability[s_i-1].eventplot(session.loc[session[bout_end] != 0, time_trial_csus].to_list(), color='black', lineoffsets=1, linelengths=1)
		

# 		# Arrange figures.

# 		# For angle.
# 		axs_fig_blocksOfTrialsSingleFish_angle[s_i-1].set(xlim=(-45,45), ylim=(-120,120), xticks=(np.arange(-40,41,10)), yticks=(np.arange(0,-120,120)))
# 		axs_fig_blocksOfTrialsSingleFish_angle[s_i-1].set_title(s_i, fontsize='small', loc='left')
# 		axs_fig_blocksOfTrialsSingleFish_angle[s_i-1].spines[:].set_visible(False)		

		
# 		# For activity.
# 		# axs_fig_blocksOfTrialsSingleFish_activity[s_i-1].set(xlim=(-45,45), ylim=(0,20), xticks=(np.arange(-40,41,10)), yticks=(np.arange(0,21,20)))
# 		# axs_fig_blocksOfTrialsSingleFish_activity[s_i-1].set_title(s_i, fontsize='small', loc='left')
# 		# axs_fig_blocksOfTrialsSingleFish_activity[s_i-1].spines[:].set_visible(False)


# 		# For bout.
# 		axs_fig_blocksOfTrialsSingleFish_boutProbability[s_i-1].set(xlim=(-45,45), xticks=(np.arange(-40,41,10)))
# 		axs_fig_blocksOfTrialsSingleFish_boutProbability[s_i-1].spines[:].set_visible(False)
# 		axs_fig_blocksOfTrialsSingleFish_boutProbability[s_i-1].axes.yaxis.set_ticks([])
# 		axs_fig_blocksOfTrialsSingleFish_boutProbability[s_i-1].set_title(s_i, fontsize='small', loc='left')

# 		# For bout_beg.
# 		axs_fig_blocksOfTrialsSingleFish_begBoutProbability[s_i-1].set(xlim=(-45,45), xticks=(np.arange(-40,41,10)))
# 		axs_fig_blocksOfTrialsSingleFish_begBoutProbability[s_i-1].set_title(s_i, fontsize='small', loc='left')
# 		axs_fig_blocksOfTrialsSingleFish_begBoutProbability[s_i-1].spines[:].set_visible(False)
# 		axs_fig_blocksOfTrialsSingleFish_begBoutProbability[s_i-1].axes.yaxis.set_ticks([])

# 		# For bout_end.
# 		axs_fig_blocksOfTrialsSingleFish_endBoutProbability[s_i-1].set(xlim=(-45,45), xticks=(np.arange(-40,41,10)))
# 		axs_fig_blocksOfTrialsSingleFish_endBoutProbability[s_i-1].set_title(s_i, fontsize='small', loc='left')
# 		axs_fig_blocksOfTrialsSingleFish_endBoutProbability[s_i-1].spines[:].set_visible(False)
# 		axs_fig_blocksOfTrialsSingleFish_endBoutProbability[s_i-1].axes.yaxis.set_ticks([])


# 		# Highlight stimuli in plots.

# 		cs_beg_array = session.loc[session[cs_beg] != 0, time_trial_csus].to_numpy()
# 		cs_end_array = session.loc[session[cs_end] != 0, time_trial_csus].to_numpy()

# 		us_beg_array = session.loc[session[us_beg] != 0, time_trial_csus].to_numpy()
# 		us_end_array = session.loc[session[us_end] != 0, time_trial_csus].to_numpy()

# 		if len(cs_end_array) < len(cs_beg_array):
# 			cs_end_array = np.append(cs_end_array, session.loc[:, time_trial_csus].iloc[-1])

# 		if len(us_end_array) < len(us_beg_array):
# 			us_end_array = np.append(us_end_array, session.loc[:, time_trial_csus].iloc[-1])

# 		for stim in range(len(cs_beg_array)):

# 			cs_beg_ = cs_beg_array[stim]
# 			cs_end_ = cs_end_array[stim]

# 			axs_fig_blocksOfTrialsSingleFish_angle[s_i-1].axvspan(cs_beg_, cs_end_, color='green', alpha=0.4, linewidth=0)

# 			# axs_fig_blocksOfTrialsSingleFish_activity[s_i-1].axvspan(cs_beg_, cs_end_, color='green', alpha=0.4, linewidth=0)
# 			axs_fig_blocksOfTrialsSingleFish_boutProbability[s_i-1].axvspan(cs_beg_, cs_end_, color='green', alpha=0.4, linewidth=0)
# 			axs_fig_blocksOfTrialsSingleFish_begBoutProbability[s_i-1].axvspan(cs_beg_, cs_end_, color='green', alpha=0.4, linewidth=0)
# 			axs_fig_blocksOfTrialsSingleFish_endBoutProbability[s_i-1].axvspan(cs_beg_, cs_end_, color='green', alpha=0.4, linewidth=0)

# 		for stim in range(len(us_beg_array)):

# 			us_beg_ = us_beg_array[stim]
# 			us_end_ = us_end_array[stim]

# 			axs_fig_blocksOfTrialsSingleFish_angle[s_i-1].axvspan(us_beg_, us_end_, color='red', alpha=0.4, linewidth=0)

# 			# axs_fig_blocksOfTrialsSingleFish_activity[s_i-1].axvspan(us_beg_, us_end_, color='red', alpha=0.4, linewidth=0)
# 			axs_fig_blocksOfTrialsSingleFish_boutProbability[s_i-1].axvspan(us_beg_, us_end_, color='red', alpha=0.4, linewidth=0)
# 			axs_fig_blocksOfTrialsSingleFish_begBoutProbability[s_i-1].axvspan(us_beg_, us_end_, color='red', alpha=0.4, linewidth=0)
# 			axs_fig_blocksOfTrialsSingleFish_endBoutProbability[s_i-1].axvspan(us_beg_, us_end_, color='red', alpha=0.4, linewidth=0)

# 		gc.collect()
			

# 	# Add labels to figures.

# 	fig_blocksOfTrialsSingleFish_angle.supxlabel('t (s)')
# 	fig_blocksOfTrialsSingleFish_angle.supylabel('Tail end angle (deg)')
# 	fig_blocksOfTrialsSingleFish_angle.suptitle('Tail end angle\n' + '_'.join(fish_id))

# 	# fig_blocksOfTrialsSingleFish_activity.supxlabel('t (s)')
# 	# fig_blocksOfTrialsSingleFish_activity.supylabel('Tail movement vigour (deg/ms)')
# 	# fig_blocksOfTrialsSingleFish_activity.suptitle('Tail movement vigour\n' + '_'.join(fish_id))

# 	fig_blocksOfTrialsSingleFish_boutProbability.supxlabel('t (s)')
# 	# fig_blocksOfTrialsSingleFish_boutProbability.supylabel('Tail movement periods')
# 	fig_blocksOfTrialsSingleFish_boutProbability.suptitle('Tail movement periods\n' + '_'.join(fish_id))

# 	fig_blocksOfTrialsSingleFish_begBoutProbability.supxlabel('t (s)')
# 	# fig_blocksOfTrialsSingleFish_begBoutProbability.supylabel('Start of tail movement')
# 	fig_blocksOfTrialsSingleFish_begBoutProbability.suptitle('Start of tail movement\n' + '_'.join(fish_id))

# 	fig_blocksOfTrialsSingleFish_endBoutProbability.supxlabel('t (s)')
# 	# fig_blocksOfTrialsSingleFish_endBoutProbability.supylabel('End of tail movement')
# 	fig_blocksOfTrialsSingleFish_endBoutProbability.suptitle('End of tail movement\n' + '_'.join(fish_id))


# 	# Save figures.

# 	#* Once I need to save as svg, rerun this script.
# 	fig_blocksOfTrialsSingleFish_angle.savefig(fig_blocksOfTrialsSingleFish_angle_name+'.svg', format='svg', facecolor='white', dpi=50)
# 	# fig_blocksOfTrialsSingleFish_activity.savefig(fig_blocksOfTrialsSingleFish_activity_name+'.svg', format='svg', facecolor='white', dpi=50)
# 	fig_blocksOfTrialsSingleFish_boutProbability.savefig(fig_blocksOfTrialsSingleFish_boutProbability_name+'.svg', format='svg', facecolor='white', dpi=50)
# 	fig_blocksOfTrialsSingleFish_begBoutProbability.savefig(fig_blocksOfTrialsSingleFish_begBoutProbability_name+'.svg', format='svg', facecolor='white', dpi=40)
# 	fig_blocksOfTrialsSingleFish_endBoutProbability.savefig(fig_blocksOfTrialsSingleFish_endBoutProbability_name+'.svg', format='svg', facecolor='white', dpi=40)

# 	plt.close('all')
# 	# plt.close(fig_blocksOfTrialsSingleFish_activity)
# 	# plt.close(fig_blocksOfTrialsSingleFish_boutProbability)
# 	# plt.close(fig_blocksOfTrialsSingleFish_begBoutProbability)
# 	# plt.close(fig_blocksOfTrialsSingleFish_endBoutProbability)

# 	gc.collect()



# def plotContinuousTracesSessionsSingleFish(data, csus, exp_type, sessions_csus, number_rows, fig_names):

# 	us_latency_ = condition_dict[exp_type][us_latency]

# 	data_csus = data[data[type_trial_csus] == csus]
	
# 	fish_id = data_csus.index[0]
	
# 	exp_type = data_csus.index.get_level_values('Exp.')[0]


# 	# Transform data_csus into a dense dataframe to be able to use groupby.
# 	data_csus[[activity, bout, bout_beg, bout_end]] = data_csus[[activity, bout, bout_beg, bout_end]].sparse.to_dense()


# 	fig_blocksOfTrialsSingleFish_activity_name, fig_blocksOfTrialsSingleFish_boutProbability_name, fig_blocksOfTrialsSingleFish_begBoutProbability_name, fig_blocksOfTrialsSingleFish_endBoutProbability_name = fig_names


# 	# Create session names.
# 	if csus == 'CS':

# 		names_sessions_csus = ['Pre-conditioning\n(CS−)  ' + (str(sessions_csus[0][0]) + ' to ' + str(sessions_csus[0][-1]))]
# 		for j in range(1, len(sessions_csus)-1):
# 			names_sessions_csus.append('Conditioning {}\n(CS-US)  {}'.format(j, str(sessions_csus[j][0]) + ' to ' + str(sessions_csus[j][-1])))
# 		names_sessions_csus.append('Post-conditioning\n(CS−)  ' + (str(sessions_csus[-1][0]) + ' to ' + str(sessions_csus[-1][-1])))
# 	else:
# 		names_sessions_csus = ['Conditioning {}\n(CS-US)  {}'.format(j, str(sessions_csus[j][0] + 1) + ' to ' + str(sessions_csus[j][-1] + 1)) for j in range(len(sessions_csus))]


# 	# Create the figures.
# 	fig_blocksOfTrialsSingleFish_activity, axs_fig_blocksOfTrialsSingleFish_activity = plt.subplots(number_rows, 1, sharex=True, sharey=True, facecolor='white', figsize=(20,20), constrained_layout=True)
# 	fig_blocksOfTrialsSingleFish_boutProbability, axs_fig_blocksOfTrialsSingleFish_boutProbability = plt.subplots(number_rows, 1, sharex=True, sharey=True, facecolor='white', figsize=(20,70), constrained_layout=True)
# 	fig_blocksOfTrialsSingleFish_begBoutProbability, axs_fig_blocksOfTrialsSingleFish_begBoutProbability = plt.subplots(number_rows, 1, sharex=True, sharey=True, facecolor='white', figsize=(20,70), constrained_layout=True)
# 	fig_blocksOfTrialsSingleFish_endBoutProbability, axs_fig_blocksOfTrialsSingleFish_endBoutProbability = plt.subplots(number_rows, 1, sharex=True, sharey=True, facecolor='white', figsize=(20,70), constrained_layout=True)


# 	for s_i, trials_in_s in enumerate(sessions_csus, start=1):

# 		session = f.pooled_session_stats(data_csus, trials_in_s)
# 			# # session is a slice of the original data, with each trial independent.
# 			# session = data_csus[data_csus[number_trial_csus].isin([str(t) for t in trials_in_s])]

			
# 			# session = session.groupby(by=time_trial_csus, as_index=True, dropna=False)[[activity, bout, bout_beg, bout_end]].agg({activity: ['count', 'mean', 'sem'], bout: 'mean', bout_beg: 'sum', bout_end: 'sum'})

# 		session.reset_index(time_trial_csus, drop=False, inplace=True)


# 		# Remove multi-index set on the column axis.
# 		session.columns = [time_trial_csus, count] + [' '.join(x) for x in session.iloc[:, 2:].columns.ravel()]


# 		# Confirm the number of elements in each timepoint.
# 		if not session[session[count] != len(trials_in_s)].empty:
# 			print('Missing timepoints in some of the trials!')
# 		#
# 			# max_time = int(session.loc[:, time_trial_csus].max().round())
# 			# time_intervals = np.arange(min_time, max_time+bin_interval, bin_interval)
# 			# bins = pd.cut(session.loc[:, time_trial_csus], time_intervals)
# 			# # , include_lowest=True)

# 			# session_2 = session.groupby(by=bins, as_index=True, dropna=False)[[bout_beg_sum, bout_end_sum]].agg({bout_beg_sum: ['count', 'mean'], bout_end_sum: 'mean'})

# 			# session_2.reset_index(time_trial_csus, drop=False, inplace=True)

# 			# # Remove multi-index set on the column axis.
# 			# session_2.columns = [time_trial_csus, count] + [' '.join(x) for x in session_2.iloc[:, 2:].columns.ravel()]


# 			# # Confirm number of elements in each timepoint.
# 			# session_2[session_2[count] != bin_interval]


# 			# # Transform time from categorical intervals of time to time as floats. Time assumes the mid timepoint the intervals.
# 			# session[time_trial_csus] = (time_intervals[:-1] + 0.5)


# 		# Convert time from frames to s.
# 		# data[time_trial_csus] = data[time_trial_csus] / expected_framerate  # s
# 		session[time_trial_csus] = session[time_trial_csus] / expected_framerate


# 		# Add data to figures.

# 		# For activity.
# 		time_ = session[time_trial_csus]
# 		activity_mean_ = session[activity_mean]
# 		activity_sem_ = session[activity_sem]
# 		axs_fig_blocksOfTrialsSingleFish_activity[s_i-1].plot(time_, activity_mean_, 'k')
# 		axs_fig_blocksOfTrialsSingleFish_activity[s_i-1].fill_between(time_, activity_mean_ - activity_sem_, activity_mean_ + activity_sem_, color='grey', linewidth=0, alpha=0.4)

# 		#For bout.
# 		axs_fig_blocksOfTrialsSingleFish_boutProbability[s_i-1].plot(time_, session[bout_mean], 'k')

# 		#For bout_beg and bout_end.
# 		min_time = int(session.loc[:, time_trial_csus].min().round())
# 		max_time = int(session.loc[:, time_trial_csus].max().round())
# 		time_intervals = np.arange(min_time, max_time+bin_interval, bin_interval)

# 		axs_fig_blocksOfTrialsSingleFish_begBoutProbability[s_i-1].hist(session.loc[session[bout_beg_sum] > 0, time_trial_csus], bins=time_intervals, density=True, histtype='stepfilled', color='black')

# 		axs_fig_blocksOfTrialsSingleFish_endBoutProbability[s_i-1].hist(session.loc[session[bout_end_sum] > 0, time_trial_csus], bins=time_intervals, density=True, histtype='stepfilled',color='black')


# 		# Arrange figures.

# 		# For activity.
# 		axs_fig_blocksOfTrialsSingleFish_activity[s_i-1].set(xlim=(-45,45), ylim=(0,20), xticks=(np.arange(-40,41,10)), yticks=(np.arange(0,21,20)))
# 		axs_fig_blocksOfTrialsSingleFish_activity[s_i-1].set_title(names_sessions_csus[s_i-1], loc='left')
# 		axs_fig_blocksOfTrialsSingleFish_activity[s_i-1].spines[:].set_visible(False)

# 		# For bout.
# 		axs_fig_blocksOfTrialsSingleFish_boutProbability[s_i-1].set(xlim=(-45,45), ylim=(0,1), xticks=(np.arange(-40,41,10)))
# 		axs_fig_blocksOfTrialsSingleFish_boutProbability[s_i-1].set_title(names_sessions_csus[s_i-1], loc='left')
# 		axs_fig_blocksOfTrialsSingleFish_boutProbability[s_i-1].spines[:].set_visible(False)
# 		# axs_fig_blocksOfTrialsSingleFish_boutProbability[s_i-1].axes.yaxis.set_ticks([])

# 		# For bout_beg.
# 		axs_fig_blocksOfTrialsSingleFish_begBoutProbability[s_i-1].set(xlim=(-45,45), ylim=(0,1), xticks=(np.arange(-40,41,10)))
# 		axs_fig_blocksOfTrialsSingleFish_begBoutProbability[s_i-1].set_title(names_sessions_csus[s_i-1], loc='left')
# 		axs_fig_blocksOfTrialsSingleFish_begBoutProbability[s_i-1].spines[:].set_visible(False)
# 		# axs_fig_blocksOfTrialsSingleFish_begBoutProbability[s_i-1].axes.yaxis.set_ticks([])

# 		# For bout_end.
# 		axs_fig_blocksOfTrialsSingleFish_endBoutProbability[s_i-1].set(xlim=(-45,45), ylim=(0,1), xticks=(np.arange(-40,41,10)))
# 		axs_fig_blocksOfTrialsSingleFish_endBoutProbability[s_i-1].set_title(names_sessions_csus[s_i-1], loc='left')
# 		axs_fig_blocksOfTrialsSingleFish_endBoutProbability[s_i-1].spines[:].set_visible(False)
# 		# axs_fig_blocksOfTrialsSingleFish_endBoutProbability[s_i-1].axes.yaxis.set_ticks([])


# 		# Highlight stimuli in plots.

# 		if csus == 'CS':
# 			axs_fig_blocksOfTrialsSingleFish_activity[s_i-1].axvspan(0, cs_duration, color='green', alpha=0.4, linewidth=0)
# 			axs_fig_blocksOfTrialsSingleFish_boutProbability[s_i-1].axvspan(0, cs_duration, color='green', alpha=0.4, linewidth=0)
# 			axs_fig_blocksOfTrialsSingleFish_begBoutProbability[s_i-1].axvspan(0, cs_duration, color='green', alpha=0.4, linewidth=0)
# 			axs_fig_blocksOfTrialsSingleFish_endBoutProbability[s_i-1].axvspan(0, cs_duration, color='green', alpha=0.4, linewidth=0)
		
# 			if s_i not in [1, len(sessions_csus)] and exp_type != 'control':
# 				axs_fig_blocksOfTrialsSingleFish_activity[s_i-1].axvspan(us_latency_, us_latency_ + us_duration, color='red', alpha=0.4, linewidth=0)
# 				axs_fig_blocksOfTrialsSingleFish_boutProbability[s_i-1].axvspan(us_latency_, us_latency_ + us_duration, color='red', alpha=0.4, linewidth=0)
# 				axs_fig_blocksOfTrialsSingleFish_begBoutProbability[s_i-1].axvspan(us_latency_, us_latency_ + us_duration, color='red', alpha=0.4, linewidth=0)
# 				axs_fig_blocksOfTrialsSingleFish_endBoutProbability[s_i-1].axvspan(us_latency_, us_latency_ + us_duration, color='red', alpha=0.4, linewidth=0)
# 		else:
# 			axs_fig_blocksOfTrialsSingleFish_activity[s_i-1].axvspan(0, us_duration, color='red', alpha=0.4, linewidth=0)
# 			axs_fig_blocksOfTrialsSingleFish_boutProbability[s_i-1].axvspan(0, us_duration, color='red', alpha=0.4, linewidth=0)
# 			axs_fig_blocksOfTrialsSingleFish_begBoutProbability[s_i-1].axvspan(0, us_duration, color='red', alpha=0.4, linewidth=0)
# 			axs_fig_blocksOfTrialsSingleFish_endBoutProbability[s_i-1].axvspan(0, us_duration, color='red', alpha=0.4, linewidth=0)


# 	# Add labels to figures.

# 	fig_blocksOfTrialsSingleFish_activity.supxlabel('t (s)')
# 	fig_blocksOfTrialsSingleFish_activity.supylabel('Tail movement vigour (deg/ms)')
# 	fig_blocksOfTrialsSingleFish_activity.suptitle('Tail movement vigour\n' + '_'.join(fish_id))

# 	fig_blocksOfTrialsSingleFish_boutProbability.supxlabel('t (s)')
# 	fig_blocksOfTrialsSingleFish_boutProbability.supylabel('Probability of tail movement')
# 	fig_blocksOfTrialsSingleFish_boutProbability.suptitle('Tail movement periods\n' + '_'.join(fish_id))

# 	fig_blocksOfTrialsSingleFish_begBoutProbability.supxlabel('t (s)')
# 	fig_blocksOfTrialsSingleFish_begBoutProbability.supylabel('Probability of start of tail movement')
# 	fig_blocksOfTrialsSingleFish_begBoutProbability.suptitle('Start of tail movement\n' + '_'.join(fish_id))

# 	fig_blocksOfTrialsSingleFish_endBoutProbability.supxlabel('t (s)')
# 	fig_blocksOfTrialsSingleFish_endBoutProbability.supylabel('Probability of end of tail movement')
# 	fig_blocksOfTrialsSingleFish_endBoutProbability.suptitle('End of tail movement\n' + '_'.join(fish_id))


# 	# Save figures.

# 	#* Once I need to save as svg, rerun this script.
# 	fig_blocksOfTrialsSingleFish_activity.savefig(fig_blocksOfTrialsSingleFish_activity_name+'.svg', format='svg', facecolor='white', dpi=300)
# 	fig_blocksOfTrialsSingleFish_boutProbability.savefig(fig_blocksOfTrialsSingleFish_boutProbability_name+'.svg', format='svg', facecolor='white', dpi=300)
# 	fig_blocksOfTrialsSingleFish_begBoutProbability.savefig(fig_blocksOfTrialsSingleFish_begBoutProbability_name+'.svg', format='svg', facecolor='white', dpi=300)
# 	fig_blocksOfTrialsSingleFish_endBoutProbability.savefig(fig_blocksOfTrialsSingleFish_endBoutProbability_name+'.svg', format='svg', facecolor='white', dpi=300)


# 	plt.close('all')










# *Plotly
'''
	def appendStimZonesTrialSingleFish(session, shapes, s_i):
		cs_beg_array = session.loc[session[cs_beg]
								  != 0, time_trial_csus].to_numpy()
		cs_end_array = session.loc[session[cs_end]
								  != 0, time_trial_csus].to_numpy()

		us_beg_array = session.loc[session[us_beg]
								  != 0, time_trial_csus].to_numpy()
		us_end_array = session.loc[session[us_end]
								  != 0, time_trial_csus].to_numpy()

		if len(cs_end_array) < len(cs_beg_array):
			cs_end_array = np.append(
				cs_end_array, session.loc[:, time_trial_csus].iloc[-1])

		if len(us_end_array) < len(us_beg_array):
			us_end_array = np.append(
				us_end_array, session.loc[:, time_trial_csus].iloc[-1])

		for stim in range(len(cs_beg_array)):

			cs_beg_ = cs_beg_array[stim]
			cs_end_ = cs_end_array[stim]

			shapes.append(go.layout.Shape(type='rect', xref='x' + str(s_i), x0=cs_beg_, x1=cs_end_, yref='y{} domain'.format(
				str(s_i) if s_i != 1 else ''), y0=0, y1=1, opacity=0.4, line_width=0, fillcolor='green'))

		for stim in range(len(us_beg_array)):

			us_beg_ = us_beg_array[stim]
			us_end_ = us_end_array[stim]

			shapes.append(go.layout.Shape(type='rect', xref='x' + str(s_i), x0=us_beg_, x1=us_end_, yref='y{} domain'.format(
				str(s_i) if s_i != 1 else ''), y0=0, y1=1, opacity=0.4, line_width=0, fillcolor='red'))

		return shapes

	def plotContinuousTracesTrialsSingleFish(data, csus, sessions_csus, number_rows, fig_names):

		data_csus = data[data[type_trial_csus] == csus]

		fig_blocksOfTrialsSingleFish_activity_name, fig_blocksOfTrialsSingleFish_boutProbability_name, fig_blocksOfTrialsSingleFish_begBoutProbability_name, fig_blocksOfTrialsSingleFish_endBoutProbability_name = fig_names

		# if csus == 'cs':
		#	sessions_csus = np.arange(1, expected_number_cs)
		# if csus == 'us':
		#	sessions_csus = np.arange(1, expected_number_us)
		# sorted([int(i) for i in data_csus[number_trial_csus].cat.categories])

		sessions_csus = [int(i) for i in sessions_csus]

		fig_blocksOfTrialsSingleFish_activity = make_subplots(rows=number_rows, cols=1, x_title='t (s)', y_title=activity, shared_xaxes=True, subplot_titles = sessions_csus)
		fig_blocksOfTrialsSingleFish_boutProbability = make_subplots(rows=number_rows, cols=1, x_title='t (s)', y_title='Tail movement', shared_xaxes=True, subplot_titles = sessions_csus)
		fig_blocksOfTrialsSingleFish_begBoutProbability = make_subplots(rows=number_rows, cols=1, x_title='t (s)', y_title='Start tail movement', shared_xaxes=True, subplot_titles = sessions_csus)
		fig_blocksOfTrialsSingleFish_endBoutProbability = make_subplots(rows=number_rows, cols=1, x_title='t (s)', y_title='End tail movement', shared_xaxes=True, subplot_titles = sessions_csus)
		# fig_blocksOfTrialsSingleFish_boutProbability = make_subplots(rows=number_rows, cols=1, x_title='t (s)', y_title='P<sub>tail movement</sub>', shared_xaxes=True, subplot_titles = sessions_csus)
		# fig_blocksOfTrialsSingleFish_begBoutProbability = make_subplots(rows=number_rows, cols=1, x_title='t (s)', y_title='P<sub>start tail movement</sub>', shared_xaxes=True, subplot_titles = sessions_csus)
		# fig_blocksOfTrialsSingleFish_endBoutProbability = make_subplots(rows=number_rows, cols=1, x_title='t (s)', y_title='P<sub>end tail movement</sub>', shared_xaxes=True, subplot_titles = sessions_csus)
		shapes = []

		for s_i, trials_in_s in enumerate(sessions_csus, start=1):

			session = data_csus[data_csus[number_trial_csus] == str(trials_in_s)]

			
			# Add shapes highlighting when cs and us. Need to be done here because of single trials case.
			shapes = appendStimZonesTrialSingleFish(session, shapes, s_i)
			
			session = session[::downsampling_step]

			x_vals = session[time_trial_csus]

			fig_blocksOfTrialsSingleFish_activity.add_trace(go.Scatter(x=x_vals, y=session[activity], line_color='black', name='tail movement', legendgroup= 'tail movement', showlegend=False), row=s_i, col=1)


			# *This is an alternative to mimick eventplot from Matplotlib
			# fig_blocksOfTrialsSingleFish_boutProbability.add_trace(go.Box(
			#	x=data.loc[data[bout]!=0, time_trial_csus], 
			#	marker_symbol='line-ns-open', 
			#	marker_color='black',
			#	boxpoints='all',
			#	jitter=0,
			#	fillcolor='rgba(255,255,255,0)',
			#	line_color='rgba(255,255,255,0)',
			#	hoveron='points',
			# ), row=s_i, col=1)
			fig_blocksOfTrialsSingleFish_boutProbability.add_trace(go.Scatter(x=x_vals, y=session[bout], line_color='black', fill='tozeroy', fillcolor='black', mode='none', name='tail movement', legendgroup='tail movement', showlegend=False), row=s_i, col=1)

			fig_blocksOfTrialsSingleFish_begBoutProbability.add_trace(go.Scatter(x=x_vals, y=session[bout_beg], line_color='black', fill='tozeroy', fillcolor='black', mode='none', name='start of tail movement', legendgroup='start of tail movement', showlegend=False), row=s_i, col=1)
			fig_blocksOfTrialsSingleFish_endBoutProbability.add_trace(go.Scatter(x=x_vals, y=session[bout_end], line_color='black', fill='tozeroy', fillcolor='black', mode='none', name='end of tail movement', legendgroup='end of tail movement', showlegend=False), row=s_i, col=1)
			# fig_blocksOfTrialsSingleFish_boutProbability.add_trace(go.Scatter(x=x_vals, y=session[bout], line_color='black', fill='tozeroy', fillcolor='black', mode='none', name='tail movement probability', legendgroup='tail movement probability', showlegend=False), row=s_i, col=1)
			# fig_blocksOfTrialsSingleFish_begBoutProbability.add_trace(go.Scatter(x=x_vals, y=session[bout_beg], line_color='black', name='start of tail movement probability', legendgroup='start of tail movement probability', showlegend=False), row=s_i, col=1)
			# fig_blocksOfTrialsSingleFish_endBoutProbability.add_trace(go.Scatter(x=x_vals, y=session[bout_end], line_color='black', name='end of tail movement probability', legendgroup='end of tail movement probability', showlegend=False), row=s_i, col=1)


			fig_blocksOfTrialsSingleFish_activity.update_xaxes(showgrid=False, zeroline=True, automargin=True, row=s_i, col=1)
			fig_blocksOfTrialsSingleFish_boutProbability.update_xaxes(showgrid=False, zeroline=False, automargin=True, row=s_i, col=1)
			fig_blocksOfTrialsSingleFish_begBoutProbability.update_xaxes(showgrid=False, zeroline=False, automargin=True, row=s_i, col=1)
			fig_blocksOfTrialsSingleFish_endBoutProbability.update_xaxes(showgrid=False, zeroline=False, automargin=True, row=s_i, col=1)

			fig_blocksOfTrialsSingleFish_activity.update_yaxes(showgrid=False, zeroline=False, zerolinecolor='black', automargin=True, row=s_i, col=1, range=[0, 20], nticks = 2)
			fig_blocksOfTrialsSingleFish_boutProbability.update_yaxes(showgrid=False, zeroline=False, zerolinecolor='black', automargin=True, row=s_i, col=1, range=[0, 1], nticks = 2, visible=False)
			fig_blocksOfTrialsSingleFish_begBoutProbability.update_yaxes(showgrid=False, zeroline=False, zerolinecolor='black', automargin=True, row=s_i, col=1, range=[0, 1], nticks = 2, visible=False)
			fig_blocksOfTrialsSingleFish_endBoutProbability.update_yaxes(showgrid=False, zeroline=False, zerolinecolor='black', automargin=True, row=s_i, col=1, range=[0, 1], nticks = 2, visible=False)

			# fig_blocksOfTrialsSingleFish_activity.layout.annotations[s_i-1].update(y=fig_blocksOfTrialsSingleFish_activity.layout.annotations[s_i-1].y-0.0001)
			# fig_blocksOfTrialsSingleFish_boutProbability.layout.annotations[s_i-1].update(y=fig_blocksOfTrialsSingleFish_activity.layout.annotations[s_i-1].y-0.0001)
			# fig_blocksOfTrialsSingleFish_begBoutProbability.layout.annotations[s_i-1].update(y=fig_blocksOfTrialsSingleFish_activity.layout.annotations[s_i-1].y-0.0001)
			# fig_blocksOfTrialsSingleFish_endBoutProbability.layout.annotations[s_i-1].update(y=fig_blocksOfTrialsSingleFish_activity.layout.annotations[s_i-1].y-0.0001)




		fig_blocksOfTrialsSingleFish_activity.update_layout(height=4500, width=2000, showlegend=False, plot_bgcolor='rgba(0,0,0,0)', shapes=shapes)
		fig_blocksOfTrialsSingleFish_boutProbability.update_layout(height=4500, width=2000, showlegend=False, plot_bgcolor='rgba(0,0,0,0)', shapes=shapes)
		fig_blocksOfTrialsSingleFish_begBoutProbability.update_layout(height=4500, width=2000, showlegend=False, plot_bgcolor='rgba(0,0,0,0)', shapes=shapes)
		fig_blocksOfTrialsSingleFish_endBoutProbability.update_layout(height=4500, width=2000, showlegend=False, plot_bgcolor='rgba(0,0,0,0)', shapes=shapes)


		py.io.write_html(fig=fig_blocksOfTrialsSingleFish_activity, file=fig_blocksOfTrialsSingleFish_activity_name + '.html', auto_open=True, include_plotlyjs='cdn', full_html=False, validate = False)
		py.io.write_html(fig=fig_blocksOfTrialsSingleFish_boutProbability, file=fig_blocksOfTrialsSingleFish_boutProbability_name + '.html', auto_open=True, include_plotlyjs='cdn', full_html=False, validate = False)
		py.io.write_html(fig=fig_blocksOfTrialsSingleFish_begBoutProbability, file=fig_blocksOfTrialsSingleFish_begBoutProbability_name + '.html', auto_open=True, include_plotlyjs='cdn', full_html=False, validate = False)
		py.io.write_html(fig=fig_blocksOfTrialsSingleFish_endBoutProbability, file=fig_blocksOfTrialsSingleFish_endBoutProbability_name + '.html', auto_open=True, include_plotlyjs='cdn', full_html=False, validate = False)


		# fig_blocksOfTrialsSingleFish_activity.write_image(fig_blocksOfTrialsSingleFish_activity_name + '.svg', engine='kaleido')
		# fig_blocksOfTrialsSingleFish_boutProbability.write_image(fig_blocksOfTrialsSingleFish_boutProbability_name + '.svg', engine='kaleido')
		# fig_blocksOfTrialsSingleFish_begBoutProbability.write_image(fig_blocksOfTrialsSingleFish_begBoutProbability_name + '.svg', engine='kaleido')
		# fig_blocksOfTrialsSingleFish_endBoutProbability.write_image(fig_blocksOfTrialsSingleFish_endBoutProbability_name + '.svg', engine='kaleido')


	def appendStimZonesSessionPooledData(session, shapes, sessions_csus, s_i, cS_duration, uS_duration, uS_latency):

		if csus == 'cs':

			shapes.append(go.layout.Shape(type='rect', xref='x', x0=0, x1=cS_duration, yref='y{} domain'.format(str(s_i) if s_i!=1 else ''), y0=0, y1=1, opacity=0.4, line_width=0, fillcolor='green'))

			if s_i not in [1, len(sessions_csus)] and exp_type != 'control':
				# if exp_type == 'trace1500CC':
				shapes.append(go.layout.Shape(type='rect', xref='x' + str(s_i), x0=uS_latency, x1=uS_latency + uS_duration, yref='y{} domain'.format(str(s_i) if s_i!=1 else ''), y0=0, y1=1, opacity=0.4, line_width=0, fillcolor='red'))

		else:
			shapes.append(go.layout.Shape(type='rect', xref='x' + str(s_i), x0=0, x1=uS_duration, yref='y{} domain'.format(str(s_i) if s_i!=1 else ''), y0=0, y1=1, opacity=0.4, line_width=0, fillcolor='red'))

		return shapes

	def plotContinuousTracesSessionsSingleFish(data, csus, sessions_csus, number_rows, fig_names):

		data_csus = data[data[type_trial_csus] == csus]

		# Transform data_csus into a dense dataframe to be able to use groupby.
		data_csus[[activity, bout, bout_beg, bout_end]] = data_csus[[activity, bout, bout_beg, bout_end]].sparse.to_dense()

		fig_blocksOfTrialsSingleFish_activity_name, fig_blocksOfTrialsSingleFish_boutProbability_name, fig_blocksOfTrialsSingleFish_begBoutProbability_name, fig_blocksOfTrialsSingleFish_endBoutProbability_name = fig_names

		if csus == 'cs':

			names_sessions_csus = ['pre-conditioning\n(cs−)  ' + (str(sessions_csus[0][0]) + ' to ' + str(sessions_csus[0][-1]))]
			for j in range(1, len(sessions_csus)-1):
				names_sessions_csus.append('conditioning {}\n(cs-us)  {}'.format(j, str(sessions_csus[j][0]) + ' to ' + str(sessions_csus[j][-1])))
			names_sessions_csus.append('post-conditioning\n(cs−)  ' + (str(sessions_csus[-1][0]) + ' to ' + str(sessions_csus[-1][-1])))
		else:
			names_sessions_csus = ['conditioning {}\n(cs-us)  {}'.format(j, str(sessions_csus[j][0] + 1) + ' to ' + str(sessions_csus[j][-1] + 1)) for j in range(len(sessions_csus))]
		# number_rows = len(names_sessions_csus)

		fig_blocksOfTrialsSingleFish_activity = make_subplots(rows=number_rows, cols=1, x_title='t (s)', y_title=activity, shared_xaxes=True, subplot_titles = names_sessions_csus)
		fig_blocksOfTrialsSingleFish_boutProbability = make_subplots(rows=number_rows, cols=1, x_title='t (s)', y_title='P<sub>tail movement</sub>', shared_xaxes=True, subplot_titles = names_sessions_csus)
		fig_blocksOfTrialsSingleFish_begBoutProbability = make_subplots(rows=number_rows, cols=1, x_title='t (s)', y_title='P<sub>start tail movement</sub>', shared_xaxes=True, subplot_titles = names_sessions_csus)
		fig_blocksOfTrialsSingleFish_endBoutProbability = make_subplots(rows=number_rows, cols=1, x_title='t (s)', y_title='P<sub>end tail movement</sub>', shared_xaxes=True, subplot_titles = names_sessions_csus)

		shapes = []

		for s_i, trials_in_s in enumerate(sessions_csus, start=1):

			session = data_csus[data_csus[number_trial_csus].isin([str(b) for b in trials_in_s])]


			# Add shapes highlighting when cs and us. Need to be done here because of single trials case.
			shapes = appendStimZonesSessionPooledData(session, shapes, sessions_csus, s_i, cS_duration, uS_duration, uS_latency)


			# Bin data when pooling data from different trials.
			min_time = int(session.loc[:, time_trial_csus].min().round())
			max_time = int(session.loc[:, time_trial_csus].max().round())
			# Each bin has to contain at least one frame. So, for 700 FPS it has to be >=1/700 s.
			bin_interval = (np.ceil((1/(predicted_framerate/1000))))/1000
			time_intervals = np.arange(min_time, max_time+bin_interval, bin_interval)
			bins = pd.cut(session.loc[:, time_trial_csus], time_intervals, include_lowest=True)


			session = session.groupby(by=bins, as_index=True, dropna=False)[[activity, bout, bout_beg, bout_end]].agg({activity:['count', 'mean', 'sem'], bout:['mean', 'sem'], bout_beg:['mean', 'sem'], bout_end:['mean', 'sem']})

			session = session.reset_index(time_trial_csus, drop=False)


			# Remove multi-index set on the column axis.
			session.columns = [time_trial_csus, Count] + [' '.join(x) for x in session.iloc[:, 2:].columns.ravel()]


			# Transform time from categorical intervals of time to time as floats. Time assumes the mid timepoint the intervals.
			session[time_trial_csus] = (time_intervals[:-1] + 0.5)

			session.dropna(inplace=True)

			# session[cols_stats] = session[cols_stats].rolling(window=filtering_window, center=True).mean().dropna()

			# session[cols_stats].rolling(window=filtering_window, center=True).mean().dropna()

			 # session = session.rolling(window=filtering_window, center=True).mean()

				# session_activity = session.groupby(time_trial_csus)[activity].mean().reset_index()
				# session_boutP = session.groupby(time_trial_csus)[bout].mean().reset_index()
				# session_begBoutP = session.groupby(time_trial_csus)[bout_beg].mean().reset_index()
				# session_endBoutP = session.groupby(time_trial_csus)[bout_end].mean().reset_index()

				# # Filter for plotting purposes
				# session_activity_smooth = session_activity.rolling(window=filtering_window, center=True).mean()
				# session_boutP_smooth = session_boutP.rolling(window=filtering_window, center=True).mean()
				# session_begBoutP_smooth = session_begBoutP.rolling(window=filtering_window, center=True).mean()
				# session_endBoutP_smooth = session_endBoutP.rolling(window=filtering_window, center=True).mean()


			# if s_i == 1:
			#	showlegend=True
			# else:
			#	showlegend=False


			# PART TO PLOT SEM

			x_vals = session[time_trial_csus].to_list() + session[time_trial_csus].to_list()[::-1]

			# fig_blocksOfTrialsPooledFish_activity.add_trace(go.Scatter(x=session[time_session_cSus], y=session[activity], line_color=line_color, name=condition, legendgroup=condition, showlegend=showlegend, opacity=0.7), row=s, col=1)

			# fig_blocksOfTrialsPooledFish_activity.add_trace(go.Scatter(x=x_vals[::downsampling_step], y=y_vals[::downsampling_step], line_width=0, mode='lines', fillcolor=line_color, legendgroup=condition, showlegend=False, fill='toself', opacity=0.5), row=s, col=1)


			fig_blocksOfTrialsSingleFish_activity.add_trace(go.Scatter(x=x_vals, y=session[activity_mean], line_color='black', name='tail movement', legendgroup='tail movement', showlegend=False), row=s_i, col=1)

			y_vals = (session[activity_mean] + session[activity_sem]).to_list() + (session[activity_mean] - session[activity_sem]).to_list()[::-1]

			fig_blocksOfTrialsSingleFish_activity.add_trace(go.Scatter(x=x_vals, y=y_vals, line_width=0, mode='lines', fillcolor='grey', fill='toself', legendgroup='tail movement', showlegend=False), row=s_i, col=1)


			fig_blocksOfTrialsSingleFish_boutProbability.add_trace(go.Scatter(x=x_vals, y=session[bout_mean], line_color='black', name='tail movement probability', legendgroup='tail movement probability', showlegend=False), row=s_i, col=1)

			y_vals = (session[bout_mean] + session[bout_sem]).to_list() + (session[bout_mean] - session[bout_sem]).to_list()[::-1]

			fig_blocksOfTrialsSingleFish_boutProbability.add_trace(go.Scatter(x=x_vals, y=y_vals, line_width=0, mode='lines', fillcolor='grey', fill='toself', legendgroup='tail movement probability', showlegend=False), row=s_i, col=1)


			fig_blocksOfTrialsSingleFish_begBoutProbability.add_trace(go.Scatter(x=x_vals, y=session[bout_beg_mean], line_color='black', name='start of tail movement probability', legendgroup='start of tail movement probability', showlegend=False), row=s_i, col=1)

			y_vals = (session[bout_beg_mean] + session[bout_beg_sem]).to_list() + (session[bout_beg_mean] - session[bout_beg_sem]).to_list()[::-1]

			fig_blocksOfTrialsSingleFish_begBoutProbability.add_trace(go.Scatter(x=x_vals, y=y_vals, line_width=0, mode='lines', fillcolor='grey', fill='toself', legendgroup='start of tail movement probability', showlegend=False), row=s_i, col=1)


			fig_blocksOfTrialsSingleFish_endBoutProbability.add_trace(go.Scatter(x=x_vals, y=session[bout_end_mean], line_color='black', name='end of tail movement probability', legendgroup='end of tail movement probability', showlegend=False), row=s_i, col=1)

			y_vals = (session[bout_end_mean] + session[bout_end_sem]).to_list() + (session[bout_end_mean] - session[bout_end_sem]).to_list()[::-1]

			fig_blocksOfTrialsSingleFish_endBoutProbability.add_trace(go.Scatter(x=x_vals, y=y_vals, line_width=0, mode='lines', fillcolor='grey', fill='toself', legendgroup='end of tail movement probability', showlegend=False), row=s_i, col=1)


			fig_blocksOfTrialsSingleFish_activity.update_xaxes(showgrid=False, zeroline=True, automargin=True, row=s_i, col=1)
			fig_blocksOfTrialsSingleFish_boutProbability.update_xaxes(showgrid=False, zeroline=True, automargin=True, row=s_i, col=1)
			fig_blocksOfTrialsSingleFish_begBoutProbability.update_xaxes(showgrid=False, zeroline=True, automargin=True, row=s_i, col=1)
			fig_blocksOfTrialsSingleFish_endBoutProbability.update_xaxes(showgrid=False, zeroline=True, automargin=True, row=s_i, col=1)

			fig_blocksOfTrialsSingleFish_activity.update_yaxes(showgrid=False, zeroline=False, zerolinecolor='black', automargin=True, row=s_i, col=1, range=[0, 20], nticks=2)
			fig_blocksOfTrialsSingleFish_boutProbability.update_yaxes(showgrid=False, zeroline=False, zerolinecolor='black', automargin=True, row=s_i, col=1, range=[0, 1], nticks=2)
			fig_blocksOfTrialsSingleFish_begBoutProbability.update_yaxes(showgrid=False, zeroline=False, zerolinecolor='black', automargin=True, row=s_i, col=1, range=[0, 1], nticks=2)
			fig_blocksOfTrialsSingleFish_endBoutProbability.update_yaxes(showgrid=False, zeroline=False, zerolinecolor='black', automargin=True, row=s_i, col=1, range=[0, 1], nticks=2)


		#fig_blocksOfTrialsSingleFish_activity.update_layout(height=4500, width=2000, showlegend=True, plot_bgcolor='rgba(0,0,0,0)', title_text='filtered with rolling average (window={} frames)'.format(filtering_window), shapes=shapes)

		#fig_blocksOfTrialsSingleFish_boutProbability.update_layout(height=4500, width=2000, showlegend=True, plot_bgcolor='rgba(0,0,0,0)', title_text='filtered with rolling average (window={} frames)'.format(filtering_window), shapes=shapes)
		fig_blocksOfTrialsSingleFish_activity.update_layout(height=4500, width=2000, showlegend=True, plot_bgcolor='rgba(0,0,0,0)', shapes=shapes)
		fig_blocksOfTrialsSingleFish_boutProbability.update_layout(height=4500, width=2000, showlegend=True, plot_bgcolor='rgba(0,0,0,0)', shapes=shapes)
		fig_blocksOfTrialsSingleFish_begBoutProbability.update_layout(height=4500, width=2000, showlegend=True, plot_bgcolor='rgba(0,0,0,0)', shapes=shapes)
		fig_blocksOfTrialsSingleFish_endBoutProbability.update_layout(height=4500, width=2000, showlegend=True, plot_bgcolor='rgba(0,0,0,0)', shapes=shapes)
		py.io.write_html(fig=fig_blocksOfTrialsSingleFish_activity, file=fig_blocksOfTrialsSingleFish_activity_name + '.html', auto_open=False, include_plotlyjs='cdn', full_html=False, validate = False)
		py.io.write_html(fig=fig_blocksOfTrialsSingleFish_boutProbability, file=fig_blocksOfTrialsSingleFish_boutProbability_name + '.html', auto_open=False, include_plotlyjs='cdn', full_html=False, validate = False)
		py.io.write_html(fig=fig_blocksOfTrialsSingleFish_begBoutProbability, file=fig_blocksOfTrialsSingleFish_begBoutProbability_name + '.html', auto_open=False, include_plotlyjs='cdn', full_html=False, validate = False)
		py.io.write_html(fig=fig_blocksOfTrialsSingleFish_endBoutProbability, file=fig_blocksOfTrialsSingleFish_endBoutProbability_name + '.html', auto_open=False, include_plotlyjs='cdn', full_html=False, validate = False)
		#fig_blocksOfTrialsSingleFish_activity.write_image(fig_blocksOfTrialsSingleFish_activity_name + 'svg', engine='kaleido')
		#fig_blocksOfTrialsSingleFish_boutProbability.write_image(fig_blocksOfTrialsSingleFish_boutProbability + 'svg', engine='kaleido')
		#fig_blocksOfTrialsSingleFish_begBoutProbability.write_image(fig_blocksOfTrialsSingleFish_begBoutProbability + 'svg', engine='kaleido')
		#fig_blocksOfTrialsSingleFish_endBoutProbability.write_image(fig_blocksOfTrialsSingleFish_endBoutProbability_name + 'svg', engine='kaleido')
'''
