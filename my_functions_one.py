from pathlib import Path
import plotly as py
from plotly import graph_objs as go
from plotly.subplots import make_subplots
from scipy import interpolate
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from timeit import default_timer as timer
from pandas.api.types import CategoricalDtype

from my_general_variables import *
from my_experiment_specific_variables import expected_number_cs

plt.style.use('classic')


def msg(stem_fish_path_orig, message):
	
	if type(message) is list:
		message = '\t'.join([str(i) for i in message])

	return [stem_fish_path_orig] + ['\t' + message + '\n']

	# return [stem_fish_path_orig + '\t' + message + '\n']

def save_info(protocol_info_path, stem_fish_path_orig, message):

	message = msg(stem_fish_path_orig, message)
	print(message)

	with open(protocol_info_path, 'a') as file:
		file.writelines(message)

def fish_id(stem_path):
	# Info about a specific fish.
	
	stem_fish_path = stem_path.lower()
	stem_fish_path = stem_fish_path.split('_')
	day = stem_fish_path[0]
	strain = stem_fish_path[1]
	age = stem_fish_path[2].replace('dpf', '')
	exp_type = stem_fish_path[3]
	rig = stem_fish_path[4]
	fish_number = stem_fish_path[5].replace('fish', '')

	return day, strain, age, exp_type, rig, fish_number

def read_initial_abs_time(camera_path):
	# Read the absolute time at the beginning of the experiment.

	try:
		with open(camera_path, 'r') as f:
			f.readline()
		# Previous version.
			# first_frame_absolute_time = int(float(f.readline().strip('\n').split('\t')[2]))

		# return first_frame_absolute_time

			return int(float(f.readline().strip('\n').split('\t')[2]))

	except:
		print('No absolute time in cam file.')

		return None

def read_camera(camera_path):

	try:
		start = timer()
		
		camera = pd.read_csv(str(camera_path), sep='\t', header=0, decimal=',')
		
		print('Time to read cam.txt: {} (s)'.format(timer()-start))

		camera.rename(columns={'TotalTime' : ela_time}, inplace=True)
		
		return camera

	except:

		print('Cannot read camera file.')
		
		return None


# def read_sync_reader(sync_reader_path):

# 	try:
# 		start = timer()
		
# 		sync_reader = pd.read_csv(str(sync_reader_path.replace('cam', 'scape sync reader')), sep=' ', header=0, decimal='.', usecols=[0,1,4])
		
# 		print('Time to read scape sync reader.txt: {} (s)'.format(timer()-start))

# 		sync_reader.rename(columns={'Time' : ela_time}, inplace=True)
		
# 		return sync_reader

# 	except:
# 		# print('Cannot read camera file.')

# 		return None


def framerate_and_reference_frame(camera, first_frame_absolute_time, number_frames_discard_beg, protocol_info_path, stem_fish_path_orig, fig_camera_name):

	camera_diff = camera.loc[:, ela_time].diff()


	# First estimate of the interframe interval, using the median
	print('Max IFI: {} ms'.format(camera_diff.max()))
	ifi = camera_diff.iloc[number_frames_discard_beg : ].median()
	print('First estimate of IFI: {} ms'.format(ifi))


	camera_diff_index_right_IFI = np.where(abs(camera_diff - ifi) <= max_interval_between_frames)[0]

	camera_diff_index_right_IFI_diff = np.diff(camera_diff_index_right_IFI)

	#* Find a region at the beginning where the IFI from frame to frame does not vary significantly and is similar to the first estimate of the true IFI (ifi).
	for i in range(1, len(camera_diff_index_right_IFI_diff)):

		if camera_diff_index_right_IFI_diff[i-1] == 1 and camera_diff_index_right_IFI_diff[i] == 1:

			reference_frame_id = camera.iloc[camera_diff_index_right_IFI[i] - 1, 0]

			# first_frame_absolute_time is not None when there is absolute time in the cam file.
			if first_frame_absolute_time is not None:
				reference_frame_time = first_frame_absolute_time + camera.iloc[camera_diff_index_right_IFI[i] - 1, 1] - camera.iloc[0, 1]
			else:
				reference_frame_time = None

			break

	#* Find a similar region but at the end of the experiment.
	for i in range(len(camera_diff_index_right_IFI_diff)-1, 0, -1):

		if camera_diff_index_right_IFI_diff[i-1] == 1 and camera_diff_index_right_IFI_diff[i] == 1:
			
			last_frame_id = camera.iloc[camera_diff_index_right_IFI[i] - 1, 0]
			#last_frame_time = first_frame_absolute_time + camera[time].iloc[camera_diff_index_right_IFI[i] - 1] - camera[time].iloc[0]

			break


	#* Second estimate of the interframe interval, using the mean, and assuming there is no increasing accumulation of frames in the buffer during the experiment; Only the region between the two frames identified in the previous two for loops is considered.
	ifi = camera_diff.iloc[reference_frame_id - camera.iloc[0,0] : last_frame_id - camera.iloc[0,0]].mean()

	print('Second estimate of IFI: {} ms'.format(ifi))
	predicted_framerate = 1000 / ifi
	print('Estimated framerate: {} FPS'.format(predicted_framerate))


	def lost_frames(camera, camera_diff, ifi, protocol_info_path, stem_fish_path_orig, fig_camera_name):


		# Delay to capture frames by the computer
		delay = (camera_diff - ifi).cumsum().to_numpy()


		# Number of lost frames
		# More than one frame might be lost and number_frames_lost sometimes is not monotonically crescent (can go down when some ms are 'recovered').
		number_frames_lost = np.floor(delay / (ifi * buffer_size))
		#TODO use this to speed up
		# number_frames_lost = np.max(number_frames_lost, 0, axis)
		number_frames_lost = np.where(number_frames_lost>=0, number_frames_lost, 0)

		number_frames_lost_diff = np.floor(np.diff(number_frames_lost))
		number_frames_lost_diff = np.where(number_frames_lost_diff>=0,number_frames_lost_diff,0)


		# Indices where frames were potentially lost
		where_frames_lost = np.where(number_frames_lost_diff > 0)[0]


		# Total number of missed frames
		if (Lost_frames := len(where_frames_lost) > 0):
			print('Total number of lost frames: ', len(where_frames_lost))
			print('Where: ', where_frames_lost)
			save_info(protocol_info_path, stem_fish_path_orig, 'Lost frames.')
		else:
			print('No frames were lost.')

		fig, axs = plt.subplots(5, 1, sharex=True, facecolor='white', figsize=(20, 40), constrained_layout=True)

		axs[0].plot(camera.iloc[:,1],'k')
		axs[0].set_ylabel('Elapsed time (ms)')
		axs[0].set_title('Estimated IFI: {} ms.    Estimated framerate: {} FPS'.format(round(ifi, 3), round(predicted_framerate, 3)))

		axs[1].plot(camera_diff,'k')
		axs[1].set_ylabel('IFI (ms)')

		axs[2].plot(delay,'k')
		axs[2].set_ylabel('Delay (ms)')

		axs[3].plot(number_frames_lost_diff.cumsum(),'k')
		axs[3].set_ylabel('Cumulative number of lost frames')

		axs[4].plot(number_frames_lost_diff,'black')
		# axs[4].set_xlabel('frame number')
		axs[4].set_ylabel('Lost frames')

		fig.supxlabel('Frame number')
		plt.suptitle('Analysis of lost frames\n' + stem_fish_path_orig)

		fig.savefig(fig_camera_name, dpi=100, facecolor='white')
		plt.close(fig)


		# Correct frame IDs in camera dataframe.
		# correctedID = np.zeros(len(camera))

		# for i in tqdm(where_frames_lost):
		# 	correctedID[i:number_frames_diff] += 1 # And not correctedID[i:] += 1 because, when the buffer is full, the Mako U29-B camera keeps what is already in the buffer and does not receive any new frames while the buffer is full.

		# del where_frames_lost, number_frames_lost_diff

		# camera['Corrected ID'] = camera['ID'] + correctedID
		# camera['Corrected ID'] = camera['Corrected ID'].astype('int')

		# # Second estimate of the interframe interval, using the median, and after estimating where there are missing frames 
		# camera_diff = camera.loc[:,'ElapsedTime'].diff()
		# ifi = camera_diff.iloc[number_frames_discard_beg : -number_frames_discard_beg].median()
		# print('\nFirst estimate of IFI: {} ms'.format(ifi))

		return Lost_frames


	Lost_frames = lost_frames(camera, camera_diff, ifi, protocol_info_path, stem_fish_path_orig, fig_camera_name)




	return predicted_framerate, reference_frame_id, reference_frame_time, Lost_frames

def read_protocol(protocol_path, reference_frame_time_or_id, protocol_info_path, stem_fish_path_orig):


	#* Read protocol file.
	if Path(protocol_path).exists():
		# Discarding the last column, which contains the cumulative number of bouts identified in C#.
		protocol = pd.read_csv(str(protocol_path), sep='\t', header=0, names=[experiment_type, beg, end], usecols=[0, 1, 2], index_col=0)
	
	else:
		save_info(protocol_info_path, stem_fish_path_orig, 'Stim control file does not exist.')
		
		return None

	#* Were the stimuli timings not saved?
	if protocol.empty:
		save_info(protocol_info_path, stem_fish_path_orig, 'Stim control file is empty.')
		
		return None
	
	#* Is the first stimulus fake? This happened at some point. There was sometimes a line in protocol file in excess.
	if protocol.loc[:,beg].iloc[0] == 0 and len(protocol.loc[protocol.index.get_level_values(experiment_type) == 'Cycle&Bout']) == expected_number_cs+1:
		save_info(protocol_info_path, stem_fish_path_orig, 'Lost beginning of first cycle.')
		
		return None
	
	protocol.rename(index={'Cycle&Bout': 'Cycle'}, inplace=True)
	protocol.sort_values(by=beg, inplace=True)

	# if reference_frame_time is not None:
		# Getting here means that there is absolute time in the cam file.
		# protocol = protocol - reference_frame_time
	protocol = protocol - reference_frame_time_or_id

	return protocol

def protocol_info(protocol):

	#* Count the number of cycles, trials, sessions and bouts.
	# Using len() just in case these is a single element.
	number_cycles = len(protocol.loc['Cycle', beg])

	if protocol.index.isin(['Session']).any():
		number_sessions = len(protocol.loc['Session', beg])
	else:
		number_sessions = number_cycles

	if protocol.index.isin([trial]).any():
		number_trials = len(protocol.loc[trial, beg])
	else:
		number_trials = number_cycles

	if protocol.index.isin([bout]).any():
		number_bouts = len(protocol.loc[bout, beg])	  
	else:
		number_bouts = 0
		
	if protocol.index.isin(['Reinforcer']).any():
 			number_reinforcers = len(protocol.loc['Reinforcer', beg])
	else:
		number_reinforcers = 0

	habituation_duration = protocol.iloc[0,0] / 1000 / 60 # min

	cs_beg = protocol.loc['Cycle', beg]
	cs_end = protocol.loc['Cycle', end]
	cs_dur = (cs_end - cs_beg).to_numpy() # in ms
	cs_isi = (cs_beg[1:] - cs_end[:-1]).to_numpy() / 1000 / 60 # min
	
	us_beg = protocol.loc['Reinforcer', beg]
	us_end = protocol.loc['Reinforcer', end]
	us_dur = (us_end - us_beg).to_numpy() # in ms
	us_isi = (us_beg[1:] - us_end[:-1]).to_numpy() / 1000 / 60 # min

	return number_cycles, number_reinforcers, number_trials, number_sessions, number_bouts, habituation_duration, cs_dur, cs_isi, us_dur, us_isi

def map_abs_time_to_elapsed_time(camera, protocol):
	
	for beg_end in [beg, end]:

		protocol_ = protocol.loc[:,beg_end].reset_index().rename(columns={beg_end : camera.columns[-1]})

		camera_protocol = pd.merge_ordered(camera, protocol_).set_index(abs_time).interpolate(kind='slinear').reset_index()

		protocol.loc['Cycle',beg_end] = camera_protocol[camera_protocol[experiment_type]=='Cycle'].set_index(experiment_type).loc[:,ela_time]
		protocol.loc['Reinforcer',beg_end] = camera_protocol[camera_protocol[experiment_type]=='Reinforcer'].set_index(experiment_type).loc[:,ela_time]
		#! protocol.loc[:,beg_end] = camera_protocol[camera_protocol[experiment_type].notna()].set_index(experiment_type).loc[:,ela_time]

def lost_stim(number_cycles, number_reinforcers, min_number_cs_trials, min_number_us_trials, protocol_info_path, stem_fish_path_orig, id_debug):

	if number_cycles < min_number_cs_trials:

		save_info(protocol_info_path, stem_fish_path_orig, 'Not all CS! Stopped at CS {} ({}).'.format(number_cycles, id_debug))

		return True

	elif number_reinforcers < min_number_us_trials:
		
		save_info(protocol_info_path, stem_fish_path_orig, 'Not all US! Stopped at US {} ({}).'.format(number_reinforcers, id_debug))

		return True
	else:
		return False

def plot_protocol(cs_dur, cs_isi, us_dur, us_isi, stem_fish_path_orig, fig_protocol_name):

	plt.figure(figsize=(14,14))
	plt.plot(np.arange(1, len(cs_isi) + 1), cs_isi, label='inter-cs interval\nmin int.=' + str(round(np.amin(cs_isi)*60,1)) + ' s\n' + 'cs min dur=' + str(round(np.amin(cs_dur)/1000,3)) + ' s\n' + 'cs max dur=' + str(round(np.amax(cs_dur)/1000,3)) + ' s')
	
	plt.plot(np.arange(5, 4+len(us_isi)+1), us_isi, label='inter-us interval\nmin int.=' + str(round(np.amin(us_isi)*60,1)) + ' s\n' + 'us min dur=' + str(round(np.amin(us_dur)/1000,3)) + 's\n' + 'us max dur='+ str(round(np.amax(us_dur)/1000,3)) + ' s')
	plt.xlabel('Trial number')
	plt.ylabel('ISI (min)')
	plt.ylim(0, 10)
	plt.legend(frameon=False, loc='upper center', ncol=2)
	plt.suptitle('Summary of protocol\n' + stem_fish_path_orig)
	plt.savefig(fig_protocol_name, dpi=100, bbox_inches='tight')
	plt.close()

def number_frames_discard(data_path, reference_frame_id):
	# Consider the experiment starts only whith the first frame whose ID is both in tail tracking and camera files.

	with open(data_path, 'r') as f:
		f.readline()
		tracking_frames_to_discard = 0
		while reference_frame_id != int(f.readline().split(' ')[0]):
			tracking_frames_to_discard += 1

	return tracking_frames_to_discard

#def readTailTracking(data_path, protocol_frame, tracking_frames_to_discard, time_bcf_window, time_max_window, time_min_window, time_bef_frame, time_aft_frame):
	#	# protocol in number of frames


	#	start = timer()
	#	extra_time_window = np.max([time_bcf_window, time_max_window, time_min_window])

	#	protocol_frame += tracking_frames_to_discard
	#	protocol_frame[beg] = protocol_frame[beg] - time_bef_frame - 2*extra_time_window
	#	protocol_frame[end] = protocol_frame[end] + time_aft_frame + 2*extra_time_window

	#	number_frames = protocol_frame[end].max()
	#	rows_to_skip = []
	#	number_rows = None
	#	for i in range(len(protocol_frame)):
	#		if i == 0:
	#			# 1 is required to avoid removing the names of the columns
	#			rows_to_skip.extend(np.arange(1, protocol_frame.iat[i,0]))
	#		else:
	#			if (b:= protocol_frame[beg].iat[i]) - (a:= protocol_frame[end].iloc[:i].max()) > 0:
	#				rows_to_skip.extend(np.arange(a, b))

	#	# frames = np.arange(reference_frame_id - tracking_frames_to_discard, last_frame)
	#	# # frames = pd.read_csv(data, sep=' ', usecols=[0], decimal=',', dtype='int64', engine='c', squeeze=True)
	#	# # frames -= reference_frame_id

	#	# mask_frames = np.zeros(len(frames), dtype=bool)

	#	# for i in range(len(protocol)):
	#	# 	mask_frames |= ((frames >= protocol.iat[i,0]) & (frames <= protocol.iat[i,1]))

	#	# rows_to_skip = np.arange(len(mask_frames))[~mask_frames]+1
	#	number_rows = number_frames - len(rows_to_skip)
		
	#	data = pd.read_csv(data_path, sep=' ', header=0, usecols=cols_to_use_orig, nrows=number_rows, skiprows=rows_to_skip, decimal=',')
		
	#	print(timer()-start)
		
	#	return data

def read_tail_tracking_data(data_path, reference_frame_id):
# number_frames
	# Angles in data come in radians.

	try:
		start = timer()
		# na_filter=False to speed up.
		# skipfooter=1 because in one file there were NaN's in, and only in, the last line.
		data = pd.read_csv(data_path, sep=' ', header=0, usecols=cols_to_use_orig, decimal=',', na_filter=False) #, skipfooter=1)
		# nrows=number_frames

		#! To correct a corrupted file (20220503_Tu_6dpf_delay_test_3-black_fish8).
		# data = pd.read_csv(data_path, sep=' ', header=0, usecols=cols_to_use_orig, decimal='.', na_filter=False)
		# data = data.iloc[:-1,:]
		# for col in data.columns[1:]:
		# 	data.loc[:,col] = data.loc[:,col].str.replace(',','.')
		# data.iloc[:,1:] = data.iloc[:,1:].astype('float32')

		print('Time to read tail tracking .txt: {} (s)'.format(timer()-start))

		# data.iloc[:,0] = data.iloc[:,0].astype('int')
		data.iloc[:,0] = data.iloc[:,0] - reference_frame_id

		data = data[data.iloc[:,0] >= 0]

		#? maybe before this was necessary because "decimal" in pd.read_csv was set to ",".
		# data.iloc[:,1:] = data.iloc[:,1:].astype('float32')

		# Convert tail tracking data from radian to degree
		data.iloc[:,1:] = data.iloc[:,1:] * (180/np.pi)
		
		# Rename columns
		data.rename(columns=dict(zip(cols_to_use_orig[1:], cols[1:])), inplace=True)

		return data

	except:
		return None



def tracking_errors(data, single_point_tracking_error_thr):

	errors = False

	if ((a := data.iloc[:,1:].abs().max()) > single_point_tracking_error_thr).any():
		print('Possible tracking error! Max(abs(angle of individual point)):')
		print(a)

		errors = True

	if data.iloc[:,1:].isna().to_numpy().any():
		print('Possible tracking failures. There are NAs in data!')

		errors = True

	return errors

def interpolate_data(data, expected_framerate, predicted_framerate):
	# expected_framerate is the framerate to which data is interpolated. So, output data is as if it had been acquired at the expected_framerate (700 FPS when I wrote this).


	data.iloc[:,0] = data.iloc[:,0] * expected_framerate/predicted_framerate
	
	interp_function = interpolate.interp1d(data.iloc[:,0], data.iloc[:,1:], kind='slinear', axis=0, assume_sorted=True)

	data_ = pd.DataFrame(np.arange(data.iat[0,0], data.iat[-1,0]), columns=['Time (frame) [{} FPS]'.format(expected_framerate)], dtype='int')
	data_[data.columns[1:]] = interp_function(data_.iloc[:,0])

	# Old
		# # data_['Original'] = True

		# # # data.iloc[:,0] = (data.iloc[:,0] * 1000/predicted_framerate)  # ms

		# # Create a dataframe with what is going to be the index for interpolation.
		# data_ = pd.DataFrame(np.nan((len(timepoints_at_expected_framerate), data.shape[1])), columns=data.columns.to_numpy(), dtype='float32')
		# data_.iloc[:,0] = timepoints_at_expected_framerate
		# data_['Original'] = False

		# print('!!!!!!!!!!', data)
		# pd.concat([data.set_index(data.columns[0]), data_.set_index(data.columns[0])], axis=0, join='outer', copy=False).sort_index().reset_index().drop_duplicates(subset=[data.columns[0]], inplace=True)
		# print('????????????', data)


		# data.interpolate(method='slinear', axis=0, inplace=True, copy=False, assume_sorted=True)


		# data.iloc[:,0] = np.arange(0, len(data))

		#s Rename column with time.
		# # data.rename(columns={data.columns[0]: 'Time (frame) [{} FPS]'.format(expected_framerate)}, inplace=True)

	return data_

def rolling_window(a, window):

	#* Alexandre Laborde confirmed this.
	shape = a.shape[:-1] + (a.shape[-1] - window + 1, window)
	strides = a.strides + (a.strides[-1],)
	return np.lib.stride_tricks.as_strided(a, shape=shape, strides=strides)

def filter_data(data, space_bcf_window, time_bcf_window):

	#* Calculate the cumulative sum in space
	# This works as a filter in space
	data.iloc[:, 1:] = data.iloc[:, 1:].cumsum(axis=1)

	#* Filter with a rolling average in space
	#* Alexandre Laborde confirmed this.
	# Not using pandas rolling mean beacause over columns it takes a lot of time (confirmed that with this way the result is the same)
	# The fact that here we are using the cumsum means that when averaging more importance is given to the first points
	data.iloc[:, 2:-1] = np.mean(rolling_window(data.iloc[:, 1:].to_numpy(), space_bcf_window), axis=2)
	data.iloc[:, 1] = data.iloc[:, 1:3].mean(axis=1)
	data.iloc[:, -1] = data.iloc[:, -2:].mean(axis=1)

	# This does not work with data in float32. Might be a bug of Pandas.
	# Too slow. Use alternative above.
	# data.iloc[:, 1:] = data.astype('float').iloc[:, 1:].rolling(window=space_bcf_window, center=True, axis=1).mean()

	#* Filter with a rolling average in time
	data.iloc[:, 1:] = data.iloc[:, 1:].rolling(window=time_bcf_window, center=True, axis=0).mean()
	data = data.dropna()
	
	
	# Filter eye tracking data
	# if eyeData:	
	# 	data.loc[:, right_eye_angle] = data.loc[:, right_eye_angle].rolling(window=time_bcf_window, center=center_window).mean().astype('float32')
	# 	data.loc[:, left_eye_angle] = data.loc[:, left_eye_angle].rolling(window=time_bcf_window, center=center_window).mean().astype('float32')

	return data

def activity_for_bout_detection(data, chosen_tail_point, time_min_window, time_max_window):
	# Calculate 'activity_bout_detection' (deg/ms)

	# Calculate the derivative over time (angular velocity)
	# Converting from deg/frame to deg/ms
	# ang_vel = data.iloc[:,1:2+chosen_tail_point].diff() * (expected_framerate / 1000)
	# ang_vel.dropna(inplace=True)

	#* Calculate the cumulative sum of the angular velocity over space.
	#* This allows to take into account movement in any segment with a single scalar value.
	data.loc[:, activity_bout_detection] = data.iloc[:,1:2+chosen_tail_point].diff().abs().sum(axis=1) * (expected_framerate / 1000) # deg/ms
	# data.loc[:, activity_bout_detection] = ang_vel.loc[:, cols[1:1+chosen_tail_point]].abs().sum(axis=1)
	
	#* Calculate the abstract measure defined by me as 'activity_bout_detection'.
	data.loc[:, activity_bout_detection] = data.loc[:, activity_bout_detection].rolling(window=time_max_window, center=True, axis=0).max() - data.loc[:, activity_bout_detection].rolling(window=time_min_window, center=True, axis=0).min()
	
	data.dropna(inplace=True)

	return data

def stim_in_data(data, protocol, number_cycles, number_reinforcers, exp_type):

	#region Merge protocol with data
	data[cols_stim[:4]] = 0


	# TODO case of Operant Conditioning.
	#if exp_type == 'OC':

		#data['Trial beg'] = 0
		#data['Trial end'] = 0
		#data['Reinforcer beg'] = 0
		#data['Reinforcer end'] = 0
	
		#if protocol.index.isin(['Session']).any():
		
		#	# For operant conditioning experiments
		#	data['Session beg'] = 0
		#	data['Session end'] = 0

		#	for i in range(number_sessions):
		#		data.loc[data.iloc[:,0] == protocol.loc['Session', beg].iat[i], 'Session beg'] = int(i + 1)
		#		data.loc[data.iloc[:,0] == protocol.loc['Session', end].iat[i], 'Session end'] = int(i + 1)
	
		#if protocol.index.isin(['Trial']).any():
		
		#	# For operant conditioning experiments	
		#	for i in range(number_trials):
		#		data.loc[data.iloc[:,0] == protocol.loc[trial, beg].iat[i], 'Trial beg'] = int(i + 1)
		#		data.loc[data.iloc[:,0] == protocol.loc[trial, end].iat[i], 'Trial end'] = int(i + 1)

		#	data['Cycle beg'] = 0
		#	data['Cycle end'] = 0

		#	for i in range(number_cycles):
			
		#		data.loc[data.iloc[:,0] == protocol.loc['Cycle', beg].iat[i], 'Cycle beg'] = int(i + 1)
		#		data.loc[data.iloc[:,0] == protocol.loc['Cycle', end].iat[i], 'Cycle end'] = int(i + 1)
		
		#else:
		#	for i in range(number_cycles):
		#		data.loc[data.iloc[:,0] == protocol.loc['Cycle', beg].iat[i], 'Trial beg'] = int(i + 1)
		#		data.loc[data.iloc[:,0] == protocol.loc['Cycle', end].iat[i], 'Trial end'] = int(i + 1)
		
		#if protocol.index.isin(['Reinforcer']).any():
		#	for i in range(number_reinforcers):
		#		data.loc[data.iloc[:,0] == protocol.loc['Reinforcer', beg].iat[i], 'Reinforcer beg'] = int(i + 1)
		#		data.loc[data.iloc[:,0] == protocol.loc['Reinforcer', end].iat[i], 'Reinforcer end'] = int(i + 1)
	
	# 	data['Session beg', 'Trial beg', 'Cycle beg', 'Reinforcer beg', 'Session end', 'Trial end', 'Cycle end', 'Reinforcer end'] = data['Session', trial, 'Cycle', 'Reinforcer'].astype('category')

	#if exp_type != 'OC':	

	for i, [cs_b, cs_e] in enumerate(protocol.loc['Cycle', [beg, end]].to_numpy()):
		data.loc[data.iloc[:,0] == cs_b, cs_beg] = int(i + 1)
		data.loc[data.iloc[:,0] == cs_e, cs_end] = int(i + 1)
		
	if protocol.index.isin(['Reinforcer']).any():

		for i, [us_b, us_e] in enumerate(protocol.loc['Reinforcer', [beg, end]].to_numpy()):
			data.loc[data.iloc[:,0] == us_b, us_beg] = int(i + 1)
			data.loc[data.iloc[:,0] == us_e, us_end] = int(i + 1)

	# data.loc[:,cols_stim[:4]] = data.loc[:,cols_stim[:4]].astype('category')
	data.loc[:, cs_beg] = data.loc[:, cs_beg].astype(CategoricalDtype(categories=data[cs_beg].unique().sort(), ordered=True))		
	data.loc[:, us_beg] = data.loc[:, us_beg].astype(CategoricalDtype(categories=data[us_beg].unique().sort(), ordered=True))
	data.loc[:, cs_end] = data.loc[:, cs_end].astype(CategoricalDtype(categories=data[cs_end].unique().sort(), ordered=True))
	data.loc[:, us_end] = data.loc[:, us_end].astype(CategoricalDtype(categories=data[us_end].unique().sort(), ordered=True))

	return data

def plot_behavior_overview(data, stem_fish_path_orig, fig_behavior_name):
	# data containing tail_angle.

	# mask_frames = np.ones(number_frames + round(60*framerate), dtype=bool)
	# mask_frames[:: round(framerate * 0.5)] = False
	# mask_frames[0] = False
	
	# rows_to_skip = np.arange(number_frames + round(60*framerate))
	# rows_to_skip = rows_to_skip[mask_frames]

	# start = timer()

	# overall_data = pd.read_csv(data, sep=' ', header=0, usecols=cols, skiprows=rows_to_skip, decimal=',')
	# overall_data = overall_data.astype('float32')

	# print(timer() - start)
	plt.figure(figsize=(28, 14))
	plt.plot(data.iloc[:,0]/expected_framerate/60/60, data.iloc[:,1	+chosen_tail_point], 'black')
	plt.xlabel('Time (h)')
	plt.ylabel('Tail end angle (deg)')
	plt.suptitle('Behavior overview\n' + stem_fish_path_orig)
	# plt.show()
	# plt.legend(frameon=False, loc='upper center', ncol=2)
	plt.savefig(fig_behavior_name, dpi=100, bbox_inches='tight')
	plt.close()

def extract_data_around_stimuli(data, protocol_frame, time_bef_frame, time_aft_frame, time_bcf_window, time_max_window, time_min_window):

	# protocol_frame is the protocol in number of frames.
	# protocol_frame sorted by beg and in number of frames.
	# Only save data around the stimuli.


	extra_time_window = np.max([time_bcf_window, time_max_window, time_min_window])

	protocol_frame[beg] = protocol_frame[beg] - time_bef_frame - 2*extra_time_window
	protocol_frame[end] = protocol_frame[end] + time_aft_frame + 2*extra_time_window

	# rows_to_skip contains the line numbers with data belonging to frames between trials and not within each trial time span (-time_bef to time_aft referenced to stim).
	rows_to_skip = np.arange(protocol_frame.iat[0,0]).tolist()
	# For each stimulus, check if the beginning of the trial of stimulus 'i' happens after or before of the end of the previous trials. Remember that protocol contains the stimuli order by their beginning.
	for i in range(1, len(protocol_frame)):
		if (b:= protocol_frame[beg].iat[i]) - (a:= protocol_frame[end].iloc[:i].max()) > 0:
			rows_to_skip.extend(np.arange(a, b).tolist())

	# errors='ignore' to ignore if rows_to_skip includes line numbers of lines that are already not present in data, instead of showing an error.
	data.drop(index=rows_to_skip, errors='ignore', inplace=True)

	return data

def find_beg_and_end_of_bouts(data, bout_detection_thr_1, min_bout_duration, min_interbout_time, bout_detection_thr_2):

	#* Use the derivative to find the beginning and end of bouts.
	def bouts_beg_and_end(bouts):
		bouts_beg = np.where(np.diff(bouts) > 0)[0] + 1
		bouts_end = np.where(np.diff(bouts) < 0)[0]
		return bouts_beg, bouts_end


	#* For each timepoint, bouts indicates whether it belongs to a bout or not.
	# It cannot be initialized to an array of nan because of the derivative calculated below.
	bouts = np.zeros(len(data.loc[:, activity_bout_detection]))


	# bouts[0] and bouts[-1] = 0 to account for cases when the period under analysis starts in the middle of a bout or finishes in the middle of a bout.
	bouts[1:-1][data[activity_bout_detection].iloc[1:-1] >= bout_detection_thr_1] = 1


	bouts_beg, bouts_end = bouts_beg_and_end(bouts)


	# In principle, the line where we used bouts[1:-1] does not allow to enter in the else part.
	if len(bouts_beg) == len(bouts_end):
		bouts_interval = bouts_beg[1:] - bouts_end[:-1]
	else:
		print('bouts_beg and end have diff len')


	#* Join bouts close in time after finding the interbout intervals too short.
	for short_interval_bout in reversed(np.where(bouts_interval < min_interbout_time)[0]):
		# if short_interval_bout < len(bouts) - 1:
		bouts[bouts_end[short_interval_bout] + 1 : bouts_beg[short_interval_bout + 1]] = 1


	bouts_beg, bouts_end = bouts_beg_and_end(bouts)


	#* Find bouts too short and remove them.
	for short_bouts in np.where(bouts_end - bouts_beg < min_bout_duration)[0]:

		bouts[bouts_beg[short_bouts] : bouts_end[short_bouts] + 1] = 0


	bouts_beg, bouts_end = bouts_beg_and_end(bouts)


	#* Filter by maximum tail angle of each tail movement.
	# bouts_max = np.zeros_like(bouts_beg)

	for bout_b, bout_e in zip(bouts_beg, bouts_end):
	
		# Angular velocity is converted to deg/ms.
		if data.iloc[bout_b : bout_e + 1, data.columns.get_loc(tail_angle)].diff().abs().max() * (expected_framerate / 1000) < bout_detection_thr_2:
			bouts[bout_b : bout_e + 1] = 0


	# Previous version
			# for bout in range(len(bouts_beg)):
			
			# 	# Find the maximum of each bout.		
			# 	bouts_max[bout] = data.iloc[bouts_beg[bout] : bouts_end[bout] + 1, data.columns.get_loc(tail_angle)].diff().abs().max()

			# too_weak_bouts = np.where(bouts_max < bout_detection_thr_2)[0]
			
			# for weak_bouts in too_weak_bouts:

			# 	bouts[bouts_beg[weak_bouts] : bouts_end[weak_bouts] + 1] = 0

	data[bout] = bouts
	data[bout_beg] = data[bout].diff() > 0
	data[bout_end] = data[bout].diff() < 0

	data.loc[:, cols_bout] = data[cols_bout].astype(pd.SparseDtype('bool'))

	# # Create a column in data with the beginning and end of bouts.
	# bouts_beg, bouts_end = bouts_beg_and_end(bouts)
	
	# data.iloc[bouts_beg, data.columns.get_loc(bout_beg)] = True
	# data.iloc[bouts_end, data.columns.get_loc(bout_end)] = True
	
	# bouts_beg = data.iloc[bouts_beg,0].to_numpy()
	# bouts_end = data.iloc[:,0].iloc[bouts_end].to_numpy()
	# bouts_beg = data.iloc[:,0].iloc[np.where(data['Bout beg'])[0]].to_numpy()
	# bouts_end = data.iloc[:,0].iloc[np.where(data['Bout end'])[0]].to_numpy()

	return data
	# , bouts_beg, bouts_end

def plot_cropped_experiment(data, expected_framerate, bout_detection_thr_1, bout_detection_thr_2, downsampling_step, stem_fish_path_orig, fig_cropped_exp_with_bout_detection_name):

	data = data.copy()

	# Convert time to s.
	data.iloc[:,0] = data.iloc[:,0] / expected_framerate


	# Stimuli beg and end need to be read from data as there were a few changes to data after applying stim_in_data function.
	cs_beg_array = data.loc[data[cs_beg] != 0, data.columns[0]].to_numpy()
	cs_end_array = data.loc[data[cs_end] != 0, data.columns[0]].to_numpy()

	us_beg_array = data.loc[data[us_beg] != 0, data.columns[0]].to_numpy()
	us_end_array = data.loc[data[us_end] != 0, data.columns[0]].to_numpy()

	if len(cs_end_array) < len(cs_beg_array):
		cs_end_array.extend([data.iat[-1,0]])

	if len(us_end_array) < len(us_beg_array):
		us_end_array.extend([data.iat[-1,0]])

	bouts_beg_array = data.loc[data[bout_beg], data.columns[0]].to_numpy()
	bouts_end_array = data.loc[data[bout_end], data.columns[0]].to_numpy()

	trial_transition = np.where(np.diff(data.index) > 1)[0]
	# np.where(data.iloc[:,0].diff() > 1)[0]
	trial_transition = data.iloc[trial_transition-1,0].to_numpy() # / expected_framerate

	# data_plot = deepcopy(data.iloc[::downsampling_step, :])
	data = data.iloc[::downsampling_step, :]


	x = data.iloc[:,0] #/ expected_framerate

	fig = make_subplots(specs=[[{'secondary_y': True}]])

	fig.add_scatter(x=x, y=data.loc[:, tail_angle], name='bcf-time bcf-space cumsum angle [point {}]'.format(chosen_tail_point), mode='lines', line_color= 'black', opacity=0.7, secondary_y=True,)

	fig.add_scatter(x=x, y=data.loc[:, tail_angle].diff().abs() * expected_framerate/1000, name='Abs velocity [point {}]'.format(chosen_tail_point), mode='lines', line_color='red', opacity=0.7, visible='legendonly')

	fig.add_scatter(x=x, y=data.loc[:, activity_bout_detection], name='Activity', mode='lines', line_color='blue', opacity=0.7, visible='legendonly', legendgroup='Activiy')


	# Make shapes for the plots
	shapes = [go.layout.Shape(type='line', xref='x', x0=trial, x1=trial, yref='paper', y0=0, y1=1, opacity=0.7, line_width=1, fillcolor='gray') for trial in trial_transition]


	# if not data[data['Bout beg']].empty:
	# for bout in range(len(bouts_beg)):
	for bout_b, bout_e in zip(bouts_beg_array, bouts_end_array):

		shapes.append(go.layout.Shape(type='rect', xref='x', x0=bout_b, x1=bout_e, yref='paper', y0=0, y1=1, opacity=0.3, line_width=0, fillcolor='lightgray'))
		# fig.add_vrect(x0=bouts_beg[bout], x1=bouts_end[bout], opacity=0.3, line_width=0, fillcolor='lightgray')

	shapes.append(go.layout.Shape(type='line', xref='paper', x0=0, x1=1, yref='y', y0=bout_detection_thr_1, y1=bout_detection_thr_1,
	opacity=0.5, line_width=1, fillcolor='gray', line_dash='dash'))

	shapes.append(go.layout.Shape(type='line', xref='paper', x0=0, x1=1, yref='y', y0=bout_detection_thr_2, y1=bout_detection_thr_2,
	opacity=0.5, line_width=1, fillcolor='gray', line_dash='dot'))

	# fig.add_hline(y=bout_detection_thr_1, opacity=0.5, line_width=1, fillcolor='gray', line_dash='dash')
	# fig.add_hline(y=bout_detection_thr_2, opacity=0.5, line_width=1, fillcolor='gray', line_dash='dot')

	for cs_b, cs_e in zip(cs_beg_array, cs_end_array):

		shapes.append(go.layout.Shape(type='rect', xref='x', x0=cs_b, x1=cs_e, yref='paper', y0=0, y1=1, opacity=0.4, line_width=0, fillcolor='green'))
		# fig.add_vrect(x0=cs_beg_, x1=cs_end_, opacity=0.4, line_width=0, fillcolor='green')

	# for stim in range(len(cs_beg_array)):

		# cs_beg_ = cs_beg_array[stim]
		# # cs_end_array = data.loc[data['cs'] == t, data.columns[0]].iloc[-1]
		# cs_end_ = cs_end_array[stim]

		# shapes.append(go.layout.Shape(type='rect', xref='x', x0=cs_beg_, x1=cs_end_, yref='paper', y0=0, y1=1, opacity=0.4, line_width=0, fillcolor='green'))
		# # fig.add_vrect(x0=cs_beg_, x1=cs_end_, opacity=0.4, line_width=0, fillcolor='green')

	for us_b, us_e in zip(us_beg_array, us_end_array):

		shapes.append(go.layout.Shape(type='rect', xref='x', x0=us_b, x1=us_e, yref='paper', y0=0, y1=1, opacity=0.4, line_width=0, fillcolor='red'))
		# fig.add_vrect(x0=us_beg_, x1=us_end_, opacity=0.4, line_width=0, fillcolor='red')	

	# for stim in range(len(us_beg_array)):

		# us_beg_ = us_beg_array[stim]
		# # us_end_array = data.loc[data['us'] == t, data.columns[0]].iloc[-1]
		# us_end_ = us_end_array[stim]

		# shapes.append(go.layout.Shape(type='rect', xref='x', x0=us_beg_, x1=us_end_, yref='paper', y0=0, y1=1, opacity=0.4, line_width=0, fillcolor='red'))
		# # fig.add_vrect(x0=us_beg_, x1=us_end_, opacity=0.4, line_width=0, fillcolor='red')

	fig.update_layout(height=1000, width=2000, showlegend=True, plot_bgcolor='rgba(0,0,0,0)', title_text='Behavior before cleaning data, downsampled 5X        ' + stem_fish_path_orig, legend=dict(yanchor='top',y=1,xanchor='left',x=0, bgcolor='white'), shapes=shapes)

	# paper_bgcolor='rgba(0,0,0,0)',

	fig.update_xaxes(title='t (s)', showgrid=False, automargin=True,)

	fig.update_yaxes(title='Velocity or activity (deg/ms)', showgrid=False, zeroline=False, zerolinecolor='black', automargin=False, range=[0, 20], secondary_y=False,)	

	fig.update_yaxes(title='Angle (deg)', showgrid=False, zeroline=False, zerolinecolor='black', automargin=False, range=[-200, 200], secondary_y=True,)

	py.io.write_html(fig=fig, file=fig_cropped_exp_with_bout_detection_name, auto_open=False)

def clean_data(data):

	# We should not set the angles to 0 deg because of subsequent steps.
	data.loc[~data[bout], data.columns[1:2+chosen_tail_point]] = np.nan

	# Previous version

			# mask_with_lines_to_keep_bout = np.array([False] * len(data))

			# bouts_beg = data.iloc[:,0].iloc[np.where(data['Bout beg'])[0]].to_numpy()
			# bouts_end = data.iloc[:,0].iloc[np.where(data['Bout end'])[0]].to_numpy()


			# if not data[data['Bout beg']].empty:
			# 	for bout in range(len(bouts_beg)):
					
			# 		mask_with_lines_to_keep_bout += ((data.iloc[:,0] >= bouts_beg[bout]) & (data.iloc[:,0] <= bouts_end[bout])).to_numpy()
	

			# # We should not set the angles to 0 deg...
			# data.loc[~mask_with_lines_to_keep_bout, cols[1:]] = np.nan

	data.drop(columns=activity_bout_detection, inplace=True)

	# data[cols[1:]] = data[cols[1:]].astype(pd.SparseDtype('float32', np.nan))
	# # Need to do this again as the previous operation seems to change the dtype to int32.
	# data[cols_stim] = data[cols_stim].astype(pd.SparseDtype('int8', 0))

	return data

# def calculate_tail_activity(data, cols, chosen_tail_point, expected_framerate):
	
	# data[activity] = data[cols[1:1+chosen_tail_point]].diff().abs().sum(axis=1) * (expected_framerate / 1000) # deg/ms
	# # data[activity] = data[activity].astype(pd.SparseDtype('float32', 0))
	
	# # Discard the columns with the angle data used to calculate the activity.
	# # data.drop(cols[1:], axis=1, inplace=True)

	# # data[bout] = data[activity] > 0
	# # data.loc[data[activity] > 0, bout] = True

	# return data

def identify_trials(data, expected_framerate, time_bef_frame, time_aft_frame):

	trials_list = []

	for cs_us in ['CS', 'US']:

		cs_us_beg = cs_us + ' beg'

		trials_csus = data.loc[data[cs_us_beg] != 0, cs_us_beg].unique()
		# trials_csus = data.loc[data[cs_us_beg] > 0, cs_us_beg].unique()

		for t in trials_csus:

			# trial_beg = trial_reference - time_bef_frame # time_bef_ms / 1000
			# # trial_end in relation to cs_us_beg also because stimuli duration may slightly differ from number_trial_csus to number_trial_csus.
			# trial_end = trial_reference + time_aft_frame # time_aft_ms / 1000 
			trial_reference = data.loc[data[cs_us_beg] == t, data.columns[0]].to_numpy()[0]
			
			trial = data.loc[(data.iloc[:,0] >= trial_reference - time_bef_frame) & (data.iloc[:,0] <= trial_reference + time_aft_frame), :]
			
			# trial[time_trial_csus] is not given by np.arange(-time_bef_frame, time_aft_frame + 1) because there may be "incomplete' trials at the end (stopped before trial_reference + time_aft_frame).
			trial[[type_trial_csus, number_trial_csus, time_trial_csus]] = cs_us, str(t), np.arange(-time_bef_frame, len(trial) - time_bef_frame)
			# 1000/expected_framerate

			trials_list.append(trial)
		
	data = pd.concat(trials_list)

	# data[activity] = data[activity].astype(pd.SparseDtype('float32', 0))
	# data.loc[ : , number_trial_csus] = data.loc[ : , number_trial_csus].astype('category')
	# data.loc[ : , type_trial_csus] = data.loc[ : , type_trial_csus].astype('category')
	# data.loc[ : , time_trial_csus] = data.loc[ : , time_trial_csus].astype('float32')

	data.drop(data.columns[0], axis=1, inplace=True)

		# To discard automatically fish.
			#zero_bouts_trials = 0
			
			# trial = data.loc[data[number_trial_csus] == t, :]

			# Check that fish beats the tail before the us at least every few trials.
			# if csus == 'us':
			# 	if trial.loc[(trial[time_trial_csus] > -numb_seconds_before_us*expected_framerate) & (trial[time_trial_csus] < numb_seconds_after_us*expected_framerate) & (trial[activity] > 0),:].empty:
			# 		zero_bouts_trials += 1
			# 		if zero_bouts_trials == max_numb_trials_no_bout_bef:
			# 			print('!!! Quiet fish before and after us !!!  trial: ', t)
			# 			lines.append(stem_fish_path + '\n\t' ' Quiet fish before and after cs ({} consecutive trials). last trial: {}\n'.format(max_numb_trials_no_bout_bef, t))

			# 			skip = True
			# 			break
			# 	else:
			# 		if zero_bouts_trials > 0:
			# 			zero_bouts_trials = 0

			# Check that fish always beats the tail after the us.
			# else:
			# 	if trial.loc[(trial[time_trial_csus] > 0) & (trial[time_trial_csus] < numb_seconds_after_us*expected_framerate) & (trial[activity] > 0),:].empty:
			# 		print('!!! Fish inactive after us !!!  trial: ', t)
			# 		lines.append(stem_fish_path + '\n\t' ' Fish inactive after us. trial: {}\n'.format(t))

			# 		skip = True
			# 		break


	return data