


# CALCULATE ALL THE KINEMATIC PARAMeTERS FOR THE WHOLE EXPERIMENT

# Determine a mininum and a maximum latency of response after the US onset
min_latency = 20
max_latency = 5000

bout_array_diff = bout_array.diff()

data['Bout duration (ms)'] = np.zeros(len(data))*np.nan
data['Bout latency (ms)'] = np.zeros(len(data))*np.nan
data['Optovin bout'] = np.zeros(len(data))*np.nan
data['Bout number'] = np.zeros(len(data))*np.nan      #! Does np.nan work with +=1?????
data['Bout displacement'] = np.zeros(len(data))*np.nan
data['Bout reorientation'] = np.zeros(len(data))*np.nan
data['Bout bend peaks'] = np.zeros(len(data))*np.nan
data['Bout number of bend peaks'] = np.zeros(len(data))*np.nan
data['Complex bout'] = np.zeros(len(data))*np.nan
data['Bout amplitudes (°)'] = np.zeros(len(data))*np.nan

bout_duration = data['Bout duration (ms)']
bout_latency = data['Bout latency (ms)']
optovin_bout = data['Optovin bout']
bout_number_resp = data['Bout number']
bout_displacement = data['Bout displacement']
bout_reorientation = data['Bout reorientation']
bout_bend_peaks = data['Bout bend peaks']
bout_number_peaks = data['Bout number of bend peaks']
complex_bout = data['Complex bout']
bout_amplitudes = data['Bout amplitudes (°)']


# Bout duration, latency and so on saved at the level of the firts frame of each bout

for i in range(len(data)):
    
    # US onset
    if data.index.get_level_values('US')[i] != np.nan and data.index.get_level_values('US')[i-1] == np.nan:
        US_onset = i
        bout_number_resp[i] = 0
        
    # Beginning of a bout
    if bout_array_diff[i] > 0:
        beg_bout = i
        
        # Determine if it is possibly a bout elicited by optovin or not; Count the number of bouts initiated during the action of optovin
        if i > US_onset + min_latency and i < US_onset + max_latency:
            optovin_bout[beg_bout] = True
            bout_number_resp[beg_bout] += 1
        else:
            optovin_bout[beg_bout] = False
        
        # Latency of the bout from the US onset
        if data.index.get_level_values('Trial')[i]!=np.nan:
            trial = data.index.get_level_values('Trial')[i]
            bout_latency[beg_bout] = beg_bout-data[data.index.get_level_values('Trial')[i]==trial].index[0][0]  # CHECK THIS!!!!!!
            
    elif bout_array_diff[i] == 0:           # does this not consider the np.nan?!
        
        # Peak bendings
        if data.loc[i, point].diff() == 0 and data.loc[i, point].diff().diff() != 0:       #how to calculate the n-th derivative?!
            
            if current_bout_bend_peak != np.nan:    # Means that this is done only for peaks after the first one
                previous_bout_bend_peak = current_bout_bend_peak
                current_bout_bend_peak = data.loc[i, point]
                
                # Determine if it is a complex bout         MAY WANT TO MAKE THIS MORE SOPHISTICATED
                if current_bout_bend_peak * previous_bout_bend_peak > 0 and ~complex_bout[beg_bout]:       # ??? What is the behavior of this with np.nan?!?!?!
                    complex_bout[beg_bout] = True
                
                # Bout amplitudes
                elif current_bout_bend_peak * previous_bout_bend_peak < 0:
                    bout_amplitudes[i] = current_bout_bend_peak - previous_bout_bend_peak 
            
            else:
                current_bout_bend_peak = data.loc[i, point]
            
            bout_bend_peaks[i] = current_bout_bend_peak

    # End of a bout
    elif bout_array_diff[i] < 0:
        end_bout = i
        optovin_bout = False        # CHECK IF REQUIRED
        current_bout_bend_peak = np.nan
        
        # Duration of the bout
        bout_duration[beg_bout] = (end_bout-beg_bout) * 0.7
        
        # Bout integral
        bout_displacement[beg_bout] = abs(data.loc[beg_bout : end_bout, point]).integral()  # CHECK THIS!!!!!!
        
        # Bout reorientation
        bout_reorientation[beg_bout] = data.loc[beg_bout : end_bout, point].integral()  # CHECK THIS!!!!!!
        
        if complex_bout[beg_bout] == np.nan:
            complex_bout = False
        
        
        
        ## DONE ABOVE TO AVOID ANOTHER FOR-LOOP
        # # Bout amplitudes
        # singe_bout_bend_peaks = bout_bend_peaks.loc[beg_bout:end_bout, [bout_bend_peaks != np.nan]]
        
        # bout_number_peaks[beg_bout] = len(singe_bout_bend_peaks)
        # for j in range(1, bout_number_peaks + 1):  # CHECK THIS!!!!!!
            
        #     # Determine if it is a complex bout         MAY WANT TO MAKE THIS MORE SOPHISTICATED
        #     if bout_bend_peaks[i] * bout_bend_peaks[i-1] > 0 and ~complex_bout[beg_bout]:       # ??? What is the behavior of this with np.nan?!?!?!
        #         complex_bout[beg_bout] = True
        #     elif bout_bend_peaks[i] * bout_bend_peaks[i-1] < 0:
        #         bout_amplitudes[i] = bout_bend_peaks[i] - bout_bend_peaks[i-1]
            
                
        #     if j == bout_number_peaks + 1:
        #         complex_bout[beg_bout] = False
            
            
        
        
        
        
        



# PRESENT THE PARAMETERS ONLY FOR THE TRIALS