from tqdm import tqdm
from pathlib import Path, PurePath
import shutil
from timeit import default_timer as timer


#region Paths of the folders with data
# Path where data is
path = Path(r'E:\\GOOD\\')

new_path = Path(r'F:\Joaquim\\')

for p in tqdm(Path(path).rglob('*dataSkewCorrected.mat')):
  
    folder_name = '_'.join(p.parent.stem.split('_')[:-1])

    new_stem = p.parent.stem + '_' + p.stem + p.suffix

    new_p = new_path / folder_name / new_stem

    (new_path/folder_name).mkdir(parents=True, exist_ok=True)

    if not new_p.exists():
        shutil.copyfile(str(p), str(new_p))