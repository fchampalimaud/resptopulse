from typing import Final
from pathlib import Path
import numpy as np
from my_general_variables import *



# Conditions.
#TODO move this to general variables.
#!move this to condition_dict
#! conditions = ['conditioning_unpaired', 'trace1500conditioning_paired', 'trace2500conditioning_paired']
#! see script 5 exp_types = ['control', 'trace1500CC', 'trace2500CC']


# def get_pool_types():
# 	return [k for k in sessions_dict.keys()][1:]


def name_sessions(csus, sessions_csus):

	if csus == 'CS':
		
		if any([len(x) != 1 for x in sessions_csus]):

			names_sessions_csus = ['Pre-conditioning\n(CS−)  ' + (str(sessions_csus[0][0]) + ' to ' + str(sessions_csus[0][-1]))]
			for j in range(1, len(sessions_csus)-1):
				names_sessions_csus.append('Conditioning {}\n(CS-US)  {}'.format(j, str(sessions_csus[j][0]) + ' to ' + str(sessions_csus[j][-1])))
			names_sessions_csus.append('Post-conditioning\n(CS−)  ' + (str(sessions_csus[-1][0]) + ' to ' + str(sessions_csus[-1][-1])))
		
		else:
			names_sessions_csus = [str(x[0]) for x in sessions_csus]

	else:

		if any([len(x) != 1 for x in sessions_csus]):

			names_sessions_csus = ['Conditioning {}\n(CS-US)  {}'.format(j + 1, str(sessions_csus[j][0]) + ' to ' + str(sessions_csus[j][-1])) for j in range(len(sessions_csus))]

		else:
			names_sessions_csus = [str(x[0]) for x in sessions_csus]

	return names_sessions_csus


#* Path where the raw data is.
# path_home = Path(r'D:\optovin\3 Classical conditioning\8, 9 trace CC 1.5 s and 2.5 s 90 trials\Raw data_all with absolute time')
# path_home = Path(r'E:\Results_behavior\trace CC 1.5 s and 2.5 s 90 trials')
# path_home = Path(r'E:\Results_behavior\trace CC 1.5 s 30 trials')
# path_home = Path(r'E:\Results_behavior\delay CC -1 s and trace 0.5 s 30 trials')
# path_home = Path(r'D:\optovin\3 Classical conditioning\5 delay CC and trace 0.5 s 30 trials\Raw data')
# path_home = Path(r'F:\Raw data_behavior\tac1')


path_home = Path(r'C:\Experiments_to copy')
# path_home = Path(r'E:\Results_behavior\6. delay CC -1 s, trace 2.5 s 50 trials, 2 controls')


# Path backing up raw data.
# path_backup = Path(r'E:')


#* Constant variables dependent on the experiment

#! ###########################################

# # Minimum number of trials to not discard an experiment.
# min_number_cs_trials: Final = 34
# min_number_us_trials: Final = 30

# # To estimate maximum duration of the experiment.
# habituation_duration: Final = 10 # min
# max_interval_trials: Final = 5 # min
# expected_number_cs: Final = 38
# expected_number_us: Final = 30
# time_aft_last_trial: Final = 1 # min


# # Estimated duration of stimuli, for pooling data.
# cs_duration: Final = 4 # s
# us_duration: Final = 0.1 # s
# # us_latency = 6.5 # s



# # To plot single trials.
# sessions_cs_single_trials = [[x] for x in np.arange(1, expected_number_cs+1)]
# sessions_us_single_trials = [[x] for x in np.arange(1, expected_number_us+1)]

# number_rows_single_trials = max(len(sessions_cs_single_trials), len(sessions_us_single_trials))

# names_cs_single_trials = name_sessions(cs, sessions_cs_single_trials)
# names_us_single_trials = name_sessions(us, sessions_us_single_trials)


# # To plot pooled trials.

# # 9*10 trials
# sessions_cs_sessions_10 =  [[*range(1,5)],
# 						[*range(5,15)],
# 						[*range(15,25)],
# 						[*range(25,35)],
# 						[*range(35,39)],]

# sessions_us_sessions_10 = [[*range(1,11)],
# 						[*range(11,21)],
# 						[*range(21,31)],]

# number_rows_sessions_10 = max(len(sessions_cs_sessions_10), len(sessions_us_sessions_10))



# sessions_dict: Final = {
# 	sessions_1t : {
# 		cs : {
# 			trials_sessions : sessions_cs_single_trials,
# 			names_sessions : names_cs_single_trials
# 			},
# 		us : {
# 			trials_sessions : sessions_us_single_trials,
# 			names_sessions : names_us_single_trials
# 			},
# 		number_cols_or_rows : expected_number_cs,
# 		horizontal_fig : False,
# 		fig_size : (15,2*expected_number_cs/3)
# 		},
# 	sessions_10t : {
# 		cs : {
# 			trials_sessions : sessions_cs_sessions_10,
# 			names_sessions : name_sessions(cs, sessions_cs_sessions_10)
# 			},
# 		us : {
# 			trials_sessions : sessions_us_sessions_10,
# 			names_sessions : name_sessions(us, sessions_us_sessions_10)
# 			},
# 		number_cols_or_rows : number_rows_sessions_10,
# 		horizontal_fig : True,
# 		fig_size : (60*number_rows_sessions_10/4,15)
# 		}
# 	}







#! Trace 1.5 and 2.5
# #* Path where the raw data is.
# path_home = Path(r'F:\EXTRA\trace CC 1.5 s and 2.5 s 30 trials')
# # before using absolute time
# # path_home = Path(r'/mnt/c/Users/joaqc/Desktop/test')

# #path_home = Path(r'D:\optovin\3 Classical conditioning\7, 8 trace CC 1.5 s and 2.5 s 90 trials')

# # Path backing up raw data.
# # path_backup = Path(r'E:')


# #* Constant variables dependent on the experiment

# ###########################################

#! 2500traceCC90trials

# # Minimum number of trials to not discard an experiment.
# min_number_cs_trials: Final = 94
# min_number_us_trials: Final = 90

# # To estimate maximum duration of the experiment.
# habituation_duration: Final = 10 # min
# max_interval_trials: Final = 5 # min
# expected_number_cs: Final = 98
# expected_number_us: Final = 90
# time_aft_last_trial: Final = 1 # min


# # Estimated duration of stimuli, for pooling data.
# cs_duration: Final = 4 # s
# us_duration: Final = 0.1 # s
# # us_latency = 6.5 # s



# # To plot single trials.
# sessions_cs_single_trials = [[x] for x in np.arange(1, expected_number_cs+1)]
# sessions_us_single_trials = [[x] for x in np.arange(1, expected_number_us+1)]

# number_rows_single_trials = max(len(sessions_cs_single_trials), len(sessions_us_single_trials))

# names_cs_single_trials = name_sessions(cs, sessions_cs_single_trials)
# names_us_single_trials = name_sessions(us, sessions_us_single_trials)


# # To plot pooled trials.

# # 9*10 trials
# sessions_cs_sessions_10 =  [[*range(1,5)],
# 						[*range(5,15)],
# 						[*range(15,25)],
# 						[*range(25,35)],
# 						[*range(35,45)],
# 						[*range(45,55)],
# 						[*range(55,65)],
# 						[*range(65,75)],
# 						[*range(75,85)],
# 						[*range(85,95)],
# 						[*range(95,99)]]

# sessions_us_sessions_10 = [[*range(1,11)],
# 						[*range(11,21)],
# 						[*range(21,31)],
# 						[*range(31,41)],
# 						[*range(41,51)],
# 						[*range(51,61)],
# 						[*range(61,71)],
# 						[*range(71,81)],
# 						[*range(81,91)]]

# number_rows_sessions_10 = max(len(sessions_cs_sessions_10), len(sessions_us_sessions_10))

# names_cs_sessions_10 = name_sessions(cs, sessions_cs_sessions_10)
# names_us_sessions_10 = name_sessions(us, sessions_us_sessions_10)

# # 3*30 trials
# sessions_cs_sessions_30 =  [[*range(1,5)],[*range(5,35)],[*range(35,65)],[*range(65,95)],[*range(95,99)]]
# sessions_us_sessions_30 = [[*range(1,31)],[*range(31,61)],[*range(61,91)]]

# number_rows_sessions_30 = max(len(sessions_cs_sessions_30), len(sessions_us_sessions_30))

# names_cs_sessions_30 = name_sessions(cs, sessions_cs_sessions_30)
# names_us_sessions_30 = name_sessions(us, sessions_us_sessions_30)

# # 2*45 trials
# sessions_cs_sessions_45 = [[*range(1, 5)], [*range(5, 50)], [*range(50, 95)], [*range(95, 99)]]
# sessions_us_sessions_45 = [[*range(1, 46)], [*range(46, 91)]]

# number_rows_sessions_45 = max(len(sessions_cs_sessions_45), len(sessions_us_sessions_45))

# names_cs_sessions_45 = name_sessions(cs, sessions_cs_sessions_45)
# names_us_sessions_45 = name_sessions(us, sessions_us_sessions_45)



# sessions_dict: Final = {
# 	sessions_1t : {
# 		cs : {
# 			trials_sessions : sessions_cs_single_trials,
# 			names_sessions : names_cs_single_trials
# 			},
# 		us : {
# 			trials_sessions : sessions_us_single_trials,
# 			names_sessions : names_us_single_trials
# 			},
# 		number_cols_or_rows : expected_number_cs,
# 		horizontal_fig : False,
# 		fig_size : (15,60*expected_number_cs/4)
# 		},
# 	sessions_10t : {
# 		cs : {
# 			trials_sessions : sessions_cs_sessions_10,
# 			names_sessions : names_cs_sessions_10
# 			},
# 		us : {
# 			trials_sessions : sessions_us_sessions_10,
# 			names_sessions : names_us_sessions_10
# 			},
# 		number_cols_or_rows : number_rows_sessions_10,
# 		horizontal_fig : False,
# 		fig_size : (20,60*number_rows_sessions_10/4)
# 		},
# 	sessions_30t : {
# 		cs : {
# 			trials_sessions : sessions_cs_sessions_30,
# 			names_sessions : names_cs_sessions_30
# 			},
# 		us : {
# 			trials_sessions : sessions_us_sessions_30,
# 			names_sessions : names_us_sessions_30
# 			},
# 		number_cols_or_rows : number_rows_sessions_30,
# 		horizontal_fig : True,
# 		fig_size : (60*number_rows_sessions_30/4,15)
# 		},
# 	sessions_45t : {
# 		cs : {
# 			trials_sessions : sessions_cs_sessions_45,
# 			names_sessions : names_cs_sessions_45
# 			},
# 		us : {
# 			trials_sessions : sessions_us_sessions_45,
# 			names_sessions : names_us_sessions_45
# 			},
# 		number_cols_or_rows : number_rows_sessions_45,
# 		horizontal_fig : True,
# 		fig_size : (60,15)
# 		}
# 	}


#! May 2022

# # Minimum number of trials to not discard an experiment.
# min_number_cs_trials: Final = 52
# min_number_us_trials: Final = 32

# # To estimate maximum duration of the experiment.
# habituation_duration: Final = 0.8 # min
# max_interval_trials: Final = 5 # min
# expected_number_cs: Final = 52
# expected_number_us: Final = 32
# time_aft_last_trial: Final = 1 # min


# # Estimated duration of stimuli, for pooling data.
# cs_duration: Final = 4 # s
# us_duration: Final = 0.1 # s
# # us_latency = 6.5 # s



# # To plot single trials.
# sessions_cs_single_trials = [[x] for x in np.arange(1, expected_number_cs+1)]
# sessions_us_single_trials = [[x] for x in np.arange(1, expected_number_us+1)]

# number_rows_single_trials = max(len(sessions_cs_single_trials), len(sessions_us_single_trials))

# names_cs_single_trials = name_sessions(cs, sessions_cs_single_trials)
# names_us_single_trials = name_sessions(us, sessions_us_single_trials)


# # To plot pooled trials.

# # 9*10 trials
# sessions_cs_sessions_10 =  [[*range(3,13)],
# 						[*range(13,23)],
# 						[*range(23,33)],
# 						[*range(33,43)],
# 						[*range(43,53)]]

# sessions_us_sessions_10 = [[*range(2,12)],
# 						[*range(12,22)],
# 						[*range(22,32)]]

# number_rows_sessions_10 = max(len(sessions_cs_sessions_10), len(sessions_us_sessions_10))



# sessions_dict: Final = {
# 	sessions_1t : {
# 		cs : {
# 			trials_sessions : sessions_cs_single_trials,
# 			names_sessions : names_cs_single_trials
# 			},
# 		us : {
# 			trials_sessions : sessions_us_single_trials,
# 			names_sessions : names_us_single_trials
# 			},
# 		number_cols_or_rows : expected_number_cs,
# 		horizontal_fig : False,
# 		fig_size : (15,2*expected_number_cs/3)
# 		},
# 	sessions_10t : {
# 		cs : {
# 			trials_sessions : sessions_cs_sessions_10,
# 			names_sessions : name_sessions(cs, sessions_cs_sessions_10)
# 			},
# 		us : {
# 			trials_sessions : sessions_us_sessions_10,
# 			names_sessions : name_sessions(us, sessions_us_sessions_10)
# 			},
# 		number_cols_or_rows : number_rows_sessions_10,
# 		horizontal_fig : True,
# 		fig_size : (60*number_rows_sessions_10/4,15)
# 		}
# 	}





#! June 2022

# Minimum number of trials to not discard an experiment.
min_number_cs_trials: Final = 72
min_number_us_trials: Final = 52

# To estimate maximum duration of the experiment.
# habituation_duration: Final = 0.5 # min
# max_interval_trials: Final = 4.1 # min
expected_number_cs: Final = min_number_cs_trials
expected_number_us: Final = min_number_us_trials
time_aft_last_trial: Final = 1 # min


# Estimated duration of stimuli, for pooling data.
cs_duration: Final = 4 # s
us_duration: Final = 0.1 # s
# us_latency = 6.5 # s



# To plot single trials.
sessions_cs_single_trials = [[x] for x in np.arange(1, expected_number_cs+1)]
sessions_us_single_trials = [[x] for x in np.arange(1, expected_number_us+1)]

number_rows_single_trials = max(len(sessions_cs_single_trials), len(sessions_us_single_trials))

names_cs_single_trials = name_sessions(cs, sessions_cs_single_trials)
names_us_single_trials = name_sessions(us, sessions_us_single_trials)


# To plot pooled trials.

# 9*10 trials
sessions_cs_sessions_10 =  [[*range(3,13)],
						[*range(13,23)],
						[*range(23,33)],
						[*range(33,43)],
						[*range(43,53)],
						[*range(53,63)],
						[*range(63,73)]]

sessions_us_sessions_10 = [[*range(2,12)],
						[*range(12,22)],
						[*range(22,32)],
						[*range(32,42)],
						[*range(42,52)]]

number_rows_sessions_10 = max(len(sessions_cs_sessions_10), len(sessions_us_sessions_10))



sessions_dict: Final = {
	sessions_1t : {
		cs : {
			trials_sessions : sessions_cs_single_trials,
			names_sessions : names_cs_single_trials
			},
		us : {
			trials_sessions : sessions_us_single_trials,
			names_sessions : names_us_single_trials
			},
		number_cols_or_rows : expected_number_cs,
		horizontal_fig : False,
		fig_size : (15,2*expected_number_cs/3)
		},
	sessions_10t : {
		cs : {
			trials_sessions : sessions_cs_sessions_10,
			names_sessions : name_sessions(cs, sessions_cs_sessions_10)
			},
		us : {
			trials_sessions : sessions_us_sessions_10,
			names_sessions : name_sessions(us, sessions_us_sessions_10)
			},
		number_cols_or_rows : number_rows_sessions_10,
		horizontal_fig : True,
		fig_size : (60*number_rows_sessions_10/4,15)
		}
	}


