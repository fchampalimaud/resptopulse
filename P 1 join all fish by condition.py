# 3. Concatenate all dataframes in a single dataframe.

# %%
from pathlib import Path
import pandas as pd
from tqdm import tqdm
from my_general_variables import *
from my_experiment_specific_variables import *
import gc

# import warnings
# warnings.filterwarnings("ignore", category=np.VisibleDeprecationWarning)


# Path to save processed data; create folder to save processed data if it does not exist yet.
path_part_to_save = path_home / 'Processed data' / 'pkl files' / 'Pooled data by condition'
path_part_to_save.mkdir(parents=True, exist_ok=True)


all_fish_data_paths = [*Path(path_home / 'Processed data' / 'pkl files').glob('*.pkl')]	



for condition in get_conditions():

	# match condition:
	# 	case 'conditioning_unpaired':
	# 		condition = control
	# 	case 'delayconditioning_paired':
	# 		condition = delay
	# 	case 'trace500conditioning_paired':
	# 		condition = trace500
	# 	case 'trace1500conditioning_paired':
	# 		condition = trace1500
	# 	case 'trace2500conditioning_paired':
	# 		condition = trace2500
	# 	case _:
	# 		continue

	# break
	# if condition == control:
	# 	continue



	# Select only the paths with data from a certain condition.
	# condition_paths = [str(path) for path in all_fish_data_paths if condition in str(path)]
	condition_paths = [path for path in all_fish_data_paths if condition in str(path)]



	condition = condition[:-1] if condition == 'delay_' else condition




	if not condition_paths:
		continue


	# # Do nothing if pkl file already exists.
	# if path_to_save.exists():
	# 	print(path_to_save, ' already exists.')
	# 	continue


	# condition_all_data = pd.DataFrame()
	condition_all_data_cs = [0] * len(condition_paths)
	condition_all_data_us = [0] * len(condition_paths)


	for fish_i, fish_path in tqdm(enumerate(reversed(condition_paths))):


		print(fish_path.stem.lower() + '\n')


		# Read data
		try:
			data = pd.read_pickle(str(fish_path))
		except:
			print('Cannot read pkl file.')
			continue


		# Drop columns not used anymore.
		data.drop(cols[1:-1], axis=1, inplace=True)


		# Crop data.
		data = data[(data[time_trial_csus] >= -t_crop_data_bef_s * expected_framerate) & (data[time_trial_csus] <= t_crop_data_aft_s * expected_framerate)]


		# data[activity] = data[activity].astype(pd.SparseDtype('float32', 0))


		# # To try to speed up the concatenation.
		# data.iloc[:,-4:] = data.iloc[:,-4:].sparse.to_dense()


		# condition_all_data = pd.concat([condition_all_data, data])
		condition_all_data_cs[fish_i] = data[data[type_trial_csus] == 'CS'].copy().drop(columns=type_trial_csus)
		condition_all_data_us[fish_i] = data[data[type_trial_csus] == 'US'].copy().drop(columns=type_trial_csus)

		# # Name the pkl file containing all data from a certain condition with the same name as in the index of the dataframes of that condition.
		# if condition is None:
		# 	condition = data.index.get_level_values('Exp.')[0]

		# break
	
	del data
	gc.collect()

	condition_all_data_cs = pd.concat(condition_all_data_cs)
	# Concatenation changes the activity columnd dtype from sparse float32 to sparse float64.
	condition_all_data_cs[activity] = condition_all_data_cs[activity].astype(pd.SparseDtype('float32', 0))

	condition_all_data_us = pd.concat(condition_all_data_us)
	condition_all_data_us[activity] = condition_all_data_us[activity].astype(pd.SparseDtype('float32', 0))
		
	# f.set_dtypes_and_sort_index(condition_all_data_cs)
	# f.set_dtypes_and_sort_index(condition_all_data_us)


	# Save data in a pkl file.
	condition_all_data_cs.to_pickle(path_part_to_save / str(condition + '_CS' + '.pkl'))
	condition_all_data_us.to_pickle(path_part_to_save / str(condition + '_US' + '.pkl'))




	print('Number fish in {} condition: '.format(condition), len(condition_paths))

	# break

print('\n\ndone')







exec(open('P 4 plots of pooled data.py').read())