from pathlib import Path
# %%
import pandas as pd
from tqdm import tqdm
import gc
from pandas.api.types import CategoricalDtype

from my_general_variables import *
from my_experiment_specific_variables import *
import my_functions_F_1 as f
# import my_functions_P_2 as ff

import importlib
importlib.reload(f)

pd.options.mode.chained_assignment = None
np.warnings.filterwarnings('ignore', category=np.VisibleDeprecationWarning)


Single_trials = True
Difference_at_onset_single_trials = True
Pooled_trials = True
Difference_at_onset_pooled_trials = True


# Single_trials = False
# Difference_at_onset_single_trials = False
# Pooled_trials = True
# Difference_at_onset_pooled_trials = True

frmt = 'png'

# figure_size = (10,100)

mean_bef_onset = 'mean '+str(baseline_window)+' s before'
difference = 'difference'


# path_home = Path(r'F:\EXTRA')

#TODO move this part to a module shared by all single fish analysis
# Path to save processed data; create folder to save processed data if it does not exist yet.

if Difference_at_onset_single_trials or Difference_at_onset_pooled_trials:
	list_ = [mean_bef_onset, '', difference]

# if activity in metrics:
# 	metrics_dict[activity][y_label] = 'Tail movement vigour (deg/ms)'
# 	metrics_dict[activity][y_lim] = (-1,25)
# 	metrics_dict[activity][y_ticks] = (np.arange(0,21,20))

if bout in metrics:
	metrics_dict[bout][y_label] = 'Tail movement'
	metrics_dict[bout][y_lim] = (0,1)
	metrics_dict[bout][y_ticks] =  []

if bout_beg in metrics:
	metrics_dict[bout_beg][y_label] = 'Start tail movement}$'
	metrics_dict[bout_beg][y_lim] = (None,None)
	metrics_dict[bout_beg][y_ticks] = []

if bout_end in metrics:
	metrics_dict[bout_end][y_label] = 'End tail movement'
	metrics_dict[bout_end][y_lim] = (None,None)
	metrics_dict[bout_end][y_ticks] = []


time_bins = list(np.arange(-bin_interval, -time_bef_ms/1000-bin_interval, -bin_interval))[::-1] + list(np.arange(0, time_aft_ms/1000+bin_interval, bin_interval))


path_to_save_proc_data = path_home / 'Processed data'
path_to_save_proc_data.mkdir(parents=True, exist_ok=True)

path_part_fig_continuous_traces = []

for csus in [cs, us]:
	for param in [metrics_single_trials_dict[m][folder_name] for m in metrics_single_trials_dict]:
	# ['2.0. single fish single trials angle', '2.2. single fish single trials bouts', '2.3. single fish single trials beg bouts', '2.4. single fish single trials end bouts']:
		path_part_fig_continuous_traces.append(path_to_save_proc_data/param / ('aligned by ' + str(csus)))
		path_part_fig_continuous_traces[-1].mkdir(parents=True, exist_ok=True)
# '2.1. single fish single trials activity', 

all_fish_data_paths = [*Path(path_home / 'Processed data' / 'pkl files').glob('*.pkl')]


# all_existing_figs_path = [*path_part_fig_continuous_traces[0].glob('*.{}'.format(frmt))]

# all_existing_figs_path = ['_'.join(str(p.stem).split('_')[:-1]) for p in all_existing_figs_path]

# ['_'.join(str(p.stem).split('_')) for p in all_existing_figs]



for fish_path in tqdm(reversed(all_fish_data_paths)):

	gc.collect()
	plt.close('all')

	stem_fish_path_orig = fish_path.stem.lower()

	# if 'unpaired' in stem_fish_path_orig:
	# 	continue

	print(stem_fish_path_orig)

	#* Do nothing if pkl file already exists.
	if Single_trials and (path_part_fig_continuous_traces[0] / (stem_fish_path_orig + '.' + frmt)).exists():
		print('Figures already exist.', '\n\n')
		continue

	if Pooled_trials and (path_part_fig_continuous_traces[1] / (stem_fish_path_orig + '_' + [k for k in sessions_dict.keys()][1] + '.' + frmt)).exists():
		print('Figures already exist.', '\n\n')
		continue

	stem_fish_path = stem_fish_path_orig.split('_')



	data = pd.read_pickle(fish_path)

	exp_type = data.index.get_level_values('Exp.')[0]



	#* Drop columns not used anymore.
	data.drop(cols[1:-1], axis=1, inplace=True)

	# data.loc[:, 'Angle of point 13 (deg)'] = data.loc[:, 'Angle of point 13 (deg)'].fillna(0)


	#* Convert time from frames to s.
	data[time_trial_csus] = data[time_trial_csus] / expected_framerate  # s

	data.loc[:, number_trial_csus] = data.loc[:, number_trial_csus].astype('int')
	data.loc[:, number_trial_csus] = data.loc[:, number_trial_csus].astype(CategoricalDtype(categories=data[number_trial_csus].unique().sort(), ordered=True))


	data[metrics] = data[metrics].sparse.to_dense()

	fish_id = data.index[0]

	data.reset_index(drop=True, inplace=True)

	# break

	for csus in [cs, us]:

		if csus == us and exp_type not in control:
			continue

		data_csus = data[data[type_trial_csus] == csus]


		if Single_trials:
			#* Plot single trials data.
			#TODO This could be done using the other more general function... but no need to change it.

			sessions_csus = sessions_dict[sessions_1t][csus][trials_sessions]
			
			session_all_stim = [[*range(len(sessions_csus))] for _ in range(4)]

			for m_i, m in enumerate(metrics_single_fish):

				fig, axs = plt.subplots(number_rows_single_trials, 1, sharex=True, sharey=True, facecolor='white', figsize=(50,2.2*len(sessions_cs_single_trials)), constrained_layout=True)

				for s_i, trials_in_s in enumerate(sessions_csus, start=1):

					session = data_csus[data_csus[number_trial_csus].isin(trials_in_s)]
					
					
					#* Save when the stimuli happened in each session.
					if m_i == 0:
						# f.stimuliInSession(session, s_i, session_all_stim)
						session_all_stim[0][s_i-1], session_all_stim[1][s_i-1], session_all_stim[2][s_i-1], session_all_stim[3][s_i-1] = f.findStim(session)


					#* Add data to figures.
					if m == tail_angle or m == activity:
						axs[s_i-1].plot(session[time_trial_csus], session[tail_angle], 'k', alpha=0.8, linewidth=0.3)
					else:
						axs[s_i-1].eventplot(session.loc[session[m] != 0, time_trial_csus].to_list(), color='black', alpha=0.8, lineoffsets=1, linelengths=1)


					#* Highlight stimuli in plots.
					for cs_beg_, cs_end_ in zip(session_all_stim[0][s_i-1], session_all_stim[1][s_i-1]):
						axs[s_i-1].axvspan(cs_beg_, cs_end_, color='green', alpha=0.8, linewidth=0)
					for us_beg_, us_end_ in zip(session_all_stim[2][s_i-1], session_all_stim[3][s_i-1]):
						axs[s_i-1].axvspan(us_beg_, us_end_, color='red', alpha=0.8, linewidth=0)


					#* Arrange figures.

					for ax in axs:
						# TODO in a function too!!
						ax.set(xlim=(-time_bef_ms/1000,time_aft_ms/1000), ylim=metrics_single_trials_dict[m][y_lim], xticks=(np.arange(-40,41,10)), yticks=metrics_single_trials_dict[m][y_ticks])
						ax.spines[:].set_visible(False)
						ax.axes.yaxis.set_ticks(metrics_single_trials_dict[m][y_ticks])
						ax.tick_params(axis='both', which='both', bottom=True, top=False, right=False)

					if len(sessions_csus) < len(axs):
						axs[len(sessions_csus)-1].xaxis.set_tick_params(labelbottom=True)
						axs[-1].xaxis.set_tick_params(labelbottom=False)
					
					axs[s_i-1].set_title(s_i, fontsize='small', loc='left')
					axs[len(sessions_csus)-1].set_xlabel('t (s)')


				#* Add labels to figures.

				if m == tail_angle:
					fig.supylabel(metrics_single_trials_dict[tail_angle][y_label])

				fig.suptitle(metrics_single_trials_dict[m][y_label] + '\n' + '_'.join(fish_id))


				#* Save figure.
				f.saveFig_1(fig, fig_name=str(path_part_fig_continuous_traces[m_i if csus == cs else m_i + len(metrics_single_fish)] / stem_fish_path_orig), figure_size=sessions_dict[sessions_1t][fig_size], frmt=frmt, dpi=100)

	# 			break
	# 		break
	# 	break
	# break

				plt.close('all')
				gc.collect()

	# 	break
	# break
	

		if Difference_at_onset_single_trials and csus != us:
			#* Plot difference at onset.

			#TODO case when csus==us

			cr_window = cs_duration if exp_type in control else condition_dict[exp_type][us_latency]

			mean_aft_onset = 'mean '+str(cr_window)+' s after'
			list_[1] = mean_aft_onset

			data_csus_difference_at_onset = f.difference_at_onset(data_csus, cr_window)

			for m_i, m in enumerate(metrics):

				fig, axs = plt.subplots(3, 1, sharex=True, sharey=False, facecolor='white', constrained_layout=True)

				for ax_i, ax in enumerate(axs):

					ax.plot(data_csus_difference_at_onset[number_trial_csus], data_csus_difference_at_onset.xs((m, list_[ax_i]), axis=1), '.', color='k', alpha=0.8, linewidth=0, markersize=10, label=list_[ax_i], clip_on=False)

					x_lim = (-0.5, len(sessions_dict[sessions_1t][csus][names_sessions]))

					ax.spines['right'].set_visible(False)
					ax.spines['top'].set_visible(False)
					ax.locator_params(axis='y', tight=False, nbins=4)
					ax.tick_params(axis='both', which='both', bottom=True, top=False, right=False)

					if ax_i < 2:
						# ax.axes.get_xaxis().set_visible(False)
						ax.set_ylim((0, None))

					ax.set_xlim(x_lim)
					# ax.set_xticks(x_ticks)
					if csus == cs:
						ax.axvline(sessions_dict[sessions_10t][cs][trials_sessions][0][-1] + 0.5, color='grey', alpha=0.8, linewidth=1)
						ax.axvline(sessions_dict[sessions_10t][cs][trials_sessions][-1][0] + 0.5, color='grey', alpha=0.8, linewidth=1)

				axs[2].axhline(0, color='k', alpha=0.8, linewidth=1)
				axs[0].set_ylabel(str(baseline_window) + ' s-window before stim. onset')
				axs[1].set_ylabel(str(cr_window) + ' s-window after stim. onset')
				axs[2].set_ylabel('$\Delta$')

				axs[2].set_xlabel('Trial number')

				fig.suptitle(metrics_single_trials_dict[m][y_label] + '\n' + '_'.join(fish_id), x=0, horizontalalignment='left')

				f.saveFig_1(fig, fig_name=str(path_part_fig_continuous_traces[m_i+1 if csus == cs else m_i+2 + len(metrics)] / (stem_fish_path_orig + '_difference at onset')), figure_size=(15,15), frmt=frmt, dpi=50)

				plt.close('all')
				gc.collect()


		if Pooled_trials:
			#* Plot single fish trials grouped in sessions.

			for pool_type in [k for k in sessions_dict.keys()][1:]:

				sessions_csus = sessions_dict[pool_type][csus][trials_sessions]

				data_csus = f.pooled_data_stats(data_csus, csus, pool_type)

	#TODO this part should call another function called binData()
				pooled_data_ = f.bin_or_filter_data(data_csus, 'mean', True, time_bins).reset_index(time_trial_csus)

				for m_i, m in enumerate(metrics):

					fig, axs = f.createFig_1(csus, pool_type, y_label=metrics_dict[m][y_label], y_lim=metrics_dict[m][y_lim] if m == bout else (pooled_data_.loc[:,m].min().min(), pooled_data_.loc[:,m].max().max()), x_lim_time=(-45,45), x_ticks_time=(np.arange(-45,46,5)))

					for ax_i, ax in enumerate(axs):

						if csus == us and ax_i >= len(axs[:-2]):
							break

						#* Add data to figures.
						try:
							axs[ax_i].plot(pooled_data_[time_trial_csus], pooled_data_.loc[:,m].iloc[:,ax_i], color=condition_dict[exp_type][color], alpha=0.8, linewidth=2, drawstyle="steps-mid", label=exp_type)
						except:
							pass

						
						#* Highlight in the subplots specific stimuli to the condition.
						f.highlightSpecStim(axs, csus, exp_type, 'red', sessions_dict[pool_type][csus][trials_sessions])


					#* Add labels to figures.
					if sessions_dict[pool_type][horizontal_fig]:
						fig.suptitle(metrics_single_trials_dict[m][y_label] + '\n' + '_'.join(fish_id), x=0, horizontalalignment='left')
					else:
						fig.suptitle(metrics_single_trials_dict[m][y_label] + '\n' + '_'.join(fish_id))


					#* Save figure.
					f.saveFig_1(fig, fig_name=str(path_part_fig_continuous_traces[m_i+1 if csus == cs else m_i+2 + len(metrics)] / (stem_fish_path_orig + '_' + pool_type)), figure_size=sessions_dict[pool_type][fig_size], frmt=frmt, dpi=50)

					plt.close('all')
					gc.collect()

		# break


		if Difference_at_onset_pooled_trials and csus != us:

			if not Difference_at_onset_single_trials:
				cr_window = cs_duration if exp_type in control else condition_dict[exp_type][us_latency]

				mean_aft_onset = 'mean '+str(cr_window)+' s after'
				list_[1] = mean_aft_onset

			for pool_type in [k for k in sessions_dict.keys()][1:]:
				
				if not Pooled_trials:
					
					sessions_csus = sessions_dict[pool_type][csus][trials_sessions]
					
					data_csus = f.pooled_data_stats(data_csus, csus, pool_type)

				pooled_data = f.difference_at_onset(data_csus, cr_window)


				#* Give names to the sessions.
				try:
					pooled_data.loc[:, number_session] = sessions_dict[pool_type][csus][names_sessions]
				except:
					print('Lacks last session.')
					pooled_data.loc[:, number_session] = sessions_dict[pool_type][csus][names_sessions][:-1]


				for m_i, m in enumerate(metrics):

					fig, axs = plt.subplots(3, 1, sharex=True, sharey=False, facecolor='white', constrained_layout=True)

					for ax_i, ax in enumerate(axs):

						ax.plot(pooled_data[number_session], pooled_data.xs((m, list_[ax_i]), axis=1), '.', color='k', alpha=0.8, linewidth=0, markersize=10, label=list_[ax_i], clip_on=False)

		# fig, axs = f.arrangeFig_2(fig, axs, csus, m, pool_type, cr_window, y_lim_3=(None,None))

						x_lim = (-0.5, len(sessions_dict[pool_type][csus][names_sessions]))

						ax.spines['right'].set_visible(False)
						ax.spines['top'].set_visible(False)
						ax.locator_params(axis='y', tight=False, nbins=4)
						ax.tick_params(axis='both', which='both', bottom=True, top=False, right=False)

						if ax_i < 2:
							# ax.axes.get_xaxis().set_visible(False)
							ax.set_ylim((0, None))

						ax.set_xlim(x_lim)
						# ax.set_xticks(x_ticks)
						if csus == cs:
							ax.axvline(0.5, color='grey', alpha=0.8, linewidth=1)
							ax.axvline(len(sessions_dict[pool_type][cs][trials_sessions]) - 1.5, color='grey', alpha=0.8, linewidth=1)

					axs[2].axhline(0, color='k', alpha=0.8, linewidth=1)
					# axs[0].set_ylabel(str(baseline_window) + ' s-window before stim. onset')
					# axs[1].set_ylabel(str(cr_window) + ' s-window after stim. onset')
					# axs[2].set_ylabel('$\Delta$')

					axs[0].set_ylabel('$P^{before\ stim.\ onset}_{tail\ movement}$')
					axs[1].set_ylabel('$P^{after\ stim.\ onset}_{tail\ movement}$')
					axs[2].set_ylabel('$\Delta{P}^{stim.\ onset}_{tail\ movement}$')

					for tick in axs[2].get_xticklabels():
						tick.set_rotation(45)

					fig.suptitle(metrics_single_trials_dict[m][y_label] + '\n' + '_'.join(fish_id), x=0, horizontalalignment='left')

					# break

					f.saveFig_1(fig, fig_name=str(path_part_fig_continuous_traces[m_i+1 if csus == cs else m_i+2 + len(metrics)] / (stem_fish_path_orig + '_' + pool_type + '_difference at onset')), figure_size=(10,10), frmt=frmt, dpi=50)

					plt.close('all')
					gc.collect()

	# 	break
	# break


	# if sessions_dict[pool_type][horizontal_fig]:
	# 	axs[-1].legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0, frameon=False)
	# else:
	# 	axs[0].legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0, frameon=False)


	# f.saveFig_1(fig, fig_names[m_i] + str(time.strftime('%Y-%m-%d %H.%M.%S', time.gmtime())), sessions_dict[pool_type][fig_size], frmt, dpi=40)

	# break

	gc.collect()
	print('\n\n')
plt.close('all')
print('done')
